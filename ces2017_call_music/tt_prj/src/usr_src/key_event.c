#include "global_define.h"


void key_init( void )
{
	key_config_power();
	key_config_vol_wake();
	key_config_source();
}

void key_config_power( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(KEY_GPIO_POWER_RCC, ENABLE);

	/* Configure PA1 in input mode */
	GPIO_InitStructure.GPIO_Pin = KEY_GPIO_POWER_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(KEY_GPIO_POWER_PORT, &GPIO_InitStructure);

	/* for use ext interrupt */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Tell system that you will use PA1 for EXTI_Line1 */
	SYSCFG_EXTILineConfig(KEY_EXTI_POWER_PORT, KEY_EXTI_POWER_PIN);

	/* PA1 is connected to EXTI_Line1 */
	EXTI_InitStructure.EXTI_Line = KEY_EXTI_POWER_LINE;
	/* Enable interrupt */
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	/* Interrupt mode */
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	/* Triggers on rising and falling edge */
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	/* Add to EXTI */
	EXTI_Init(&EXTI_InitStructure);

	/* EXTI1 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = KEY_EXTI_POWER_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


void key_config_source( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(KEY_GPIO_SOURCE_RCC, ENABLE);

	/* Configure PA1 in input mode */
	GPIO_InitStructure.GPIO_Pin = KEY_GPIO_SOURCE_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(KEY_GPIO_SOURCE_PORT, &GPIO_InitStructure);

	/* for use ext interrupt */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Tell system that you will use PA1 for EXTI_Line1 */
	SYSCFG_EXTILineConfig(KEY_EXTI_SOURCE_PORT, KEY_EXTI_SOURCE_PIN);

	/* PA1 is connected to EXTI_Line1 */
	EXTI_InitStructure.EXTI_Line = KEY_EXTI_SOURCE_LINE;
	/* Enable interrupt */
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	/* Interrupt mode */
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	/* Triggers on rising and falling edge */
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	/* Add to EXTI */
	EXTI_Init(&EXTI_InitStructure);

	/* EXTI1 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = KEY_EXTI_SOURCE_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


/* Function key Input Configuration
* EXT13~15  PC13~15
*/
void key_config_vol_wake( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(KEY_GPIO_VOL_WAKE_RCC, ENABLE);

	/* Configure PC13~15 in input mode */
	GPIO_InitStructure.GPIO_Pin = KEY_GPIO_WAKEUP_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(KEY_GPIO_VOL_WAKE_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = KEY_GPIO_VOLUP_PIN | KEY_GPIO_VOLDN_PIN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(KEY_GPIO_VOL_WAKE_PORT, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Tell system that you will use PC13~15 for EXTI_Line15_10 */
	SYSCFG_EXTILineConfig(KEY_EXTI_VOL_WAKE_PORT, KEY_EXTI_WAKEUP_PIN);

	/* PC13~15 is connected to EXTI_Line15_10 */
	EXTI_InitStructure.EXTI_Line = KEY_EXTI_WAKEUP_LINE;
	/* Enable interrupt */
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	/* Interrupt mode */
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	/* Triggers on rising and falling edge */
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//
	/* Add to EXTI */
	EXTI_Init(&EXTI_InitStructure);
	
	SYSCFG_EXTILineConfig(KEY_EXTI_VOL_WAKE_PORT, KEY_EXTI_VOLUP_PIN);
	EXTI_InitStructure.EXTI_Line = KEY_EXTI_VOLUP_LINE;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	EXTI_Init(&EXTI_InitStructure);
	
	SYSCFG_EXTILineConfig(KEY_EXTI_VOL_WAKE_PORT, KEY_EXTI_VOLDN_PIN);
	EXTI_InitStructure.EXTI_Line = KEY_EXTI_VOLDN_LINE;
	EXTI_Init(&EXTI_InitStructure);

	/* EXTI13~15 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = KEY_EXTI_VOL_WAKE_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

void key_power_isr( void )
{
#ifdef ENABLE_LOWPWR
	g_str_timer.ui16_sleep_cnt = 0;
#endif
	if (EXTI_GetITStatus(KEY_EXTI_POWER_LINE) != RESET)
	{
		if( POWER_KEY_STATUS )
		{
			g_key_state |= KEY_POWER_PRESS;
		}
		else
		{
			g_key_state |= KEY_POWER_RELEASE;
		}
		
		g_key_event = KEY_EVENT_POWER;
		
		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(KEY_EXTI_POWER_LINE);
	}
}

void key_vol_wake_isr( void )
{
#ifdef ENABLE_LOWPWR
	g_str_timer.ui16_sleep_cnt = 0;
#endif

	if (EXTI_GetITStatus(KEY_EXTI_WAKEUP_LINE) != RESET)
	{
                GPIOA->BSRRL = GPIO_Pin_15;

		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(KEY_EXTI_WAKEUP_LINE);
	}
  
	else if (EXTI_GetITStatus(KEY_EXTI_VOLUP_LINE) != RESET)
	{
		if( VOLUP_KEY_STATUS )
		{
			g_key_state |= KEY_VOLUP_PRESS;
		}
		else
		{
			g_key_state |= KEY_VOLUP_RELEASE;
		}
		
		g_key_event = KEY_EVENT_VOLUP;
		
		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(KEY_EXTI_VOLUP_LINE);

	}
	
	else if (EXTI_GetITStatus(KEY_EXTI_VOLDN_LINE) != RESET)
	{
		if( VOLDN_KEY_STATUS )
		{
			g_key_state |= KEY_VOLDN_PRESS;
		}
		else
		{
			g_key_state |= KEY_VOLDN_RELEASE;
		}
		
		g_key_event = KEY_EVENT_VOLDN;

		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(KEY_EXTI_VOLDN_LINE);
	}
	
}

void key_source_isr(void)
{
#ifdef ENABLE_LOWPWR
	g_str_timer.ui16_sleep_cnt = 0;
#endif

	if (EXTI_GetITStatus(KEY_EXTI_SOURCE_LINE) != RESET)
	{
		if( SOURCE_KEY_STATUS )
		{
			g_key_state |= KEY_SOURCE_DOWN;
		}
		else
		{
			g_key_state |= KEY_SOURCE_UP;
		}
		
		g_key_event = KEY_EVENT_SOURCE;

		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(KEY_EXTI_SOURCE_LINE);
	}
}
