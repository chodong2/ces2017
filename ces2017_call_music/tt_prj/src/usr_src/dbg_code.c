#include "global_define.h"

#ifdef ENABLE_DBG

void dbg_init(void)
{
	dbg_timer_config_us(1000000);
}

/* Timer Configuration
* Use Timer 2
*/
void dbg_timer_config_us(uint32_t ui32usec)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the TIM2 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = DBG_TIM_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseInitTypeDef TIM_BaseStruct;
	/* TIM2 clock enable */
	RCC_APB1PeriphClockCmd(DBG_TIM_RCC, ENABLE);
	/* Time base configuration */
	TIM_BaseStruct.TIM_Period = ui32usec - 1; // N (us)
	TIM_BaseStruct.TIM_Prescaler = 50; // 50 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(DBG_TIM_BASE, &TIM_BaseStruct);

	/* TIM IT enable */
	TIM_ITConfig(DBG_TIM_BASE, TIM_IT_Update, ENABLE);
}

void dbg_timer_stop(void)
{
	TIM_Cmd(DBG_TIM_BASE, DISABLE);
}

void dbg_timer_start(uint32_t ui32usec)
{
	dbg_timer_config_us(ui32usec);

	TIM_Cmd(DBG_TIM_BASE, ENABLE);
}

void dbg_timer_isr(void)
{
	if (TIM_GetITStatus(DBG_TIM_BASE, TIM_IT_Update) == SET)
	{
		TIM_ClearITPendingBit(DBG_TIM_BASE, TIM_IT_Update);

		/* led blink pattern array */
		//led_blink();
		g_ui16_dbg_cnt++;
	}
}

#endif //ENABLE_DBG