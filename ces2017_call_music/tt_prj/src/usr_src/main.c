#include "global_define.h"
#include "main.h"

bitflag_struct_t g_str_bitflag;
key_state_t g_key_state;
key_event_t g_key_event;

#ifdef ENABLE_UART
bt_uart_struct_t g_str_bt;
#endif

#ifdef ENABLE_AUDIO
audio_struct_t g_str_audio;
#endif

timer_struct_t g_str_timer;

#ifdef ENABLE_PWM
pwm_struct_t g_str_pwm;
#ifdef ENABLE_NOTIFY
pattern_cnt_struct_t g_str_pattern;
#endif
#endif

#ifdef ENABLE_FORMANT_ENHANCEMENT
fe_struct_t g_str_fe;
#endif

#ifdef ENABLE_OTA
ota_struct_t g_str_ota;
#endif

#ifdef ENABLE_BATT_STATUS
batt_struct_t g_str_batt;
chg_struct_t g_str_chg;
#endif

#ifdef ENABLE_NOTIFY
pattern_cnt_struct_t g_str_batt_level;
pattern_cnt_struct_t g_str_app_noti;
pattern_cnt_struct_t g_str_bt_conn;
pattern_cnt_struct_t g_str_bt_disconn;
pattern_cnt_struct_t g_str_pwr_on;
pattern_cnt_struct_t g_str_incomming_call;
#endif

void idletest(void)
{
	//uint32_t Address = 0;
	uint32_t jump_addr;
	//	uint32_t app_start_addr;

	jump_addr = *(__IO uint32_t*) (FLASH_ADDR_JUMP_APP);
	if (jump_addr == FLASH_ADDR_CURRENT_APP)
		return;// FLASH_ERROR_NONE;

        /* Unlock the Flash to enable the flash control register access *************/
	FLASH_Unlock();

	/* Erase the user Flash area
	(area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

	/* Clear pending flags (if any) */
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
		FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);


	/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
	be done by word */
	if (FLASH_EraseSector(FLASH_Sector_2, VoltageRange_3) != FLASH_COMPLETE)
	{
		/* Error occurred while sector erase.
		User can add here some code to deal with this error  */
		//ret = FLASH_ERROR_ERASE;
		FLASH_Lock();
		return;
	}

	if (FLASH_ProgramWord(FLASH_ADDR_JUMP_APP, FLASH_ADDR_CURRENT_APP)
		!= FLASH_COMPLETE)
	{
		//ret = FLASH_ERROR_WRITE;
	}
	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) *********/
	FLASH_Lock();
}

void peripheral_init(void)
{
	//I2C_Config();
        i2c_init();

	monitor_init();
#ifdef ENABLE_AUTH
	authenticate();
#endif //ENABLE_AUTH

#ifdef ENABLE_PEDOMETER
	pedometer_init();
#endif

	pin_enable_init();
	key_init();

#ifdef ENABLE_UART
	uart_init();
#endif

#ifdef ENABLE_LED
	led_init();
#endif

#ifdef ENABLE_PWM
	pwm_init();
#endif

#ifdef ENABLE_AUDIO
	audio_init(AUDIO_INPUT_44_1K);
#endif

#ifdef ENABLE_IS2063
	BTAPP_Init();
#endif
	idletest();
}

void struct_init(void)
{
	g_str_bitflag.b1_frame_state = AUDIO_FRAME_OK;
	g_str_bitflag.b1_power_down = 0;
	g_str_bitflag.b1_power_key_flag = 0;
	g_str_bitflag.b1_uart_end = 0;

	g_str_bitflag.b1_audio_state = AUDIO_STATE_STOP;
	g_str_bitflag.b1_audio_stream_flag = 0;

	g_str_bitflag.b1_bt_conn = 0;

	g_str_bitflag.b1_batt_chk = 0;

#ifdef ENABLE_FORMANT_ENHANCEMENT
	g_str_bitflag.b1_fe_enable_flag = 0;
	g_str_fe.ui16_frame_no = 0;

	for (int i = 0; i < AUDIO_FRAME_SIZE; i++)
	{
		g_str_fe.ui16_data[i] = 0;
		g_str_fe.ui16_data2[i] = 0;
	}
#endif

	g_str_bitflag.b2_pair_flag = PAIR_NONE;

	g_str_bitflag.b3_freq_chk = FREQ_CHK_READY;

#ifdef ENABLE_UART

	for (int i = 0; i < BT_UART_BUFFERSIZE; i++)
	{
		g_str_bt.ui8_rx_buf[i] = 0;
		g_str_bt.ui8_tx_buf[i] = 0;
	}

	g_str_bt.ui16_rx_head = 0;
	g_str_bt.ui16_rx_tail = 0;
	g_str_bt.ui16_rx_cnt = 0;

	g_str_bt.ui16_tx_head = 0;
	g_str_bt.ui16_tx_tail = 0;
	g_str_bt.ui16_tx_cnt = BT_UART_BUFFERSIZE;
	g_str_bt.ui16_tx_flag = 0;

#endif //ENABLE_UART

#ifdef ENABLE_AUDIO

	for (int i = 0; i < AUDIO_FRAME_SIZE; i++)
	{
		g_str_audio.ui16_save_buf[i] = 0;
	}

	g_str_audio.ui16_save_cnt = 0;
	g_str_audio.ui16_freq_cnt = 0;
	g_str_audio.ui16_pre_freq = AUDIO_INPUT_NONE;
	g_str_audio.ui16_audio_type = AUDIO_INPUT_NONE;

#ifdef ENABLE_AUDIO_CHANNEL
	g_str_audio.ui16_channel_sel = AUDIO_CHANNEL_NONE;//AUDIO_CHANNEL_LEFT;
#endif //ENABLE_AUDIO_CHANNEL

#endif //ENABLE_AUDIO

	g_str_timer.ui16_audio_cnt = 0;
	g_str_timer.ui16_power_cnt = 0;
	g_str_timer.ui16_led_cnt = 0;
	g_str_timer.ui16_sleep_cnt = 0;

#ifdef ENABLE_OTA
	g_str_ota.ui16_state = OTA_STATE_NONE;
#endif //ENABLE_OTA

#ifdef ENABLE_BATT_STATUS
	g_str_batt.ui16_batt_status = BATT_STATUS_INITIAL;
	g_str_batt.ui16_batt_level = BATT_LEVEL_INITIAL;

	g_str_chg.ui16_chg_status = CHG_STATUS_INITIAL;
	g_str_chg.ui16_chg_type = CHG_TYPE_INITIAL;

	memset(&g_str_batt_level, 0, sizeof(g_str_batt_level));
	memset(&g_str_app_noti, 0, sizeof(g_str_batt_level));
	memset(&g_str_bt_conn, 0, sizeof(g_str_batt_level));
	memset(&g_str_bt_disconn, 0, sizeof(g_str_batt_level));

	memset(&g_str_pwr_on, 0, sizeof(g_str_batt_level));
	memset(&g_str_incomming_call, 0, sizeof(g_str_batt_level));

#endif 

#ifdef ENABLE_PEDOMETER
        g_str_bitflag.b1_motion_chk = 0;
#endif
        
#ifdef ENABLE_LOWPWR
	g_str_bitflag.b2_power_mode = PWR_MODE_LOW;
#endif

        g_str_bitflag.b3_haptic_status = STOP;
}

#define HAPTIC_PERIOD   200
#define HAPTIC_SWITCH   100
void main(void)
{
	//pefipheral configuration initialize
	peripheral_init();

#ifdef ENABLE_LOWPWR
	set_clk_low();
#endif
	//structure variable initailize
	struct_init();

	POWER_HOLD_ENABLE;
	
#ifdef ENABLE_WATCHDOG
	
	if( TM_WATCHDOG_Init(TM_WATCHDOG_Timeout_30ms))
	{
		//system was reset by watchdog	
	}
	else
	{
		//system was not reset by watchdog
	}
#endif

#ifdef ENABLE_AUDIO
	audio_rx_start();
	AMP_STATUS_DISABLE;
#endif
        //I2S_Cmd(AUDIO_I2S_TX, ENABLE);
	//AMP_STATUS_ENABLE;

	while (1)
	{
		key_scan();

#ifdef ENABLE_OTA
		if (g_str_ota.ui16_state == OTA_STATE_NONE)
		{
#endif //ENABLE_OTA

#ifdef ENABLE_HAPTIC
                        if(g_str_bitflag.b3_haptic_status == CALL_INCOMING)
                        {
                                (g_str_timer.ui16_haptic_cnt < HAPTIC_SWITCH)?  pwm_pulse_haptic(0):pwm_pulse_haptic(100);
                        }
                        else if(g_str_bitflag.b3_haptic_status == NOTIFICATION_START)
                        {
                                (g_str_timer.ui16_haptic_cnt < HAPTIC_SWITCH)?  pwm_pulse_haptic(0):pwm_pulse_haptic(50);
                        }
                        
                        if(g_str_timer.ui16_haptic_cnt >= HAPTIC_PERIOD)
                        {
                                g_str_timer.ui16_haptic_cnt = 0;
                                if(g_str_bitflag.b3_haptic_status == NOTIFICATION_START)
                                {
                                        g_str_bitflag.b3_haptic_status = STOP;
                                        pwm_pulse_haptic(0);
                                }
                        }
#endif

#if 01
                        if(g_str_bitflag.b1_incoming_call)
                        {
                                g_str_bitflag.b3_freq_chk = FREQ_CHK_READY;
                                g_str_audio.ui16_freq_cnt = 0;
                                g_str_audio.ui16_sec_cnt = 0;
                        }
#endif

#ifdef ENABLE_AUDIO
			// check frame error
			if (g_str_bitflag.b1_frame_state == AUDIO_FRAME_ERR)
			{
				//wait 20ms
				if (g_str_timer.ui16_audio_cnt >= 2)
				{
                                        if( g_str_audio.ui16_audio_type <= AUDIO_INPUT_16K )
                                                fe_init();      //fe buffer reset

					//enable audio rx
					g_str_bitflag.b1_frame_state = AUDIO_FRAME_OK;
					audio_rx_start();
					g_str_timer.ui16_audio_cnt = 0;

				}
			}

                        if( ( g_str_audio.ui16_audio_type <= AUDIO_INPUT_16K ) && (g_str_bitflag.b3_freq_chk == FREQ_CHK_END) )
                        {
                                // BCU audio out
                                if (g_str_bitflag.b1_fe_enable_flag)
                                {
                                        //fe start
                                        formant_enhancement();

                                        g_str_bitflag.b1_fe_enable_flag = 0;
                                }
                        }  
#endif //ENABLE_AUDIO

			//power down
			if ( g_str_bitflag.b1_power_down || g_str_bitflag.b1_batt_low )
			{
				//power off sequence
				if (g_str_timer.ui16_power_cnt > 350)//jump to monitor program after 1sec
				{
                                        __disable_irq();
                                        monitor_timer_stop();
                                        USART_Cmd(BT_UART, DISABLE);
					g_str_bitflag.b1_power_down = 0;
					g_str_timer.ui16_power_cnt = 0;
                                        g_str_bitflag.b1_batt_low = 0;

                                        typedef  void(*pFunction)(void);
                                        pFunction Jump_To_Monitor;

                                        uint32_t JumpAddress;
                                        /* Jump to user application */
                                        JumpAddress = *(__IO uint32_t*)0x08000000;
                                        Jump_To_Monitor = (pFunction)JumpAddress;

                                        /* Initialize user application's Stack Pointer */
                                        Jump_To_Monitor();
				}
				else if (g_str_timer.ui16_power_cnt > 250)//BT reset after 1sec
				{
					BTRESET_DISABLE;
				}
                                else if (g_str_timer.ui16_power_cnt > 150)//led off after 1sec
                                {
#ifdef ENABLE_PWM
					pwm_pulse_off();
#endif
                                }
                                else if (g_str_timer.ui16_power_cnt < 50)//500ms after press pwr key during 5sec
				{
					MFB_DISABLE;
					AMP_STATUS_DISABLE;
                                        BTAPP_TaskReq(BT_REQ_SYSTEM_OFF);

#ifdef ENABLE_PWM
					pwm_pulse_led1(80);
					pwm_pulse_led2(80);
					pwm_pulse_led3(80);
					pwm_pulse_led4(80);
					pwm_pulse_led5(80);
#endif
				}
			}

#ifdef ENABLE_AUDIO
			//input audio frequency check
			if (g_str_bitflag.b3_freq_chk == FREQ_CHK_ANALYZE)
			{
                                g_str_bitflag.b1_audio_state = AUDIO_STATE_STOP;
                                AMP_STATUS_DISABLE;
				if (g_str_audio.ui16_freq_cnt < 800*2)
				{
					g_str_audio.ui16_freq_cnt = 0;
					g_str_audio.ui16_pre_freq = AUDIO_INPUT_NONE;
				}
				else if (g_str_audio.ui16_freq_cnt < 1600*2)
				{
					g_str_audio.ui16_pre_freq = AUDIO_INPUT_8K;
                                        g_str_audio.ui16_audio_type = AUDIO_INPUT_8K;
                                        audio_i2s_config_rx(AUDIO_INPUT_8K);
                                        audio_i2s_config_tx(AUDIO_INPUT_8K);
                                        I2S_Cmd(AUDIO_I2S_RX, ENABLE);
				}
				else if (g_str_audio.ui16_freq_cnt < 4410*2)
				{
					g_str_audio.ui16_pre_freq = AUDIO_INPUT_16K;
                                        g_str_audio.ui16_audio_type = AUDIO_INPUT_16K;
                                        audio_i2s_config_rx(AUDIO_INPUT_16K);
                                        audio_i2s_config_tx(AUDIO_INPUT_16K);
                                        I2S_Cmd(AUDIO_I2S_RX, ENABLE);
				}
				else if (g_str_audio.ui16_freq_cnt < 4800*2)
				{
					g_str_audio.ui16_pre_freq = AUDIO_INPUT_44_1K;
                                        g_str_audio.ui16_audio_type = AUDIO_INPUT_44_1K;
                                        audio_i2s_config_rx(AUDIO_INPUT_44_1K);
                                        audio_i2s_config_tx(AUDIO_INPUT_44_1K);
                                        I2S_Cmd(AUDIO_I2S_RX, ENABLE);
				}
				else
				{
					g_str_audio.ui16_pre_freq = AUDIO_INPUT_48K;
                                        g_str_audio.ui16_audio_type = AUDIO_INPUT_48K;
                                        audio_i2s_config_rx(AUDIO_INPUT_48K);
                                        audio_i2s_config_tx(AUDIO_INPUT_48K);
                                        I2S_Cmd(AUDIO_I2S_RX, ENABLE);
				}
                                
                                g_str_bitflag.b3_freq_chk = FREQ_CHK_END;
			}
#endif //ENABLE_AUDIO

#ifdef ENABLE_OTA
		}
		else
		{
			OTA_Task();
		}
#endif //ENABLE_OTA

#ifdef ENABLE_IS2063
		BTAPP_Task();
#endif

#ifdef ENABLE_LOWPWR

#ifdef ENABLE_PEDOMETER
		if ((g_str_timer.ui16_sleep_cnt > 1000) && (!g_str_bitflag.b1_motion_chk))
#else
		if (g_str_timer.ui16_sleep_cnt > 1000)
#endif                
		{
			set_sleepMode();
			g_str_timer.ui16_sleep_cnt = 0;
		}
#endif

#ifdef ENABLE_PEDOMETER  
		pedometer();
#endif //ENABLE_PEDOMETER
		
#ifdef ENABLE_WATCHDOG
		TM_WATCHDOG_Reset();
#endif
	}
}




#if 0
void hard_fault_handler_c(unsigned int * hardfault_args)
{
	unsigned int stacked_r0;
	unsigned int stacked_r1;
	unsigned int stacked_r2;
	unsigned int stacked_r3;
	unsigned int stacked_r12;
	unsigned int stacked_lr;
	unsigned int stacked_pc;
	unsigned int stacked_psr;

	stacked_r0 = ((unsigned long)hardfault_args[0]);
	stacked_r1 = ((unsigned long)hardfault_args[1]);
	stacked_r2 = ((unsigned long)hardfault_args[2]);
	stacked_r3 = ((unsigned long)hardfault_args[3]);
	stacked_r12 = ((unsigned long)hardfault_args[4]);
	stacked_lr = ((unsigned long)hardfault_args[5]);
	stacked_pc = ((unsigned long)hardfault_args[6]);
	stacked_psr = ((unsigned long)hardfault_args[7]);

	printf("[Hard fault handler]\n\r");
	printf("R0 = %x\n\r", stacked_r0);
	printf("R1 = %x\n\r", stacked_r1);
	printf("R2 = %x\n\r", stacked_r2);
	printf("R3 = %x\n\r", stacked_r3);
	printf("R12 = %x\n\r", stacked_r12);
	printf("LR = %x\n\r", stacked_lr);
	printf("PC = %x\n\r", stacked_pc);
	printf("PSR = %x\n\r", stacked_psr);
	printf("BFAR = %x\n\r", (*((volatile unsigned long *)(0xE000ED38))));
	printf("CFSR = %x\n\r", (*((volatile unsigned long *)(0xE000ED28))));
	printf("HFSR = %x\n\r", (*((volatile unsigned long *)(0xE000ED2C))));
	printf("DFSR = %x\n\r", (*((volatile unsigned long *)(0xE000ED30))));
	printf("AFSR = %x\n\r", (*((volatile unsigned long *)(0xE000ED3C))));

}
#endif

void HardFault_Handler(void)
{
	SCB->AIRCR = (0x05FA0000) | 0x04;
}
