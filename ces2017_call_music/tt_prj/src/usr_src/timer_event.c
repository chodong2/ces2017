#include "global_define.h"

volatile uint32_t g_ui32_10ms_count = 0;
volatile uint32_t g_ui32_1ms_count = 0;

void monitor_init(void)
{
	monitor_timer_config();
	monitor_timer_start();
}

/* Timer Configuration
* Use Timer 4
*/
void monitor_timer_config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the TIM4 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = MONITOR_TIM_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseInitTypeDef TIM_BaseStruct;
	/* TIM4 clock enable */
	RCC_APB1PeriphClockCmd(MONITOR_TIM_RCC, ENABLE);
	/* Time base configuration */
	TIM_BaseStruct.TIM_Period = SystemCoreClock / (10000 - 1); // N (us)
	TIM_BaseStruct.TIM_Prescaler = 50; // 50 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(MONITOR_TIM_BASE, &TIM_BaseStruct);

	/* TIM IT enable */
	TIM_ITConfig(MONITOR_TIM_BASE, TIM_IT_Update, ENABLE);
}

void monitor_timer_stop(void)
{
	TIM_Cmd(MONITOR_TIM_BASE, DISABLE);
}

void monitor_timer_start(void)
{
	TIM_Cmd(MONITOR_TIM_BASE, ENABLE);
}

void monitor_timer_isr(void)
{
	if (TIM_GetITStatus(MONITOR_TIM_BASE, TIM_IT_Update) != RESET)
	{
                if(g_str_bitflag.b3_haptic_status != STOP)
                {
                        g_str_timer.ui16_haptic_cnt++;
                }
                else
                {
                        g_str_timer.ui16_haptic_cnt = 0;
                }
          
#ifdef ENABLE_AUDIO
                g_str_timer.ui16_audio_cnt = (g_str_bitflag.b1_frame_state == AUDIO_FRAME_ERR)? g_str_timer.ui16_audio_cnt + 1:0;

		if (g_str_bitflag.b3_freq_chk == FREQ_CHK_START)
		{
			g_str_bitflag.b3_freq_chk = FREQ_CHK_ING;
			g_str_audio.ui16_freq_cnt = 0;
			g_str_audio.ui16_sec_cnt = 0;
		}
		else if (g_str_bitflag.b3_freq_chk == FREQ_CHK_ING)
		{
			g_str_audio.ui16_sec_cnt++;
		}

		if (g_str_audio.ui16_sec_cnt == 20)
		{
			g_str_audio.ui16_freq_cnt = 0;
		}
		else if (g_str_audio.ui16_sec_cnt == 30)
		{
			g_str_bitflag.b3_freq_chk = FREQ_CHK_ANALYZE;
			g_str_audio.ui16_sec_cnt = 0;
		}
#endif //ENABLE_AUDIO

		if (g_str_bitflag.b1_power_key_flag)
		{
			if (POWER_KEY_STATUS)
			{
				g_str_timer.ui16_power_cnt++;
			}
			else
			{
				g_str_bitflag.b1_power_key_flag = 0;
			}

			if (g_str_timer.ui16_power_cnt > 500)
			{
				g_str_bitflag.b1_power_down = 1;
				g_str_timer.ui16_power_cnt = 0;
			}
		}

		if ( (g_str_bitflag.b1_power_down) || (g_str_bitflag.b1_batt_low) )
		{
			g_str_timer.ui16_power_cnt++;
		}

#ifdef ENABLE_PWM

		if (g_str_pwr_on.ui16_timer_cnt)
		{
			pattern_power_on();
		}
		else
		{
			if (g_str_bitflag.b1_incoming_call)
			{
				pattern_incoming_call();
			}
			else
			{
#ifdef ENABLE_BATT_STATUS
				if ((g_str_app_noti.ui16_timer_cnt + g_str_bt_conn.ui16_timer_cnt + g_str_bt_disconn.ui16_timer_cnt) == 0)
				{
					//if (g_str_batt_level.ui16_timer_cnt || (g_str_chg.ui16_chg_status == CHG_STATUS_IN_CHARGING))
					if( !g_str_bitflag.b1_batt_low && !g_str_bitflag.b1_power_down && (g_str_batt_level.ui16_timer_cnt || (g_str_chg.ui16_chg_status == CHG_STATUS_IN_CHARGING)) )
					{
						pattern_batt_level();
					}
				}
#endif //ENABLE_BATT_STATUS

				if (g_str_app_noti.ui16_timer_cnt)
				{
					pattern_app_noti();
				}

				if (g_str_bt_conn.ui16_timer_cnt)
				{
					pattern_bt_conn();
				}

				if (g_str_bt_disconn.ui16_timer_cnt)
				{
					pattern_bt_disconn();
				}
			}
		}
#endif //ENABLE_PWM

#ifdef ENABLE_LOWPWR
#ifdef ENABLE_BATT_STATUS
		if (g_str_chg.ui16_chg_status != CHG_STATUS_IN_CHARGING)
#endif
			g_str_timer.ui16_sleep_cnt++;
#endif

#ifdef ENABLE_IS2063
		if (BTAPP_timer1ms)
			--BTAPP_timer1ms;

		if (BT_CommandSentMFBWaitTimer)
			--BT_CommandSentMFBWaitTimer;

		if (BT_CommandStartMFBWaitTimer)
		{
			--BT_CommandStartMFBWaitTimer;
			if (!BT_CommandStartMFBWaitTimer)
			{
			  	if ( g_str_bitflag.b1_power_mode_change )
				{
					BT_CommandStartMFBWaitTimer++;
				}
				else
				{
					if (BT_CMD_SendState == BT_CMD_SEND_MFB_HIGH_WAITING)
					{
						BT_CMD_SendState = BT_CMD_SEND_DATA_SENDING;
						UART_TransferFirstByte();
					}
				}
			}
		}
#endif

#ifdef ENABLE_PEDOMETER
		if (g_ui32_10ms_count != 0x00)
			g_ui32_10ms_count--;

		if (g_ui32_pedometer_timer++ >= PEDOMETER_PERIOD) // PEDOMETER_PERIOD / 1000 = second
		{
			g_ui32_pedometer_call_app = 1;
			g_ui32_mcu_hour = 1;
		}
#endif //ENABLE_PEDOMETER
		TIM_ClearITPendingBit(MONITOR_TIM_BASE, TIM_IT_Update);
	}
}

void delay_10ms(uint32_t nTime)
{
	g_ui32_10ms_count = nTime;
	while (g_ui32_10ms_count != 0);
}
