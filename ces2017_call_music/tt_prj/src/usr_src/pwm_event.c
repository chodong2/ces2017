#include "global_define.h"

#ifdef ENABLE_PWM

//static const GPIO_InitTypeDef HAPTIC_GpioConfiguration  =  {PWM_HAPTIC_GPIO, GPIO_Mode_OUT,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};

static const GPIO_InitTypeDef HAPTIC_GpioConfiguration  =  {PWM_HAPTIC_GPIO, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_DOWN};

static const GPIO_InitTypeDef LED1_GpioConfiguration  =  {PWM_LED1_GPIO, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
static const GPIO_InitTypeDef LED2_GpioConfiguration  =  {PWM_LED2_GPIO, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
static const GPIO_InitTypeDef LED3_GpioConfiguration  =  {PWM_LED3_GPIO, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
static const GPIO_InitTypeDef LED4_GpioConfiguration  =  {PWM_LED4_GPIO, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
static const GPIO_InitTypeDef LED5_GpioConfiguration  =  {PWM_LED5_GPIO, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};

void pwm_init( void )
{
	pwm_config_gpio();

	pwm_config_tim1_output();
	pwm_config_tim3_output();
}

void pwm_config_gpio( void )
{
	/* TIM1 clock enable */
	RCC_APB2PeriphClockCmd(PWM_TIM1_GPIO_RCC, ENABLE);
	
	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(PWM_TIM3_GPIO_RCC, ENABLE);

	/* GPIOA, GPIOB clock enable */
	RCC_AHB1PeriphClockCmd(PWM_GPIO_CLK , ENABLE);

	GPIO_Init(PWM_HAPTIC_GPIO_PORT, (GPIO_InitTypeDef *)&HAPTIC_GpioConfiguration );
	GPIO_PinAFConfig(PWM_HAPTIC_GPIO_PORT, PWM_HAPTIC_GPIO_SOURCE, GPIO_AF_TIM1);
	
	GPIO_Init(PWM_LED1_GPIO_PORT, (GPIO_InitTypeDef *)&LED1_GpioConfiguration );
	GPIO_PinAFConfig(PWM_LED1_GPIO_PORT, PWM_LED1_GPIO_SOURCE, GPIO_AF_TIM1);
	
	GPIO_Init(PWM_LED2_GPIO_PORT, (GPIO_InitTypeDef *)&LED2_GpioConfiguration );
	GPIO_PinAFConfig(PWM_LED2_GPIO_PORT, PWM_LED2_GPIO_SOURCE, GPIO_AF_TIM1); 
	
	GPIO_Init(PWM_LED3_GPIO_PORT, (GPIO_InitTypeDef *)&LED3_GpioConfiguration );
	GPIO_PinAFConfig(PWM_LED3_GPIO_PORT, PWM_LED3_GPIO_SOURCE, GPIO_AF_TIM3);
	
	GPIO_Init(PWM_LED4_GPIO_PORT, (GPIO_InitTypeDef *)&LED4_GpioConfiguration );
	GPIO_PinAFConfig(PWM_LED4_GPIO_PORT, PWM_LED4_GPIO_SOURCE, GPIO_AF_TIM3);
	
	GPIO_Init(PWM_LED5_GPIO_PORT, (GPIO_InitTypeDef *)&LED5_GpioConfiguration );
	GPIO_PinAFConfig(PWM_LED5_GPIO_PORT, PWM_LED5_GPIO_SOURCE, GPIO_AF_TIM3);

}

void pwm_config_tim1_output( void )
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	uint16_t CCR_Val = 100;

	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 100-1;
	TIM_TimeBaseStructure.TIM_Prescaler = 20000-1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

	/* haptic */
	/* PWM1 Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	
	/* LED1 */
	/* PWM1 Mode configuration: Channel3 */
	TIM_OCInitStructure.TIM_Pulse = CCR_Val;
	TIM_OC3Init(TIM1, &TIM_OCInitStructure);

	/* LED2 */
	/* PWM1 Mode configuration: Channel4 */
	TIM_OC4Init(TIM1, &TIM_OCInitStructure);

	/* TIM1 enable counter */
	TIM_Cmd(TIM1, ENABLE);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
}

void pwm_config_tim3_output( void )
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	
	uint16_t CCR_Val = 100;

	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 100-1;
	TIM_TimeBaseStructure.TIM_Prescaler = 50-1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* PWM1 Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR_Val;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

	/* LED3 */
	TIM_OC2Init(TIM3, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
	
	/* LED4 */
	TIM_OC3Init(TIM3, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);
	
	/* LED5 */
	TIM_OC4Init(TIM3, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);

	TIM_ARRPreloadConfig(TIM3, ENABLE);

	/* TIM3 enable counter */
	TIM_Cmd(TIM3, ENABLE);
	
}

void pwm_pulse_off( void )
{
	PWM_HAPTIC_CCR = 0;

	PWM_LED1_CCR = 100;
	PWM_LED2_CCR = 100;
	PWM_LED3_CCR = 100;
	PWM_LED4_CCR = 100;
	PWM_LED5_CCR = 100;
}

void pwm_pulse_led_all(uint16_t ui16_pulse)
{
	ui16_pulse = 100 - ui16_pulse;
	if( ui16_pulse <= 100 )
	{
		PWM_LED1_CCR = ui16_pulse;
		PWM_LED2_CCR = ui16_pulse;
		PWM_LED3_CCR = ui16_pulse;
		PWM_LED4_CCR = ui16_pulse;
		PWM_LED5_CCR = ui16_pulse;
	}
}


void pwm_pulse_haptic(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		PWM_HAPTIC_CCR = ui16_pulse;
	}
}

void pwm_pulse_led1(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		PWM_LED1_CCR = 100 - ui16_pulse;
	}
}

void pwm_pulse_led2(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		PWM_LED2_CCR = 100 - ui16_pulse;
	}
}

void pwm_pulse_led3(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		PWM_LED3_CCR = 100 - ui16_pulse;
	}
}

void pwm_pulse_led4(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		PWM_LED4_CCR = 100 - ui16_pulse;
	}
}

void pwm_pulse_led5(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		PWM_LED5_CCR = 100 - ui16_pulse;
	}
}

#ifdef ENABLE_TEST

void pwm_pulse_test(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		PWM_LED1_CCR = ui16_pulse;
		PWM_LED2_CCR = ui16_pulse;
		PWM_LED3_CCR = ui16_pulse;
		PWM_LED4_CCR = ui16_pulse;
		PWM_LED5_CCR = ui16_pulse;
	}
}

#endif //ENABLE_TEST

#endif //ENABLE_PWM

