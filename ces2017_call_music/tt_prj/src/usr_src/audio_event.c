#include "global_define.h"

#ifdef ENABLE_AUDIO

void audio_init(uint16_t audio)
{
	audio_i2s_config_rx(audio);
	audio_i2s_config_tx(audio);
}

void audio_i2s_config_rx(uint16_t audio)
{
	I2S_InitTypeDef I2S_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
        uint16_t freq = audio;

	/* Enable GPIO clocks */
	RCC_APB1PeriphClockCmd(AUDIO_I2S_RX_CLK, ENABLE);
	RCC_AHB1PeriphClockCmd(AUDIO_I2S_RX_GPIO_CLOCK, ENABLE);

	/* I2S GPIO Configuration --------------------------------------------------*/
	/* Connect I2S pins to Alternate functions */
	GPIO_InitStructure.GPIO_Pin = AUDIO_I2S_RX_WS_PIN | AUDIO_I2S_RX_CK_PIN | AUDIO_I2S_RX_SD_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(AUDIO_I2S_RX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_PinAFConfig(AUDIO_I2S_RX_GPIO_PORT, AUDIO_I2S_RX_WS_SOURCE, AUDIO_I2S_RX_GPIO_AF);
	GPIO_PinAFConfig(AUDIO_I2S_RX_GPIO_PORT, AUDIO_I2S_RX_CK_SOURCE, AUDIO_I2S_RX_GPIO_AF);
	GPIO_PinAFConfig(AUDIO_I2S_RX_GPIO_PORT, AUDIO_I2S_RX_SD_SOURCE, AUDIO_I2S_RX_GPIO_AF);

	/* I2S configuration -------------------------------------------------------*/
	SPI_I2S_DeInit(AUDIO_I2S_RX);
	/* Configure the Audio Frequency, Standard and the data format */
	I2S_InitStructure.I2S_Standard = I2S_Standard_Phillips;
	I2S_InitStructure.I2S_DataFormat = I2S_DataFormat_16bextended;
	I2S_InitStructure.I2S_MCLKOutput = I2S_MCLKOutput_Disable;
        
        switch(freq)
        {
                case AUDIO_INPUT_8K:    I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_8k;     break;
                case AUDIO_INPUT_16K:   I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_16k;    break;
                case AUDIO_INPUT_44_1K: I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_44k;    break;
                case AUDIO_INPUT_48K:   I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_48k;    break;
        }
                
        I2S_InitStructure.I2S_CPOL = I2S_CPOL_Low;
	I2S_InitStructure.I2S_Mode = I2S_Mode_SlaveRx;
	I2S_Init(AUDIO_I2S_RX, &I2S_InitStructure);

	/* SPI2 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = AUDIO_I2S_RX_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_RXNE, ENABLE);
	SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_TIFRFE, ENABLE);
}

void audio_i2s_config_tx(uint16_t audio)
{
	I2S_InitTypeDef I2S_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
        uint16_t freq = audio;

	/* Enable GPIO clocks */
	RCC_AHB1PeriphClockCmd(AUDIO_I2S_TX_GPIO_CLOCK, ENABLE);
	RCC_APB1PeriphClockCmd(AUDIO_I2S_TX_CLK, ENABLE);

	/* I2S GPIO Configuration --------------------------------------------------*/
	/* Connect I2S pins to Alternate functions */
	GPIO_InitStructure.GPIO_Pin = AUDIO_I2S_TX_MCK_PIN | AUDIO_I2S_TX_SD_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

	GPIO_PinAFConfig(AUDIO_I2S_TX_GPIO, AUDIO_I2S_TX_MCK_SOURCE, AUDIO_I2S_TX_GPIO_AF);
	GPIO_PinAFConfig(AUDIO_I2S_TX_GPIO, AUDIO_I2S_TX_SD_SOURCE, AUDIO_I2S_TX_GPIO_AF);
	GPIO_Init(AUDIO_I2S_TX_GPIO, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = AUDIO_I2S_TX_SCK_PIN;
	GPIO_PinAFConfig(AUDIO_I2S_TX_GPIO, AUDIO_I2S_TX_SCK_SOURCE, AUDIO_I2S_TX_SCK_GPIO_AF);
	GPIO_Init(AUDIO_I2S_TX_GPIO, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = AUDIO_I2S_TX_WS_PIN;
	GPIO_PinAFConfig(AUDIO_I2S_TX_WS_GPIO, AUDIO_I2S_TX_WS_SOURCE, AUDIO_I2S_TX_GPIO_AF);
	GPIO_Init(AUDIO_I2S_TX_WS_GPIO, &GPIO_InitStructure);

	/* I2S configuration -------------------------------------------------------*/

	/* Initialize  I2S3 peripherals */
	SPI_I2S_DeInit(AUDIO_I2S_TX);
	/* Configure the Audio Frequency, Standard and the data format */
	I2S_InitStructure.I2S_Standard = I2S_Standard_Phillips;
	I2S_InitStructure.I2S_DataFormat = I2S_DataFormat_16b;
	I2S_InitStructure.I2S_MCLKOutput = I2S_MCLKOutput_Enable;

        switch(freq)
        {
                case AUDIO_INPUT_8K:    I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_8k;     break;
                case AUDIO_INPUT_16K:   I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_16k;    break;
                case AUDIO_INPUT_44_1K: I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_44k;    break;
                case AUDIO_INPUT_48K:   I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_48k;    break;
        }
                
	I2S_InitStructure.I2S_CPOL = I2S_CPOL_Low;
	I2S_InitStructure.I2S_Mode = I2S_Mode_MasterTx;

	I2S_Init(AUDIO_I2S_TX, &I2S_InitStructure);

	/* SPI IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = AUDIO_I2S_TX_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	SPI_I2S_ITConfig(AUDIO_I2S_TX, SPI_I2S_IT_TXE, ENABLE);
}

void audio_rx_start(void)
{
	I2S_Cmd(AUDIO_I2S_RX, ENABLE);
	//I2S_Cmd(AUDIO_I2S_TX, ENABLE);
	//AMP_STATUS_ENABLE;
}

void audio_rx_stop(void)
{
	I2S_Cmd(AUDIO_I2S_RX, DISABLE);
}

void audio_rx_isr(void)
{
	//static uint16_t ui16_i2s_rx_buf[12] = { 0, };

        static uint16_t ui16_i2s_rx_buf[160] = { 0, };
	static uint16_t ui16_rx_idx = 0;

#ifdef ENABLE_LOWPWR
	g_str_timer.ui16_sleep_cnt = 0;
#endif

	if (SPI_I2S_GetITStatus(AUDIO_I2S_RX, SPI_I2S_IT_TIFRFE) != RESET)
	{
		I2S_Cmd(AUDIO_I2S_RX, DISABLE);
		I2S_Cmd(AUDIO_I2S_TX, DISABLE);
		AMP_STATUS_DISABLE;
		g_str_bitflag.b1_audio_state = AUDIO_STATE_STOP;
		g_str_bitflag.b1_frame_state = AUDIO_FRAME_ERR;
		g_str_timer.ui16_audio_cnt = 0;

		SPI_I2S_ClearITPendingBit(AUDIO_I2S_RX, SPI_I2S_IT_TIFRFE);
	}

	/* rx I2S interrupt */
	else if (SPI_I2S_GetITStatus(AUDIO_I2S_RX, SPI_I2S_IT_RXNE) != RESET)
	{
                switch(g_str_bitflag.b3_freq_chk)
                {
                        case FREQ_CHK_READY:
                                g_str_bitflag.b3_freq_chk = FREQ_CHK_START;
                                break;

                        case FREQ_CHK_ING:
                                g_str_audio.ui16_freq_cnt++;
                                break;
                                
                        default:
                                break;
                }
          
                ui16_i2s_rx_buf[ui16_rx_idx] = SPI_I2S_ReceiveData(AUDIO_I2S_RX);

                if( g_str_audio.ui16_audio_type <= AUDIO_INPUT_16K )
                {
                        if (ui16_rx_idx % 2)
                        {
                                g_str_audio.ui16_save_buf[g_str_audio.ui16_save_cnt++] = ui16_i2s_rx_buf[ui16_rx_idx];
                        }
                }
                else if( g_str_audio.ui16_audio_type >= AUDIO_INPUT_44_1K )
                        g_str_audio.ui16_save_buf[g_str_audio.ui16_save_cnt++] = ui16_i2s_rx_buf[ui16_rx_idx];

		ui16_rx_idx++;
                
                if (g_str_audio.ui16_save_cnt >= AUDIO_FRAME_SIZE)
                {
                        if( g_str_bitflag.b1_audio_state == AUDIO_STATE_STOP )
                        {
                                I2S_Cmd(AUDIO_I2S_TX, ENABLE);
                                AMP_STATUS_ENABLE;
                                g_str_bitflag.b1_audio_state = AUDIO_STATE_PLAY;
                        }
                        g_str_audio.ui16_save_cnt = 0;
                        ui16_rx_idx = 0;
                        g_str_bitflag.b1_frame_state = AUDIO_FRAME_OK;
                        g_str_timer.ui16_audio_cnt = 0;

                        if( g_str_audio.ui16_audio_type <= AUDIO_INPUT_16K )
                                g_str_bitflag.b1_fe_enable_flag = 1;
                }
                
		SPI_I2S_ClearITPendingBit(AUDIO_I2S_RX, SPI_I2S_IT_RXNE);
	}

}

void audio_tx_isr(void)
{
	static uint16_t ui16_tx_idx = 0;
	static volatile uint16_t *p_buffer;

#ifdef ENABLE_LOWPWR
	g_str_timer.ui16_sleep_cnt = 0;
#endif

	if (SPI_I2S_GetITStatus(AUDIO_I2S_TX, SPI_I2S_IT_TXE) != RESET)
	{
                if( g_str_audio.ui16_audio_type <= AUDIO_INPUT_16K )
                {
                        SPI_I2S_SendData(AUDIO_I2S_TX, p_buffer[ui16_tx_idx++]);
                        if (ui16_tx_idx >= AUDIO_FRAME_SIZE*2)
                        {
                                ui16_tx_idx = 0;

                                if (g_str_fe.ui16_frame_no % 2)
                                {
                                        p_buffer = g_str_fe.ui16_data;
                                }
                                else
                                {
                                        p_buffer = g_str_fe.ui16_data2;
                                }
                        }
                }
                else if( g_str_audio.ui16_audio_type >= AUDIO_INPUT_44_1K )
                {
                        SPI_I2S_SendData(AUDIO_I2S_TX, g_str_audio.ui16_save_buf[ui16_tx_idx++]);
                        if (ui16_tx_idx >= AUDIO_FRAME_SIZE)
                        {
                                ui16_tx_idx = 0;
                        }
                }

		SPI_I2S_ClearITPendingBit(AUDIO_I2S_TX, SPI_I2S_IT_TXE);
	}
}

#endif //ENABLE_AUDIO

