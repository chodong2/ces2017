#include "global_define.h"

void key_scan( void )
{
	switch(g_key_event)
	{
	case KEY_EVENT_POWER :
		if( g_key_state & KEY_POWER_PRESS )
		{
			g_str_timer.ui16_power_cnt = 0;
			g_str_bitflag.b1_power_key_flag = 1;

			if( g_str_bitflag.b1_incoming_call )
  			{
			#ifdef ENABLE_LOWPWR
				set_clk_high();
			#endif
	  			g_str_bitflag.b1_incoming_call = 0;
			}
			else
			{
			#ifdef ENABLE_LOWPWR
				set_clk_low();
			#endif
			}
			
#ifdef ENABLE_PWM
			pwm_pulse_off();
#endif
			
			g_key_state &= ~KEY_POWER_PRESS;
		}
		
		if( g_key_state & KEY_POWER_RELEASE )
		{
			g_str_timer.ui16_power_cnt = 0;
#ifdef ENABLE_IS2063

			if( BTAPP_GetStatus() == BT_STATUS_OFF )
			{
				BTAPP_TaskReq( BT_REQ_SYSTEM_ON );
			}
			else
			{
				#if 1
				//BT_ReadBTMBattStatus();
				g_str_batt_level.ui16_timer_cnt = 350;
				#endif
			}
                        
                        if(g_str_bitflag.b3_haptic_status == CALL_INCOMING)
                        {
                                //BTAPP_CallEventShort();
                                BT_MMI_ActionCommand(ACCEPT_CALL, 0);
                                BT_MMI_ActionCommand(UNMUTE_MIC, 0);
                                pwm_pulse_off();
                        }
                        else if(g_str_bitflag.b3_haptic_status == CALL_CALLING)
                        {
                                //BTAPP_CallEventShort();
                                BT_MMI_ActionCommand(FORCE_END_CALL, 0);
                                BT_MMI_ActionCommand(MUTE_MIC, 0);
                        }
#endif
			g_key_state &= ~KEY_POWER_RELEASE;
		}
		
		g_key_event = KEY_EVENT_NONE;

		break;
	
	case KEY_EVENT_SOURCE :
		if( g_key_state & KEY_SOURCE_UP )
		{
			g_key_state &= ~KEY_SOURCE_UP;
		}
		
		if( g_key_state & KEY_SOURCE_DOWN )
		{
			g_key_state &= ~KEY_SOURCE_DOWN;
		}
		
		g_key_event = KEY_EVENT_NONE;

		break;
	
	case KEY_EVENT_VOLUP :
		if( g_key_state == KEY_PAIR )
		{
#ifdef ENABLE_IS2063
			BT_MMI_ActionCommand(0x56, 0); 
			BT_MMI_ActionCommand(0x5D, 0); 
#endif //ENABLE_IS2063
			g_key_state &= ~KEY_PAIR;
		}

		if( g_key_state & KEY_VOLUP_PRESS )
		{
		}
		
		if( g_key_state & KEY_VOLUP_RELEASE )
		{
#ifdef ENABLE_IS2063
                        //BTAPP_VolUp();
#endif
			g_key_state &= ~KEY_VOLUP_PRESS;
			g_key_state &= ~KEY_VOLUP_RELEASE;
			g_key_event = KEY_EVENT_NONE;
		}
		
		break;
	case KEY_EVENT_VOLDN :
		if( g_key_state == KEY_PAIR )
		{
#ifdef ENABLE_IS2063
			BT_MMI_ActionCommand(0x56, 0); 
			BT_MMI_ActionCommand(0x5D, 0); 
#endif //ENABLE_IS2063
			g_key_state &= ~KEY_PAIR;
		}

		if( g_key_state & KEY_VOLDN_PRESS )
		{
		}

		if( g_key_state & KEY_VOLDN_RELEASE )
		{
#ifdef ENABLE_IS2063
                        //BTAPP_VolDown();
#endif
			g_key_state &= ~KEY_VOLDN_PRESS;
			g_key_state &= ~KEY_VOLDN_RELEASE;
			g_key_event = KEY_EVENT_NONE;
		}
		break;
	
	}
}
