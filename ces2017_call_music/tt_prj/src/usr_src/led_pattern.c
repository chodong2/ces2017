#include "global_define.h"

#ifdef ENABLE_PWM

void pattern_batt_level(void)
{
#ifdef ENABLE_BATT_STATUS
	pwm_struct_t *p_pwm = &g_str_batt_level.str_pwm;
	if (p_pwm->ui16_dimming_flag)
	{
		p_pwm->ui16_dimming_data++;
	}
	else
	{
		p_pwm->ui16_dimming_data--;
	}

	if (p_pwm->ui16_dimming_data > 65)
	{
		p_pwm->ui16_dimming_data = 65;
		p_pwm->ui16_dimming_flag = 0;
	}
	else if (p_pwm->ui16_dimming_data < 3)
	{
		p_pwm->ui16_dimming_flag = 1;
	}

	if (p_pwm->ui16_dimming_data > 65)
	{
		p_pwm->ui16_pwm = 95;
	}
	else
	{
		p_pwm->ui16_pwm = (p_pwm->ui16_dimming_data * 3) >> 1;
	}

	switch (g_str_batt.ui16_batt_level)
	{
	case BATT_LEVEL_3_0:
	case BATT_LEVEL_3_1:
	case BATT_LEVEL_3_2:
	case BATT_LEVEL_3_3:
	case BATT_LEVEL_3_4:
	case BATT_LEVEL_3_5:
		pwm_pulse_off();
		pwm_pulse_led1(p_pwm->ui16_pwm);
                //pwm_pulse_haptic(p_pwm->ui16_pwm);
		break;
                
	case BATT_LEVEL_3_6:
	case BATT_LEVEL_3_7:
		pwm_pulse_off();
		pwm_pulse_led1(95);
		pwm_pulse_led2(p_pwm->ui16_pwm);
                //pwm_pulse_haptic(p_pwm->ui16_pwm);
		break;
                
	case BATT_LEVEL_3_8:
	case BATT_LEVEL_3_9:
		pwm_pulse_off();
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(p_pwm->ui16_pwm);
                //pwm_pulse_haptic(p_pwm->ui16_pwm);
		break;
                
	case BATT_LEVEL_4_0:
	case BATT_LEVEL_4_1:
		pwm_pulse_off();
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(95);
		pwm_pulse_led4(p_pwm->ui16_pwm);
                //pwm_pulse_haptic(p_pwm->ui16_pwm);
		break;

	case BATT_LEVEL_4_2:
		pwm_pulse_off();
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(95);
		pwm_pulse_led4(95);
		pwm_pulse_led5(p_pwm->ui16_pwm);
                //pwm_pulse_haptic(p_pwm->ui16_pwm);
		break;
	default:
		break;
	}

        if(g_str_batt.ui16_batt_level <= PWR_OFF_VOL)
        {
                g_str_bitflag.b1_batt_low = 1;
        }
        
	if (g_str_chg.ui16_chg_status != CHG_STATUS_IN_CHARGING)
	{
		g_str_batt_level.ui16_timer_cnt--;
	}
        else
        {
                g_str_bitflag.b1_batt_low = 0;
                g_str_timer.ui16_power_cnt = 0;
        }

	if (g_str_batt_level.ui16_timer_cnt == 0)
	{
		pwm_pulse_off();
	}
}

void pattern_app_noti(void)
{
#if 0
	pwm_struct_t *p_pwm = &g_str_app_noti.str_pwm;
	if (p_pwm->ui16_dimming_flag)
	{
		p_pwm->ui16_dimming_data++;
	}
	else
	{
		p_pwm->ui16_dimming_data--;
	}

	if (p_pwm->ui16_dimming_data > 65)
	{
		p_pwm->ui16_dimming_data = 65;
		p_pwm->ui16_dimming_flag = 0;
	}
	else if (p_pwm->ui16_dimming_data < 3)
	{
		p_pwm->ui16_dimming_flag = 1;
	}

	if (p_pwm->ui16_dimming_data > 65)
	{
		p_pwm->ui16_pwm = 95;
	}
	else
	{
		p_pwm->ui16_pwm = (p_pwm->ui16_dimming_data * 3) >> 1;
	}

	pwm_pulse_led3(p_pwm->ui16_pwm);
#else
	if (g_str_app_noti.ui16_timer_cnt > 500)
	{
		pwm_pulse_led_all(100);
	}
	else if (g_str_app_noti.ui16_timer_cnt > 450)
	{
		pwm_pulse_off();
	}
	else if (g_str_app_noti.ui16_timer_cnt > 400)
	{
		pwm_pulse_led_all(100);
	}
	else if (g_str_app_noti.ui16_timer_cnt > 350)
	{
		pwm_pulse_off();
	}
	else if (g_str_app_noti.ui16_timer_cnt > 300)
	{
		pwm_pulse_led_all(100);
	}
	else if (g_str_app_noti.ui16_timer_cnt > 250)
	{
		pwm_pulse_off();
	}
	else if (g_str_app_noti.ui16_timer_cnt > 200)
	{
		pwm_pulse_led_all(100);
	}
	else if (g_str_app_noti.ui16_timer_cnt > 120)
	{
		pwm_pulse_off();
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100);
		pwm_pulse_led4(100);
	}
	else if (g_str_app_noti.ui16_timer_cnt > 90)
	{
		pwm_pulse_off();
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100);
	}
	else if (g_str_app_noti.ui16_timer_cnt > 60)
	{
		pwm_pulse_off();
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
	}
	else if (g_str_app_noti.ui16_timer_cnt > 30)
	{
		pwm_pulse_off();
		pwm_pulse_led1(100);
	}
	else
	{
		pwm_pulse_off();
	}

#endif

	g_str_app_noti.ui16_timer_cnt--;

	if (g_str_app_noti.ui16_timer_cnt == 0)
	{
		pwm_pulse_off();
	}
#endif //ENABLE_BATT_STATUS
}

void pattern_bt_conn(void)
{
	pwm_struct_t *p_pwm = &g_str_bt_conn.str_pwm;

	if (p_pwm->ui16_pwm > 95)
	{
		p_pwm->ui16_pwm = 95;
	}

	else if (p_pwm->ui16_pwm < 5)
	{
		p_pwm->ui16_pwm = 5;
	}

	if (g_str_bt_conn.ui16_timer_cnt > 330)
	{
		pwm_pulse_off();
		pwm_pulse_led1(p_pwm->ui16_pwm++);
		pwm_pulse_led5(p_pwm->ui16_pwm++);
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 300)
	{
		pwm_pulse_led2(100 - p_pwm->ui16_pwm--);
		pwm_pulse_led4(100 - p_pwm->ui16_pwm--);
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 270)
	{
		pwm_pulse_led3(p_pwm->ui16_pwm++);
		p_pwm->ui16_pwm++;
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 240)
	{
		pwm_pulse_off();
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 210)
	{
		pwm_pulse_off();
		pwm_pulse_led1(p_pwm->ui16_pwm++);
		pwm_pulse_led5(p_pwm->ui16_pwm++);
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 180)
	{
		pwm_pulse_led2(100 - p_pwm->ui16_pwm--);
		pwm_pulse_led4(100 - p_pwm->ui16_pwm--);
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 150)
	{
		pwm_pulse_led3(p_pwm->ui16_pwm++);
		p_pwm->ui16_pwm++;
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 120)
	{
		pwm_pulse_off();
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 90)
	{
		pwm_pulse_off();
		pwm_pulse_led1(p_pwm->ui16_pwm++);
		pwm_pulse_led5(p_pwm->ui16_pwm++);
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 60)
	{
		pwm_pulse_led2(100 - p_pwm->ui16_pwm--);
		pwm_pulse_led4(100 - p_pwm->ui16_pwm--);
	}
	else if (g_str_bt_conn.ui16_timer_cnt > 30)
	{
		pwm_pulse_led3(p_pwm->ui16_pwm++);
		p_pwm->ui16_pwm++;
	}
	else
	{
		pwm_pulse_off();
	}

	g_str_bt_conn.ui16_timer_cnt--;

	if (g_str_bt_conn.ui16_timer_cnt == 0)
	{
		pwm_pulse_off();
	}
}

void pattern_bt_disconn(void)
{
	pwm_struct_t *p_pwm = &g_str_bt_disconn.str_pwm;

	if (p_pwm->ui16_pwm > 95)
	{
		p_pwm->ui16_pwm = 95;
	}

	else if (p_pwm->ui16_pwm < 5)
	{
		p_pwm->ui16_pwm = 5;
	}

	if (g_str_bt_disconn.ui16_timer_cnt > 330)
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100 - p_pwm->ui16_pwm++);
		pwm_pulse_led4(100);
		pwm_pulse_led5(100);

		p_pwm->ui16_pwm++;
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 300)
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(p_pwm->ui16_pwm--);
		pwm_pulse_led3(0);
		pwm_pulse_led4(p_pwm->ui16_pwm--);
		pwm_pulse_led5(100);
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 270)
	{
		pwm_pulse_led1(100 - p_pwm->ui16_pwm++);
		pwm_pulse_led2(0);
		pwm_pulse_led3(0);
		pwm_pulse_led4(0);
		pwm_pulse_led5(100 - p_pwm->ui16_pwm++);
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 240)
	{
		pwm_pulse_off();
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 210)
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100 - p_pwm->ui16_pwm++);
		pwm_pulse_led4(100);
		pwm_pulse_led5(100);

		p_pwm->ui16_pwm++;
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 180)
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(p_pwm->ui16_pwm--);
		pwm_pulse_led3(0);
		pwm_pulse_led4(p_pwm->ui16_pwm--);
		pwm_pulse_led5(100);
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 150)
	{
		pwm_pulse_led1(100 - p_pwm->ui16_pwm++);
		pwm_pulse_led2(0);
		pwm_pulse_led3(0);
		pwm_pulse_led4(0);
		pwm_pulse_led5(100 - p_pwm->ui16_pwm++);
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 120)
	{
		pwm_pulse_off();
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 90)
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100 - p_pwm->ui16_pwm++);
		pwm_pulse_led4(100);
		pwm_pulse_led5(100);

		p_pwm->ui16_pwm++;
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 60)
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(p_pwm->ui16_pwm--);
		pwm_pulse_led3(0);
		pwm_pulse_led4(p_pwm->ui16_pwm--);
		pwm_pulse_led5(100);
	}
	else if (g_str_bt_disconn.ui16_timer_cnt > 30)
	{
		pwm_pulse_led1(100 - p_pwm->ui16_pwm++);
		pwm_pulse_led2(0);
		pwm_pulse_led3(0);
		pwm_pulse_led4(0);
		pwm_pulse_led5(100 - p_pwm->ui16_pwm++);
	}
	else
	{
		pwm_pulse_off();
	}

	g_str_bt_disconn.ui16_timer_cnt--;

	if (g_str_bt_disconn.ui16_timer_cnt == 0)
	{
		pwm_pulse_off();
	}
}

void pattern_power_on(void)
{
	pwm_struct_t *p_pwm = &g_str_pwr_on.str_pwm;

	if (p_pwm->ui16_pwm > 95)
	{
		p_pwm->ui16_pwm = 95;
	}

	else if (p_pwm->ui16_pwm < 5)
	{
		p_pwm->ui16_pwm = 5;
	}

	if (g_str_pwr_on.ui16_timer_cnt > 300)
	{
		pwm_pulse_off();
		pwm_pulse_led1(p_pwm->ui16_pwm++);
		p_pwm->ui16_pwm++;
	}
	else if (g_str_pwr_on.ui16_timer_cnt > 270)
	{
		pwm_pulse_led2(100 - p_pwm->ui16_pwm--);
		p_pwm->ui16_pwm--;
	}
	else if (g_str_pwr_on.ui16_timer_cnt > 240)
	{
		pwm_pulse_led3(p_pwm->ui16_pwm++);
		p_pwm->ui16_pwm++;
	}
	else if (g_str_pwr_on.ui16_timer_cnt > 210)
	{
		pwm_pulse_led4(100 - p_pwm->ui16_pwm--);
		p_pwm->ui16_pwm--;
	}
	else if (g_str_pwr_on.ui16_timer_cnt > 180)
	{
		pwm_pulse_led5(p_pwm->ui16_pwm++);
		p_pwm->ui16_pwm++;
	}
	else if (g_str_pwr_on.ui16_timer_cnt > 150)
	{
		pwm_pulse_off();
		pwm_pulse_led1(p_pwm->ui16_pwm++);
		p_pwm->ui16_pwm++;
	}
	else if (g_str_pwr_on.ui16_timer_cnt > 120)
	{
		pwm_pulse_led2(100 - p_pwm->ui16_pwm--);
		p_pwm->ui16_pwm--;
	}
	else if (g_str_pwr_on.ui16_timer_cnt > 90)
	{
		pwm_pulse_led3(p_pwm->ui16_pwm++);
		p_pwm->ui16_pwm++;
	}
	else if (g_str_pwr_on.ui16_timer_cnt > 60)
	{
		pwm_pulse_led4(100 - p_pwm->ui16_pwm--);
		p_pwm->ui16_pwm--;
	}
	else if (g_str_pwr_on.ui16_timer_cnt > 30)
	{
		pwm_pulse_led5(p_pwm->ui16_pwm++);
		p_pwm->ui16_pwm++;
	}
	else
	{
		pwm_pulse_off();
	}

	g_str_pwr_on.ui16_timer_cnt--;

	if (g_str_pwr_on.ui16_timer_cnt == 0)
	{
		pwm_pulse_off();
	}
}

void pattern_incoming_call(void)
{
	pwm_struct_t *p_pwm = &g_str_incomming_call.str_pwm;

	static uint16_t ui16_level;

	if (p_pwm->ui16_dimming_flag)
	{
		p_pwm->ui16_dimming_data++;
	}
	else
	{
		p_pwm->ui16_dimming_data--;
	}

	if (p_pwm->ui16_dimming_data > 33)
	{
		p_pwm->ui16_dimming_data = 33;
		p_pwm->ui16_dimming_flag = 0;

		ui16_level++;
	}
	else if (p_pwm->ui16_dimming_data < 3)
	{
		p_pwm->ui16_dimming_flag = 1;
		ui16_level++;
	}

	if (p_pwm->ui16_dimming_data > 33)
	{
		p_pwm->ui16_pwm = 95;
	}
	else
	{
		p_pwm->ui16_pwm = p_pwm->ui16_dimming_data * 3;
	}

	if (!g_str_bitflag.b1_incoming_call)
	{
		g_str_incomming_call.ui16_timer_cnt--;
	}

	if (g_str_incomming_call.ui16_timer_cnt == 0)
	{
		pwm_pulse_off();
	}

	switch (ui16_level)
	{
	case 0:
		pwm_pulse_off();
		pwm_pulse_led1(p_pwm->ui16_pwm);
		break;
	case 1:
		pwm_pulse_off();
		pwm_pulse_led1(100);
		pwm_pulse_led2(100 - p_pwm->ui16_pwm);
		break;
	case 2:
		pwm_pulse_off();
		pwm_pulse_led1(100 - p_pwm->ui16_pwm);
		pwm_pulse_led2(100);
		pwm_pulse_led3(p_pwm->ui16_pwm);
		break;
	case 3:
		pwm_pulse_off();
		pwm_pulse_led2(p_pwm->ui16_pwm);
		pwm_pulse_led3(100);
		pwm_pulse_led4(100 - p_pwm->ui16_pwm);
		break;
	case 4:
		pwm_pulse_off();
		pwm_pulse_led3(100 - p_pwm->ui16_pwm);
		pwm_pulse_led4(100);
		pwm_pulse_led5(p_pwm->ui16_pwm);
		break;
	case 5:
		pwm_pulse_off();
		pwm_pulse_led4(p_pwm->ui16_pwm);
		pwm_pulse_led5(100);
		break;
	default:
		ui16_level = 0;
		break;
	}
}

#endif //ENABLE_PWM
