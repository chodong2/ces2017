/******************************************************************************
 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") for its PICmicro(r) Microcontroller is intended and
 supplied to you, the Company's customer, for use solely and
 exclusively on Microchip PICmicro Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
********************************************************************/

#include "global_define.h"

#ifdef ENABLE_IS2063

#define UR_TX_BUF_SIZE              300
static uint8_t          UR_TxBuf[UR_TX_BUF_SIZE];
static uint16_t         UR_TxBufHead;
static uint16_t         UR_TxBufTail;
typedef enum {
	TXRX_BUF_EMPTY,
	TXRX_BUF_OK,
	TXRX_BUF_FULL
} TXRX_BUF_STATUS;
static TXRX_BUF_STATUS  UR_TxBufStatus;

BT_CMD_SEND_STATE BT_CMD_SendState;
uint8_t BT_CommandStartMFBWaitTimer;
uint8_t BT_CommandSentMFBWaitTimer;


struct {
    uint8_t MMI_ACTION_status;// : 4;
    uint8_t MUSIC_CTRL_status;// : 4;
    uint8_t DISCOVERABLE_status;// : 4;
    uint8_t READ_LINK_MODE_status;// : 4;
    uint8_t READ_BD_ADDRESS_status;// : 4;
    uint8_t READ_PAIR_RECORD_status;// : 4;
    uint8_t LINK_BACK_status;// : 4;
    uint8_t DISCONNECT_PROFILE_status;// : 4;
    uint8_t SET_OVERALL_GAIN_status;// : 4;
    uint8_t SET_GPIO_CTRL_status;// : 4;
    uint8_t SPP_DATA_status;// : 4;
    uint8_t BTM_UTILITY_REQ_status;// : 4;
#if 1 //SH_ADD
	uint8_t CHANGE_DEVICE_NAME_status;// : 4;
	uint8_t READ_BTM_BATT_CHG_status;// : 4;
	uint8_t LE_ANCS_SERVICE_status;// : 4;
#endif //SH_ADD
    uint8_t LE_SIGNALING_status;// : 4;
    uint8_t SET_RX_BUFFER_SIZE_status;
    uint8_t PROFILE_LINK_BACK_status;
} CommandAckStatus; //Command ACK status

/*======================================*/
/*  function implemention  */
/*======================================*/
/*------------------------------------------------------------*/
static void StartToSendCommand( void )
{
    switch(BT_CMD_SendState)
    {
        case BT_CMD_SEND_MFB_HIGH_WAITING:
            //MFB waiting, do nothing
            break;

        case BT_CMD_SEND_DATA_SENDING:
            //data is going, do nothing
            break;

        case BT_CMD_SEND_DATA_SENT_WAITING:
            BT_CMD_SendState = BT_CMD_SEND_DATA_SENDING;
            UART_TransferFirstByte();
            break;

        case BT_CMD_SEND_STATE_IDLE:
            BT_CMD_SendState = BT_CMD_SEND_MFB_HIGH_WAITING;
			MFB_ENABLE;
            BT_CommandStartMFBWaitTimer = 1;      //wait 2 - 3ms
            break;
    }
}
/*------------------------------------------------------------*/
static bool copySendingCommandToBuffer(uint8_t* data, uint16_t size)
{
    bool buf_result = true;
    uint8_t ur_tx_buf_status_save = UR_TxBufStatus;
    uint16_t ur_tx_buf_head_save = UR_TxBufHead;

    if(UR_TxBufStatus !=  TXRX_BUF_FULL)
    {
        while(size--)
        {
            UR_TxBuf[UR_TxBufHead++] = *data++;

            if(UR_TxBufHead >= UR_TX_BUF_SIZE)
                UR_TxBufHead = 0;

            if(UR_TxBufHead ==  UR_TxBufTail)
            {
                if(size)
                {
                    buf_result = false;
                    UR_TxBufStatus = ur_tx_buf_status_save;		//restore in this case
                    UR_TxBufHead = ur_tx_buf_head_save;         //restore in this case
                }
                else
                {
                    UR_TxBufStatus =  TXRX_BUF_FULL;
                }
                break;
            }
            else
            {
                UR_TxBufStatus = TXRX_BUF_OK;
            }
        }

        if(buf_result)
        {
            StartToSendCommand();
        }
    }
    else
    {
        buf_result = false;
    }
    return buf_result;
}

/*------------------------------------------------------------*/
static uint8_t calculateChecksum(uint8_t* startByte, uint8_t* endByte)
{
    uint8_t checksum = 0;
    while(startByte <= endByte)
    {
        checksum += *startByte;
        startByte++;
    }
    checksum = ~checksum + 1;
    return checksum;
}

static uint8_t calculateChecksum2(uint8_t checksum, uint8_t* startByte, uint16_t length)
{
    while(length)
    {
        checksum += *startByte++;
        length--;
    }
    return checksum;
}

/*------------------------------------------------------------*/
uint8_t BT_IsAllowedToSendCommand( void )
{
    uint16_t i, size = sizeof(CommandAckStatus);
    uint8_t* p = &CommandAckStatus.MMI_ACTION_status;
    for(i=0; i<size; i++)
    {
        if(*p == COMMAND_IS_SENT)
            return false;
        p++;
    }
    return true;
}
/*------------------------------------------------------------*/
void BT_SendBytesAsCompleteCommand(uint8_t* command, uint8_t command_length)
{
    copySendingCommandToBuffer(command, command_length);
    BT_UpdateAckStatusWhenSent(command[3]);
}


#if 0

/*------------------------------------------------------------*/
void BT_MMI_ActionCommand(uint8_t MMI_ActionCode, uint8_t link_index)
{
    uint8_t command[8];
    command[0] = 0xAA;      //header byte 0
    command[1] = 0x00;      //header byte 1
    command[2] = 0x03;      //length
    command[3] = MMI_CMD;      //command ID
    command[4] = link_index;      //link_index, set to 0
    command[5] = MMI_ActionCode;
    command[6] = calculateChecksum(&command[2], &command[5]);
    copySendingCommandToBuffer(&command[0], 7);
    CommandAckStatus.MMI_ACTION_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_MusicControlCommand(uint8_t CtrlCode)
{
    uint8_t command[8];
    command[0] = 0xAA;      //header byte 0
    command[1] = 0x00;      //header byte 1
    command[2] = 0x03;      //length
    command[3] = MUSIC_CONTROL;      //command ID
    command[4] = 0x00;      //link_index, set to 0
    command[5] = CtrlCode;
    command[6] = calculateChecksum(&command[2], &command[5]);
    copySendingCommandToBuffer(&command[0], 7);
    CommandAckStatus.MUSIC_CTRL_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_SendAckToEvent(uint8_t ack_event)
{
    uint8_t command[6];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                      //length
    command[3] = MCU_SEND_EVENT_ACK;        //command ID
    command[4] = ack_event;                 //event to ack
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
}

/*------------------------------------------------------------*/
void BT_SendDiscoverableCommand(uint8_t discoverable)
{
    uint8_t command[6];
    command[0] = 0xAA;
    command[1] = 0x00;
    command[2] = 0x02;
    command[3] = DISCOVERABLE;
    command[4] = discoverable;      //0: disable, 1: enable
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.DISCOVERABLE_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_ReadBTMLinkModeCommand( void )
{
    uint8_t command[6];
    command[0] = 0xAA;
    command[1] = 0x00;
    command[2] = 0x02;
    command[3] = READ_LINK_MODE;
    command[4] = 0;         //dummy byte
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.READ_LINK_MODE_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_ReadDeviceAddressCommand(void)
{
    uint8_t command[6];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                      //length
    command[3] = READ_LOCAL_BD_ADDR;         //command ID
    command[4] = 0x00;                      //dummy byte
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.READ_BD_ADDRESS_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_GetPairRecordCommand(void)
{
    uint8_t command[6];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                      //length
    command[3] = READ_PAIRING_RECORD;         //command ID
    command[4] = 0x00;                      //dummy byte
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.READ_PAIR_RECORD_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_LinkBackToLastDevice(void)
{
    uint8_t command[6];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                      //length
    command[3] = PROFILE_LINK_BACK;         //command ID
    command[4] = 0x00;                      //0x00: last device, 0x01: last HFP device, 0x02: last A2DP device
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.LINK_BACK_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_LinkBackMultipoint(void)
{
    uint8_t command[6];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                      //length
    command[3] = PROFILE_LINK_BACK;         //command ID
    command[4] = 0x06;                      //multipoint devices
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.LINK_BACK_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_LinkBackToDeviceByBTAddress(uint8_t* address)
{
    uint8_t command[14];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 10;                      //length
    command[3] = PROFILE_LINK_BACK;         //command ID
    command[4] = 0x05;              //0x05: link back to device with specified address
    command[5] = 0x00;
    command[6] = 0x07;
    command[7] = *address++;        //byte 0
    command[8] = *address++;        //byte 1
    command[9] = *address++;        //byte 2
    command[10] = *address++;        //byte 3
    command[11] = *address++;        //byte 4
    command[12] = *address++;        //byte 5
    command[13] = calculateChecksum(&command[2], &command[12]);
    copySendingCommandToBuffer(&command[0], 14);
    CommandAckStatus.LINK_BACK_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_DisconnectAllProfile(void)
{
    uint8_t command[6];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                      //length
    command[3] = DISCONNECT;                //command ID
    command[4] = 0x0f;                      //event to ack
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.DISCONNECT_PROFILE_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_SetOverallGainCommand(uint8_t set_type, uint8_t gain1, uint8_t gain2, uint8_t gain3)
{
    uint8_t command[11];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[3] = SET_OVERALL_GAIN;                //command ID
    command[4] = 0x00;                      //link index
    command[5] = 0x00;                      //mask bits
    command[6] = set_type;                      //type
    if(set_type == 1 || set_type == 2)
    {
        command[2] = 0x04;                    //length
        command[7] = calculateChecksum(&command[2], &command[6]);
        copySendingCommandToBuffer(&command[0], 8);
    }
    else if(set_type == 3)
    {
        command[2] = 0x07;                    //length
        command[7] = gain1&0x0f;
        command[8] = gain2&0x0f;
        command[9] = gain3&0x0f;
        command[10] = calculateChecksum(&command[2], &command[9]);
        copySendingCommandToBuffer(&command[0], 11);
    }
    else
    {
        command[2] = 0x07;                    //lengthcommand[2] = 0x07;                    //length
        command[7] = gain1&0x7f;
        command[8] = gain2&0x7f;
        command[9] = gain3&0x7f;
        command[10] = calculateChecksum(&command[2], &command[9]);
        copySendingCommandToBuffer(&command[0], 11);
    }
    CommandAckStatus.SET_OVERALL_GAIN_status = COMMAND_IS_SENT;
}
/*------------------------------------------------------------*/
void BT_SetOverallGain(uint8_t gain1, uint8_t gain2, uint8_t gain3)
{
    uint8_t command[11];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x07; //lengthcommand[2] = 0x07;                    //length
    command[3] = SET_OVERALL_GAIN;                //command ID
    command[4] = 0x00;                      //link index
    command[5] = 0x00;                      //mask bits
    command[6] = 0x05;                      //type
    command[7] = gain1 & 0x7f;
    command[8] = gain2 & 0x7f;
    command[9] = gain3 & 0x7f;
    command[10] = calculateChecksum(&command[2], &command[9]);
    copySendingCommandToBuffer(&command[0], 11);
    CommandAckStatus.SET_OVERALL_GAIN_status = COMMAND_IS_SENT;
}
/*------------------------------------------------------------*/
void BT_updateA2DPGain(uint8_t gain)
{
    uint8_t command[11];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x07; //lengthcommand[2] = 0x07;                    //length
    command[3] = SET_OVERALL_GAIN;                //command ID
    command[4] = 0x00;                      //link index
    command[5] = 0x01;                      //mask bits
    command[6] = 0x04;                      //type
    command[7] = gain & 0x7f;
    command[8] = 0;
    command[9] = 0;
    command[10] = calculateChecksum(&command[2], &command[9]);
    copySendingCommandToBuffer(&command[0], 11);
    
    CommandAckStatus.SET_OVERALL_GAIN_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_updateHFPGain(uint8_t gain)
{
    uint8_t command[11];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x07;                    //length
    command[3] = SET_OVERALL_GAIN;                //command ID
    command[4] = 0x00;                      //link index
    command[5] = 0x02;                      //mask bits
    command[6] = 0x03;                      //type
    command[7] = 0;
    command[8] = gain & 0x0f;
    command[9] = 0;
    command[10] = calculateChecksum(&command[2], &command[9]);
    copySendingCommandToBuffer(&command[0], 11);
    
    CommandAckStatus.SET_OVERALL_GAIN_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_updateLineInGain(uint8_t gain)
{
    uint8_t command[11];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x07;                    //length
    command[3] = SET_OVERALL_GAIN;                //command ID
    command[4] = 0x00;                      //link index
    command[5] = 0x04;                      //mask bits
    command[6] = 0x03;                      //type
    command[7] = 0;
    command[8] = 0;
    command[9] = gain & 0x0f;
    command[10] = calculateChecksum(&command[2], &command[9]);
    copySendingCommandToBuffer(&command[0], 11);
    CommandAckStatus.SET_OVERALL_GAIN_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_SendSPPData(uint8_t* addr, uint16_t size, uint8_t link_index)
{
    uint8_t command[11];
    uint8_t checksum = 0;
    uint16_t cmd_length = size + 7;
    command[0] = 0xAA;                      //header byte 0
    command[1] = (uint8_t)(cmd_length>>8);                 //header byte 1
    command[2] = (uint8_t)(cmd_length&0xff);                      //length
    command[3] = SEND_SPP_DATA;             //command ID
    command[4] = link_index;                      //link_index, set to 0
    command[5] = 0x00;          //single packet format
    //total_length: 2byte
    command[6] = (uint8_t)(size>>8);
    command[7]= (uint8_t)(size&0xff);
    //payload_length: 2byte
    command[8] = (uint8_t)(size>>8);
    command[9] = (uint8_t)(size&0xff);
    copySendingCommandToBuffer(&command[0], 10);
    checksum = calculateChecksum2(checksum, &command[1], 9);
    copySendingCommandToBuffer(addr, size);
    checksum = calculateChecksum2(checksum, addr, size);
    checksum = ~checksum + 1;
    copySendingCommandToBuffer(&checksum, 1);
    CommandAckStatus.SPP_DATA_status = COMMAND_IS_SENT;
}

void BT_LoopBackSPPData(uint8_t* addr, uint16_t total_command_size)
{
    uint8_t command[11];
    uint8_t checksum = 0;
    uint16_t cmd_length = total_command_size+1;
    command[0] = 0xAA;                                  //header byte 0
    command[1] = (uint8_t)(cmd_length>>8);                 //header byte 1(length high byte)
    command[2] = (uint8_t)(cmd_length&0xff);                      //length(length low byte)
    command[3] = SEND_SPP_DATA;             //command ID
    copySendingCommandToBuffer(&command[0], 4);
    checksum = calculateChecksum2(checksum, &command[1], 3);
    copySendingCommandToBuffer(addr, total_command_size);
    checksum = calculateChecksum2(checksum, addr, total_command_size);
    checksum = ~checksum + 1;
    copySendingCommandToBuffer(&checksum, 1);
    CommandAckStatus.SPP_DATA_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_SetupBTMGPIO( void )
{
    uint8_t command[20];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 13;                        //length
    command[3] = GPIO_CTRL;                //command ID
    command[4] = 0xFF;                      //IO_Ctrl_Mask_P0,
    command[5] = 0xDF;                      //IO_Ctrl_Mask_P1,
    command[6] = 0xFF;                      //IO_Ctrl_Mask_P2,
    command[7] = 0xBF;                      //IO_Ctrl_Mask_P3,
    command[8] = 0x00;                      //IO_Setting_P0,
    command[9] = 0x00;                      //IO_Setting_P1,
    command[10] = 0x00;                     //IO_Setting_P2,
    command[11] = 0x00;                     //IO_Setting_P3,
    command[12] = 0x00;                     //Output_Value_P0,
    command[13] = 0x00;                     //Output_Value_P1,
    command[14] = 0x00;                     //Output_Value_P2,
    command[15] = 0x00;                     //Output_Value_P3,
    command[16] = calculateChecksum(&command[2], &command[15]);
    copySendingCommandToBuffer(&command[0], 17);
    CommandAckStatus.SET_GPIO_CTRL_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_EnterLineInMode(uint8_t disable0_enable1, uint8_t analog0_I2S1)
{
    uint8_t command[10];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 3;                        //length
    command[3] = BTM_UTILITY_FUNCTION;                //command ID
    command[4] = 1;                         //utility_function_type=0x01
    if(analog0_I2S1)
    {
        if(disable0_enable1)
            command[5] = 3;
        else
            command[5] = 2;
    }
    else
    {
        if(disable0_enable1)
            command[5] = 1;
        else
            command[5] = 0;
    }
    command[6] = calculateChecksum(&command[2], &command[5]);
    copySendingCommandToBuffer(&command[0], 7);
    CommandAckStatus.BTM_UTILITY_REQ_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_SetRXBufferSize( void )
{
    uint8_t command[10];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 3;                        //length
    command[3] = BT_MCU_UART_RX_BUFF_SIZE;                //command ID
    command[4] = 0;
    command[5] = 200;
    command[6] = calculateChecksum(&command[2], &command[5]);
    copySendingCommandToBuffer(&command[0], 7);
    CommandAckStatus.SET_RX_BUFFER_SIZE_status = COMMAND_IS_SENT;
}
void BT_ProfileLinkBack(uint8_t linked_profile, uint8_t link_index)
{
    uint8_t command[10];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 3;                        //length
    command[3] = ADDITIONAL_PROFILE_LINK_SETUP;                //command ID
    command[4] = link_index;
    command[5] = linked_profile;
    command[6] = calculateChecksum(&command[2], &command[5]);
    copySendingCommandToBuffer(&command[0], 7);
    CommandAckStatus.PROFILE_LINK_BACK_status = COMMAND_IS_SENT;
}
/*------------------------------------------------------------*/


#if 1 //SH_ADD

void LE_AdvData( void )
{
	uint8_t command[26];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x13;                      //length
    command[3] = LE_SIGNALING;				//command ID
	command[4] = 0x06;						//scan response data
	command[5] = 0x00;						//reserved
	command[6] = 0x02;						//name length + 1
	command[7] = 0x09;						//name type HCI_LE_ADVERTISING_REPORT_DATA_TYPE_LOCAL_NAME_COMPLETE
	sprintf((char *)&command[8],"LE_SGNL_BAND");
	command[20] = calculateChecksum(&command[2], &command[19]);
    copySendingCommandToBuffer(&command[0], 21);
    CommandAckStatus.LE_SIGNALING_status = COMMAND_IS_SENT;
}

void LE_EnableAdvertising( void )
{
    uint8_t command[10];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x03;                        //length
    command[3] = LE_SIGNALING;                //command ID
    command[4] = 0x01;
    command[5] = 0x01;
    command[6] = calculateChecksum(&command[2], &command[5]);
    copySendingCommandToBuffer(&command[0], 7);
    CommandAckStatus.LE_SIGNALING_status = COMMAND_IS_SENT;
}

void LE_SearchANCS( void )
{
	uint8_t command[10];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                        //length
    command[3] = LE_ANCS_SERVICE;                //command ID
    command[4] = 0x00;
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.LE_ANCS_SERVICE_status = COMMAND_IS_SENT;
	
}

void LE_SubscribeANCS( void )
{
	uint8_t command[10];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x03;                      //length
    command[3] = LE_ANCS_SERVICE;           //command ID
    command[4] = 0x01;
    command[5] = 0x01;
    command[6] = calculateChecksum(&command[2], &command[5]);
    copySendingCommandToBuffer(&command[0], 7);
    CommandAckStatus.LE_ANCS_SERVICE_status = COMMAND_IS_SENT;
}

void LE_GetNotifANCS(uint8_t uid)
{
	uint8_t command[26];

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1
        command[2] = 0x0B;

	command[3] = LE_ANCS_SERVICE;           //command ID
	command[4] = 0x02;
	command[5] = 0x00;                      //ancs CommandID
	command[6] = uid;                       //NotificationUID3
	command[7] = 0x00;                      //NotificationUID2
	command[8] = 0x00;                      //NotificationUID1
	command[9] = 0x00;                      //NotificationUID0
	command[10] = 0x00;                     //AttributeID0
	command[11] = 0x01;                     //AttributeID1
	command[12] = 0x40;                     //AttributeID1_1 length
	command[13] = 0x00;                     //AttributeID1_2 length 
        command[14] = calculateChecksum(&command[2], &command[13]);
        copySendingCommandToBuffer(&command[0], 15);
        CommandAckStatus.LE_ANCS_SERVICE_status = COMMAND_IS_SENT;
}

void BT_ReadLinkStatus( void )
{
	uint8_t command[10];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                      //length
    command[3] = READ_LINK_STATUS;           //command ID
    command[4] = 0x00;
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.LE_ANCS_SERVICE_status = COMMAND_IS_SENT;
}

void BT_ReadBTMBattStatus( void )
{
	uint8_t command[10];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                      //length
    command[3] = READ_BTM_BATT_CHG;			//command ID
    command[4] = 0x00;						//read batt status
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.READ_BTM_BATT_CHG_status = COMMAND_IS_SENT;
}

void BT_ReadBTMChargerStatus( void )
{
	uint8_t command[10];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x02;                      //length
    command[3] = READ_BTM_BATT_CHG;			//command ID
    command[4] = 0x01;						//read charger status
    command[5] = calculateChecksum(&command[2], &command[4]);
    copySendingCommandToBuffer(&command[0], 6);
    CommandAckStatus.READ_BTM_BATT_CHG_status = COMMAND_IS_SENT;
}

void BT_ChangeDeviceName( void )
{
	uint8_t command[26];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x0A;                      //length
    command[3] = CHANGE_DEVICE_NAME;           //command ID
	sprintf((char *)&command[4],"SGNL_BAND");
	command[13] = calculateChecksum(&command[2], &command[12]);
    copySendingCommandToBuffer(&command[0], 14);
    CommandAckStatus.CHANGE_DEVICE_NAME_status = COMMAND_IS_SENT;
}

#endif //SH_ADD


#else

void BT_MMI_ActionCommand(uint8_t MMI_ActionCode, uint8_t link_index)
{
	uint8_t command[8];
	uint8_t pos = 3;

	command[0] = 0xAA;      //header byte 0
	command[1] = 0x00;      //header byte 1

	command[pos++] = MMI_CMD;      //command ID
	command[pos++] = link_index;      //link_index, set to 0
	command[pos++] = MMI_ActionCode;

	BT_SendCommand(command, pos, &CommandAckStatus.MMI_ACTION_status);
}

/*------------------------------------------------------------*/
void BT_MusicControlCommand(uint8_t CtrlCode)
{
	uint8_t command[8];
	uint8_t pos = 3;

	command[0] = 0xAA;      //header byte 0
	command[1] = 0x00;      //header byte 1

	command[pos++] = MUSIC_CONTROL;      //command ID
	command[pos++] = 0x00;      //link_index, set to 0
	command[pos++] = CtrlCode;

	BT_SendCommand(command, pos, &CommandAckStatus.MUSIC_CTRL_status);
}

/*------------------------------------------------------------*/
void BT_SendAckToEvent(uint8_t ack_event)
{
	uint8_t command[6];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = MCU_SEND_EVENT_ACK;        //command ID
	command[pos++] = ack_event;                 //event to ack

	BT_SendCommand(command, pos, NULL);
}

/*------------------------------------------------------------*/
void BT_SendDiscoverableCommand(uint8_t discoverable)
{
	uint8_t command[6];
	uint8_t pos = 3;
	command[0] = 0xAA;
	command[1] = 0x00;

	command[pos++] = DISCOVERABLE;
	command[pos++] = discoverable;      //0: disable, 1: enable

	BT_SendCommand(command, pos, &CommandAckStatus.DISCOVERABLE_status);
}

/*------------------------------------------------------------*/
void BT_ReadBTMLinkModeCommand(void)
{
	uint8_t command[6];
	uint8_t pos = 3;

	command[0] = 0xAA;
	command[1] = 0x00;

	command[pos++] = READ_LINK_MODE;
	command[pos++] = 0;         //dummy byte

	BT_SendCommand(command, pos, &CommandAckStatus.READ_LINK_MODE_status);
}

/*------------------------------------------------------------*/
void BT_ReadDeviceAddressCommand(void)
{
	uint8_t command[6];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = READ_LOCAL_BD_ADDR;         //command ID
	command[pos++] = 0x00;                      //dummy byte

	BT_SendCommand(command, pos, &CommandAckStatus.READ_BD_ADDRESS_status);
}

/*------------------------------------------------------------*/
void BT_GetPairRecordCommand(void)
{
	uint8_t command[6];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = READ_PAIRING_RECORD;         //command ID
	command[pos++] = 0x00;                      //dummy byte

	BT_SendCommand(command, pos, &CommandAckStatus.READ_PAIR_RECORD_status);
}

/*------------------------------------------------------------*/
void BT_LinkBackToLastDevice(void)
{
	uint8_t command[6];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = PROFILE_LINK_BACK;         //command ID
	command[pos++] = 0x00;                      //0x00: last device, 0x01: last HFP device, 0x02: last A2DP device

	BT_SendCommand(command, pos, &CommandAckStatus.LINK_BACK_status);
}

/*------------------------------------------------------------*/
void BT_LinkBackMultipoint(void)
{
	uint8_t command[6];
	uint8_t pos = 3;
	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = PROFILE_LINK_BACK;         //command ID
	command[pos++] = 0x06;                      //multipoint devices

	BT_SendCommand(command, pos, &CommandAckStatus.LINK_BACK_status);
}

/*------------------------------------------------------------*/
void BT_LinkBackToDeviceByBTAddress(uint8_t* address)
{
	uint8_t command[14];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = PROFILE_LINK_BACK;         //command ID
	command[pos++] = 0x05;              //0x05: link back to device with specified address
	command[pos++] = 0x00;
	command[pos++] = 0x07;
	command[pos++] = *address++;        //byte 0
	command[pos++] = *address++;        //byte 1
	command[pos++] = *address++;        //byte 2
	command[pos++] = *address++;        //byte 3
	command[pos++] = *address++;        //byte 4
	command[pos++] = *address++;        //byte 5

	BT_SendCommand(command, pos, &CommandAckStatus.LINK_BACK_status);
}

/*------------------------------------------------------------*/
void BT_DisconnectAllProfile(void)
{
	uint8_t command[6];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = DISCONNECT;                //command ID
	command[pos++] = 0x0f;                      //event to ack

	BT_SendCommand(command, pos, &CommandAckStatus.DISCONNECT_PROFILE_status);
}

/*------------------------------------------------------------*/
void BT_SetOverallGainCommand(uint8_t set_type, uint8_t gain1, uint8_t gain2, uint8_t gain3)
{
	uint8_t command[11];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = SET_OVERALL_GAIN;                //command ID
	command[pos++] = 0x00;                      //link index
	command[pos++] = 0x00;                      //mask bits
	command[pos++] = set_type;                      //type
	if (set_type == 1 || set_type == 2)
	{
	}
	else if (set_type == 3)
	{
		command[pos++] = (gain1 & 0x0f);
		command[pos++] = (gain2 & 0x0f);
		command[pos++] = (gain3 & 0x0f);
	}
	else
	{
		command[pos++] = (gain1 & 0x7f);
		command[pos++] = (gain2 & 0x7f);
		command[pos++] = (gain3 & 0x7f);
	}

	BT_SendCommand(command, pos, &CommandAckStatus.SET_OVERALL_GAIN_status);
}
/*------------------------------------------------------------*/
void BT_SetOverallGain(uint8_t gain1, uint8_t gain2, uint8_t gain3)
{
	uint8_t command[11];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = SET_OVERALL_GAIN;                //command ID
	command[pos++] = 0x00;                      //link index
	command[pos++] = 0x00;                      //mask bits
	command[pos++] = 0x05;                      //type
	command[pos++] = (gain1 & 0x7f);
	command[pos++] = (gain2 & 0x7f);
	command[pos++] = (gain3 & 0x7f);

	BT_SendCommand(command, pos, &CommandAckStatus.SET_OVERALL_GAIN_status);
}
/*------------------------------------------------------------*/
void BT_updateA2DPGain(uint8_t gain)
{
    uint8_t command[11];
    command[0] = 0xAA;                      //header byte 0
    command[1] = 0x00;                      //header byte 1
    command[2] = 0x07; //lengthcommand[2] = 0x07;                    //length
    command[3] = SET_OVERALL_GAIN;                //command ID
    command[4] = 0x00;                      //link index
    command[5] = 0x01;                      //mask bits
    command[6] = 0x04;                      //type
    command[7] = gain & 0x7f;
    command[8] = 0;
    command[9] = 0;
    command[10] = calculateChecksum(&command[2], &command[9]);
    copySendingCommandToBuffer(&command[0], 11);
    
    CommandAckStatus.SET_OVERALL_GAIN_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_updateHFPGain(uint8_t gain)
{
	uint8_t command[11];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = SET_OVERALL_GAIN;                //command ID
	command[pos++] = 0x00;                      //link index
	command[pos++] = 0x02;                      //mask bits
	command[pos++] = 0x03;                      //type
	command[pos++] = 0;
	command[pos++] = gain & 0x0f;
	command[pos++] = 0;

	BT_SendCommand(command, pos, &CommandAckStatus.SET_OVERALL_GAIN_status);
}

/*------------------------------------------------------------*/
void BT_updateLineInGain(uint8_t gain)
{
	uint8_t command[11];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = SET_OVERALL_GAIN;                //command ID
	command[pos++] = 0x00;                      //link index
	command[pos++] = 0x04;                      //mask bits
	command[pos++] = 0x03;                      //type
	command[pos++] = 0;
	command[pos++] = 0;
	command[pos++] = (gain & 0x0f);

	BT_SendCommand(command, pos, &CommandAckStatus.SET_OVERALL_GAIN_status);
}

/*------------------------------------------------------------*/
void BT_SendSPPData(uint8_t* addr, uint16_t size, uint8_t link_index)
{
	uint8_t command[11];
	uint8_t checksum = 0;
	uint16_t cmd_length = size + 7;
	command[0] = 0xAA;                      //header byte 0
	command[1] = (uint8_t)(cmd_length >> 8);                 //header byte 1
	command[2] = (uint8_t)(cmd_length & 0xff);                      //length
	command[3] = SEND_SPP_DATA;             //command ID
	command[4] = link_index;                      //link_index, set to 0
	command[5] = 0x00;          //single packet format
				    //total_length: 2byte
	command[6] = (uint8_t)(size >> 8);
	command[7] = (uint8_t)(size & 0xff);
	//payload_length: 2byte
	command[8] = (uint8_t)(size >> 8);
	command[9] = (uint8_t)(size & 0xff);
	copySendingCommandToBuffer(&command[0], 10);
	checksum = calculateChecksum2(checksum, &command[1], 9);
	copySendingCommandToBuffer(addr, size);
	checksum = calculateChecksum2(checksum, addr, size);
	checksum = ~checksum + 1;
	copySendingCommandToBuffer(&checksum, 1);
	CommandAckStatus.SPP_DATA_status = COMMAND_IS_SENT;
}

void BT_LoopBackSPPData(uint8_t* addr, uint16_t total_command_size)
{
	uint8_t command[11];
	uint8_t checksum = 0;
	uint16_t cmd_length = total_command_size + 1;
	command[0] = 0xAA;                                  //header byte 0
	command[1] = (uint8_t)(cmd_length >> 8);                 //header byte 1(length high byte)
	command[2] = (uint8_t)(cmd_length & 0xff);                      //length(length low byte)
	command[3] = SEND_SPP_DATA;             //command ID
	copySendingCommandToBuffer(&command[0], 4);
	checksum = calculateChecksum2(checksum, &command[1], 3);
	copySendingCommandToBuffer(addr, total_command_size);
	checksum = calculateChecksum2(checksum, addr, total_command_size);
	checksum = ~checksum + 1;
	copySendingCommandToBuffer(&checksum, 1);
	CommandAckStatus.SPP_DATA_status = COMMAND_IS_SENT;
}

/*------------------------------------------------------------*/
void BT_SetupBTMGPIO(void)
{
	uint8_t command[20];
	uint8_t pos = 3;
	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = GPIO_CTRL;                //command ID
	command[pos++] = 0xFF;                      //IO_Ctrl_Mask_P0,
	command[pos++] = 0xDF;                      //IO_Ctrl_Mask_P1,
	command[pos++] = 0xFF;                      //IO_Ctrl_Mask_P2,
	command[pos++] = 0xBF;                      //IO_Ctrl_Mask_P3,
	command[pos++] = 0x00;                      //IO_Setting_P0,
	command[pos++] = 0x00;                      //IO_Setting_P1,
	command[pos++] = 0x00;                     //IO_Setting_P2,
	command[pos++] = 0x00;                     //IO_Setting_P3,
	command[pos++] = 0x00;                     //Output_Value_P0,
	command[pos++] = 0x00;                     //Output_Value_P1,
	command[pos++] = 0x00;                     //Output_Value_P2,
	command[pos++] = 0x00;                     //Output_Value_P3,

	BT_SendCommand(command, pos, &CommandAckStatus.SET_GPIO_CTRL_status);
}

/*------------------------------------------------------------*/
void BT_EnterLineInMode(uint8_t disable0_enable1, uint8_t analog0_I2S1)
{
	uint8_t command[10];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = BTM_UTILITY_FUNCTION;                //command ID
	command[pos++] = 1;                         //utility_function_type=0x01
	if (analog0_I2S1)
	{
		command[pos++] = (disable0_enable1) ? 3 : 2;
	}
	else
	{
		command[pos++] = (disable0_enable1) ? 1 : 0;
	}

	BT_SendCommand(command, pos, &CommandAckStatus.BTM_UTILITY_REQ_status);
}

/*------------------------------------------------------------*/
void BT_SetRXBufferSize(void)
{
	uint8_t command[10];
	uint8_t pos = 3;
	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = BT_MCU_UART_RX_BUFF_SIZE;                //command ID
	command[pos++] = 0;
	command[pos++] = 200;

	BT_SendCommand(command, pos, &CommandAckStatus.SET_RX_BUFFER_SIZE_status);
}

void BT_ProfileLinkBack(uint8_t linked_profile, uint8_t link_index)
{
	uint8_t command[10];
	uint8_t pos = 3;
	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = ADDITIONAL_PROFILE_LINK_SETUP;                //command ID
	command[pos++] = link_index;
	command[pos++] = linked_profile;

	BT_SendCommand(command, pos, &CommandAckStatus.PROFILE_LINK_BACK_status);
}
/*------------------------------------------------------------*/


#if 1 //SH_ADD

void LE_AdvData(void)
{
	uint8_t command[26];
	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1
	command[2] = 0x13;                      //length
	command[3] = LE_SIGNALING;				//command ID
	command[4] = 0x06;						//scan response data
	command[5] = 0x00;						//reserved
	command[6] = 0x02;						//name length + 1
	command[7] = 0x09;						//name type HCI_LE_ADVERTISING_REPORT_DATA_TYPE_LOCAL_NAME_COMPLETE
	sprintf((char *)&command[8], "LE_SGNL_BAND");
	command[20] = calculateChecksum(&command[2], &command[19]);
	copySendingCommandToBuffer(&command[0], 21);
	CommandAckStatus.LE_SIGNALING_status = COMMAND_IS_SENT;
}

void LE_EnableAdvertising(void)
{
	uint8_t command[10];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = LE_SIGNALING;                //command ID
	command[pos++] = 0x01;
	command[pos++] = 0x01;

	BT_SendCommand(command, pos, &CommandAckStatus.LE_SIGNALING_status);
}

void LE_SearchANCS(void)
{
	uint8_t command[10];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = LE_ANCS_SERVICE;                //command ID
	command[pos++] = 0x00;

	BT_SendCommand(command, pos, &CommandAckStatus.LE_ANCS_SERVICE_status);
}

void LE_SubscribeANCS(void)
{
	uint8_t command[10];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = LE_ANCS_SERVICE;           //command ID
	command[pos++] = 0x01;
	command[pos++] = 0x01;

	BT_SendCommand(command, pos, &CommandAckStatus.LE_ANCS_SERVICE_status);
}

void LE_GetNotifANCS(uint8_t uid)
{
	uint8_t command[26];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = LE_ANCS_SERVICE;           //command ID
	command[pos++] = 0x02;
	command[pos++] = 0x00;                      //ancs CommandID
	command[pos++] = uid;                       //NotificationUID3
	command[pos++] = 0x00;                      //NotificationUID2
	command[pos++] = 0x00;                      //NotificationUID1
	command[pos++] = 0x00;                      //NotificationUID0
	command[pos++] = 0x00;                     //AttributeID0
	command[pos++] = 0x01;                     //AttributeID1
	command[pos++] = 0x40;                     //AttributeID1_1 length
	command[pos++] = 0x00;                     //AttributeID1_2 length 

	BT_SendCommand(command, pos, &CommandAckStatus.LE_ANCS_SERVICE_status);
}

void BT_ReadLinkStatus(void)
{
	uint8_t command[10];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = READ_LINK_STATUS;           //command ID
	command[pos++] = 0x00;

	BT_SendCommand(command, pos, &CommandAckStatus.LE_ANCS_SERVICE_status);
}

void BT_ReadBTMBattStatus(void)
{
	uint8_t command[10];
	uint8_t pos = 3;
	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = READ_BTM_BATT_CHG;			//command ID
	command[pos++] = 0x00;						//read batt status

	BT_SendCommand(command, pos, &CommandAckStatus.READ_BTM_BATT_CHG_status);
}

void BT_ReadBTMChargerStatus(void)
{
	uint8_t command[10];
	uint8_t pos = 3;

	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1

	command[pos++] = READ_BTM_BATT_CHG;			//command ID
	command[pos++] = 0x01;						//read charger status

	BT_SendCommand(command, pos, &CommandAckStatus.READ_BTM_BATT_CHG_status);
}

void BT_ChangeDeviceName(void)
{
	uint8_t command[26];
	command[0] = 0xAA;                      //header byte 0
	command[1] = 0x00;                      //header byte 1
	command[2] = 0x0A;                      //length
	command[3] = CHANGE_DEVICE_NAME;           //command ID
	sprintf((char *)&command[4], "SGNL_BAND");
	command[13] = calculateChecksum(&command[2], &command[12]);
    copySendingCommandToBuffer(&command[0], 14);
    CommandAckStatus.CHANGE_DEVICE_NAME_status = COMMAND_IS_SENT;
}

void BT_SendCommand(uint8_t *command, uint8_t pos, uint8_t *status)
{
	command[2] = pos - 3;//length
	command[pos] = calculateChecksum(&command[2], &command[pos - 1]);
	copySendingCommandToBuffer(&command[0], pos + 1);
	*status = COMMAND_IS_SENT;
}


#endif //SH_ADD

#endif

/*------------------------------------------------------------*/
void BT_CommandSendInit(void)
{
	UR_TxBufHead = 0;
	UR_TxBufTail = 0;
	UR_TxBufStatus = TXRX_BUF_EMPTY;
	BT_InitAckStatus();
}

/*------------------------------------------------------------*/
void BT_CommandSendTask(void)
{
	switch (BT_CMD_SendState)
	{
	case BT_CMD_SEND_MFB_HIGH_WAITING:
		//do nothing, timer interrupt will handle
		break;

	case BT_CMD_SEND_DATA_SENDING:
		//do nothing, UART interrupt will handle
		break;

	case BT_CMD_SEND_DATA_SENT_WAITING:
		if (!BT_CommandSentMFBWaitTimer)
		{
			MFB_DISABLE;
			BT_CMD_SendState = BT_CMD_SEND_STATE_IDLE;
		}
		break;

	case BT_CMD_SEND_STATE_IDLE:
		break;

	default:
		break;
	}
}

/*------------------------------------------------------------*/
void BT_CommandSend1MS_event(void)
{
	if (BT_CommandStartMFBWaitTimer)
	{
		--BT_CommandStartMFBWaitTimer;
		if (!BT_CommandStartMFBWaitTimer)
		{
			if (BT_CMD_SendState == BT_CMD_SEND_MFB_HIGH_WAITING)
			{
				BT_CMD_SendState = BT_CMD_SEND_DATA_SENDING;
				UART_TransferFirstByte();
			}
		}
	}
}

/*------------------------------------------------------------*/
void UART_TransferFirstByte(void)
{
	uint8_t data;
	if (UR_TxBufStatus != TXRX_BUF_EMPTY)
	{
		data = UR_TxBuf[UR_TxBufTail++];
		if (UR_TxBufTail >= UR_TX_BUF_SIZE)
			UR_TxBufTail = 0;
		if (UR_TxBufHead == UR_TxBufTail)
			UR_TxBufStatus = TXRX_BUF_EMPTY;
		else
			UR_TxBufStatus = TXRX_BUF_OK;
		uart_send_byte(data);
	}
}

/*------------------------------------------------------------*/

void UART_TransferNextByte(void)
{
	uint8_t data;

	if (UR_TxBufStatus != TXRX_BUF_EMPTY)
	{
		data = UR_TxBuf[UR_TxBufTail++];
		if (UR_TxBufTail >= UR_TX_BUF_SIZE)
			UR_TxBufTail = 0;
		if (UR_TxBufHead == UR_TxBufTail)
			UR_TxBufStatus = TXRX_BUF_EMPTY;
		else
			UR_TxBufStatus = TXRX_BUF_OK;
		uart_send_byte(data);
	}
	else
	{
		if (BT_CMD_SendState == BT_CMD_SEND_DATA_SENDING)
		{
			BT_CMD_SendState = BT_CMD_SEND_DATA_SENT_WAITING;
			BT_CommandSentMFBWaitTimer = 1;
		}
	}
}

/*------------------------------------------------------------*/
void BT_InitAckStatus(void)
{
	uint16_t i, size = sizeof(CommandAckStatus);
	uint8_t* p = &CommandAckStatus.MMI_ACTION_status;
	for (i = 0; i<size; i++)
	{
		*p++ = COMMAND_STS_INIT;
	}
}
/*------------------------------------------------------------*/
void BT_UpdateAckStatusWhenReceived(uint8_t command_id, uint8_t ack_status)
{
	ack_status &= 0x0f;
	switch (command_id) {
	case MMI_CMD:
		CommandAckStatus.MMI_ACTION_status = ack_status;
		break;
	case MUSIC_CONTROL:
		CommandAckStatus.MUSIC_CTRL_status = ack_status;
		break;
	case DISCOVERABLE:
		CommandAckStatus.DISCOVERABLE_status = ack_status;
		break;
	case READ_LINK_MODE:
		CommandAckStatus.READ_LINK_MODE_status = ack_status;
		break;
	case READ_LOCAL_BD_ADDR:
		CommandAckStatus.READ_BD_ADDRESS_status = ack_status;
		break;
	case READ_PAIRING_RECORD:
		CommandAckStatus.READ_PAIR_RECORD_status = ack_status;
		break;
	case PROFILE_LINK_BACK:
		CommandAckStatus.LINK_BACK_status = ack_status;
		break;
	case DISCONNECT:
		CommandAckStatus.DISCONNECT_PROFILE_status = ack_status;
		break;
	case SET_OVERALL_GAIN:
		CommandAckStatus.SET_OVERALL_GAIN_status = ack_status;
		break;
	case SEND_SPP_DATA:
		CommandAckStatus.SPP_DATA_status = ack_status;
		break;
	case GPIO_CTRL:
		CommandAckStatus.SET_GPIO_CTRL_status = ack_status;
		break;
	case BTM_UTILITY_FUNCTION:
		CommandAckStatus.BTM_UTILITY_REQ_status = ack_status;
		break;
	case LE_SIGNALING:
		CommandAckStatus.LE_SIGNALING_status = ack_status;
		break;
#if 1 //SH_ADD
	case CHANGE_DEVICE_NAME:
		CommandAckStatus.CHANGE_DEVICE_NAME_status = ack_status;
		break;
	case READ_BTM_BATT_CHG:
		CommandAckStatus.READ_BTM_BATT_CHG_status = ack_status;
		break;
	case LE_ANCS_SERVICE:
		CommandAckStatus.LE_ANCS_SERVICE_status = ack_status;
		break;
#endif
	case BT_MCU_UART_RX_BUFF_SIZE:
		CommandAckStatus.SET_RX_BUFFER_SIZE_status = ack_status;
		break;
	case ADDITIONAL_PROFILE_LINK_SETUP:
		CommandAckStatus.PROFILE_LINK_BACK_status = ack_status;
		break;
	default:
		break;
	}
}

/*------------------------------------------------------------*/
void BT_UpdateAckStatusWhenSent(uint8_t command_id)
{
	switch (command_id) {
	case MMI_CMD:
		CommandAckStatus.MMI_ACTION_status = COMMAND_IS_SENT;
		break;
	case MUSIC_CONTROL:
		CommandAckStatus.MUSIC_CTRL_status = COMMAND_IS_SENT;
		break;
	case DISCOVERABLE:
		CommandAckStatus.DISCOVERABLE_status = COMMAND_IS_SENT;
		break;
	case READ_LINK_MODE:
		CommandAckStatus.READ_LINK_MODE_status = COMMAND_IS_SENT;
		break;
	case READ_LOCAL_BD_ADDR:
		CommandAckStatus.READ_BD_ADDRESS_status = COMMAND_IS_SENT;
		break;
	case READ_PAIRING_RECORD:
		CommandAckStatus.READ_PAIR_RECORD_status = COMMAND_IS_SENT;
		break;
	case PROFILE_LINK_BACK:
		CommandAckStatus.LINK_BACK_status = COMMAND_IS_SENT;
		break;
	case DISCONNECT:
		CommandAckStatus.DISCONNECT_PROFILE_status = COMMAND_IS_SENT;
		break;
	case SET_OVERALL_GAIN:
		CommandAckStatus.SET_OVERALL_GAIN_status = COMMAND_IS_SENT;
		break;
	case SEND_SPP_DATA:
		CommandAckStatus.SPP_DATA_status = COMMAND_IS_SENT;
		break;
	case GPIO_CTRL:
		CommandAckStatus.SET_GPIO_CTRL_status = COMMAND_IS_SENT;
		break;
	case BTM_UTILITY_FUNCTION:
		CommandAckStatus.BTM_UTILITY_REQ_status = COMMAND_IS_SENT;
		break;
#if 1 //SH_ADD
	case CHANGE_DEVICE_NAME:
		CommandAckStatus.CHANGE_DEVICE_NAME_status = COMMAND_IS_SENT;
		break;
	case READ_BTM_BATT_CHG:
		CommandAckStatus.READ_BTM_BATT_CHG_status = COMMAND_IS_SENT;
		break;
	case LE_ANCS_SERVICE:
		CommandAckStatus.LE_ANCS_SERVICE_status = COMMAND_IS_SENT;
		break;
#endif
	case LE_SIGNALING:
		CommandAckStatus.LE_SIGNALING_status = COMMAND_IS_SENT;
		break;
	case BT_MCU_UART_RX_BUFF_SIZE:
		CommandAckStatus.SET_RX_BUFFER_SIZE_status = COMMAND_IS_SENT;
		break;
	case ADDITIONAL_PROFILE_LINK_SETUP:
		CommandAckStatus.PROFILE_LINK_BACK_status = COMMAND_IS_SENT;
	default:
		break;
	}
}

/*------------------------------------------------------------*/
uint8_t BT_GetAckStatusByCommandID(uint8_t command_id)
{
	uint8_t ack_status;
	switch (command_id) {
	case MMI_CMD:
		ack_status = CommandAckStatus.MMI_ACTION_status;
		break;
	case MUSIC_CONTROL:
		ack_status = CommandAckStatus.MUSIC_CTRL_status;
		break;
	case DISCOVERABLE:
		ack_status = CommandAckStatus.DISCOVERABLE_status;
		break;
	case READ_LINK_MODE:
		ack_status = CommandAckStatus.READ_LINK_MODE_status;
		break;
	case READ_LOCAL_BD_ADDR:
		ack_status = CommandAckStatus.READ_BD_ADDRESS_status;
		break;
	case READ_PAIRING_RECORD:
		ack_status = CommandAckStatus.READ_PAIR_RECORD_status;
		break;
	case PROFILE_LINK_BACK:
		ack_status = CommandAckStatus.LINK_BACK_status;
		break;
	case DISCONNECT:
		ack_status = CommandAckStatus.DISCONNECT_PROFILE_status;
		break;
	case SET_OVERALL_GAIN:
		ack_status = CommandAckStatus.SET_OVERALL_GAIN_status;
		break;
	case SEND_SPP_DATA:
		ack_status = CommandAckStatus.SPP_DATA_status;
		break;
	case GPIO_CTRL:
		ack_status = CommandAckStatus.SET_GPIO_CTRL_status;
		break;
	case BTM_UTILITY_FUNCTION:
		ack_status = CommandAckStatus.BTM_UTILITY_REQ_status;
		break;
	case LE_ANCS_SERVICE:
		ack_status = CommandAckStatus.LE_ANCS_SERVICE_status;
		break;
	case LE_SIGNALING:
		ack_status = CommandAckStatus.LE_SIGNALING_status;
		break;
	case BT_MCU_UART_RX_BUFF_SIZE:
		ack_status = CommandAckStatus.SET_RX_BUFFER_SIZE_status;
		break;
	case ADDITIONAL_PROFILE_LINK_SETUP:
		ack_status = CommandAckStatus.PROFILE_LINK_BACK_status;
		break;
	default:
		ack_status = ACK_STS_OK;            //for unprocessed command returns ACK_OK
		break;
	}
	return ack_status;
}
uint8_t BT_GetAckStatusSendSPPData(void)
{
	return CommandAckStatus.SPP_DATA_status;
}
#endif //ENABLE_IS2063
