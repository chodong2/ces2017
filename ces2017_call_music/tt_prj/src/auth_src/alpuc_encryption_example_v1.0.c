/***********************************************************************************
 * Project : Alpu-a Encryption Example
 * Version : v1.0
 * Date    : 2013-05-10
 * Author  : 
 * Reviser : Kim Jinyoung
 * Company : Neowine Co., Ltd.
 * Comments:
***********************************************************************************/
#include "global_define.h"

#include <stdlib.h>
#include <time.h>


#ifdef ENABLE_AUTH
unsigned char _i2c_write(unsigned char dev_addr, unsigned char address, unsigned char *data, int ByteNo)
{
#if 0
        uint8_t ret = 0;
        ret = Sensors_I2C_WriteRegister(dev_addr, address, ByteNo, data);
        return ret;
#else
        TM_I2C_WriteMulti(SENSORS_I2C, dev_addr, address, data, ByteNo);//?????
        //delay_10ms(1);
        return 0;
#endif
}

unsigned char _i2c_read(unsigned char dev_addr, unsigned char address, unsigned char *data, int ByteNo)
{
#if 01
        uint8_t ret = 0;
        ret = Sensors_I2C_ReadRegister(dev_addr, address, ByteNo, data);//????????
        return ret;
#else
        TM_I2C_ReadMulti(SENSORS_I2C, dev_addr, address, data, ByteNo);
        return 0;
#endif
}


void _alpu_delay_ms(unsigned int i)
{
	delay_10ms(i);
}


unsigned char _alpu_rand(void)
{

  static unsigned long seed; // 2byte, must be a static variable

  seed = seed + rand(); // rand(); <------------------ add time value
  seed =  seed * 1103515245 + 12345;

  return (seed/65536) % 32768;

}

//귀사의 alpu_main에서 하기와 같이 _alpuc_process()함수만을 호출하여 사용하시기 바랍니다.
//tx_data 와 dx_data 값이 같으면 success 입니다.
//success일경우 return값은 '0' 입니다.

int authenticate(void)
{
	unsigned char tx_data[8];
	unsigned char dx_data[8];
	unsigned char error_code;
        int i = 0;
#if 0
	int j;
        unsigned char ctrl_data[8];
        unsigned char system_use_data[8];
	
	srand(time(NULL));
        memset(alpuc_tx_data, 0x00, sizeof(alpuc_tx_data));
        memset(alpuc_rx_data, 0x00, sizeof(alpuc_rx_data));

	for(j=0; j<100; j++)
	{ 		

//CASE 1 선택시 랜덤값을 입력값으로 설정, CASE 2 선택시 업체측에서 사용되는 data를 입력값으로 설정.
#if 01
	 for(i=0; i<8; i++) tx_data[i] = _alpu_rand();	// Case 1  랜덤 값 입력
	 
	 error_code = alpuc_process(tx_data, dx_data);
	 
	 if ( error_code )      exit(1);

#else
	 for(i=0; i<8; i++) tx_data[i] = system_use_data[i];	// Case 2	사용자측에서 사용되는 data 입력 ex)리모컨
	
	 error_code = alpuc_process(tx_data, dx_data);
	 
	 for(i=1; i<7; i++) ctrl_data[i] = dx_data[i] ;  // ctrl_data[] 귀사에서  system적으로 dx_data[]를 저장할 필요가 있을때 별도 선언 후 사용..    
	 
	 if ( error_code )      exit(1);

#endif
	}
#else
        //if(g_ui32_10ms_count % 6000 == 0)
        {
                //srand(time(NULL));
                for(i=0; i<8; i++)
                        tx_data[i] = _alpu_rand();
                        //tx_data[i] = 0x12;

                error_code = alpuc_process(tx_data, dx_data);
                if(error_code)
                        exit(1);
        }
#endif
        
	return 0;
}

/* EOF */
#endif