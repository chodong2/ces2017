/*
* ________________________________________________________________________________________________________
* Copyright ?2014-2015 InvenSense Inc. Portions Copyright ?2014-2015 Movea. All rights reserved.
* This software, related documentation and any modifications thereto (collectively �Software? is subject
* to InvenSense and its licensors' intellectual property rights under U.S. and international copyright and
* other intellectual property rights laws.
* InvenSense and its licensors retain all intellectual property and proprietary rights in and to the Software
* and any use, reproduction, disclosure or distribution of the Software without an express license
* agreement from InvenSense is strictly prohibited.
* ________________________________________________________________________________________________________
*/

#include "inv_defines.h"

struct base_driver_t base_state;
static uint8_t sAllowLpEn = 1;

void inv_mems_prevent_lpen_control(void)
{
	sAllowLpEn = 0;
}
void inv_mems_allow_lpen_control(void)
{
	sAllowLpEn = 1;
	inv_set_chip_power_state(CHIP_LP_ENABLE, 1);
}
uint8_t inv_mems_get_lpen_control(void)
{
	return sAllowLpEn;
}

/*!
******************************************************************************
*   @brief     This function sets the power state of the Ivory chip
*				loop
*   @param[in] Function - CHIP_AWAKE, CHIP_LP_ENABLE
*   @param[in] On/Off - The functions are enabled if previously disabled and
disabled if previously enabled based on the value of On/Off.
******************************************************************************
*/
int inv_set_chip_power_state(unsigned char func, unsigned char on_off)
{
	unsigned char d = 0;
	int result = 0;

	d = base_state.pwr_mgmt_1;

	switch (func) {

	case CHIP_AWAKE:
	{
		if (on_off) {
			if ((base_state.wake_state & CHIP_AWAKE) == 0) {// undo sleep_en
				d = BIT_CLK_PLL;
                                result = Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_PWR_MGMT_1, 1, &d);
                                //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_PWR_MGMT_1, &d, 1);
				base_state.wake_state |= CHIP_AWAKE;
				base_state.pwr_mgmt_1 = d;
				delay_10ms(1); // after writting the bit wait 100 Micro Seconds
			}
		}
		else {
			if (base_state.wake_state & CHIP_AWAKE) {// set sleep_en
				d = BIT_SLEEP;
                                result = Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_PWR_MGMT_1, 1, &d);
                                //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_PWR_MGMT_1, &d, 1);
				base_state.wake_state &= ~CHIP_AWAKE;
				base_state.pwr_mgmt_1 = d;
				delay_10ms(1); // after writting the bit wait 100 Micro Seconds
			}
		}
	}
	break;

	case CHIP_LP_ENABLE:
	{
		if (base_state.lp_en_support == 1) {
			if (on_off) {
				if ((inv_mems_get_lpen_control()) && ((base_state.wake_state & CHIP_LP_ENABLE) == 0)) {
					d |= BIT_LP_EN; // lp_en ON
                                        result = Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_PWR_MGMT_1, 1, &d);
                                        //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_PWR_MGMT_1, &d, 1);
					base_state.wake_state |= CHIP_LP_ENABLE;
					base_state.pwr_mgmt_1 = d;
					delay_10ms(1); // after writting the bit wait 100 Micro Seconds
				}
			}
			else {
				if (base_state.wake_state & CHIP_LP_ENABLE) {
					d &= ~BIT_LP_EN; // lp_en off
                                        result = Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_PWR_MGMT_1, 1, &d);
                                        //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_PWR_MGMT_1, &d, 1);
					base_state.wake_state &= ~CHIP_LP_ENABLE;
					base_state.pwr_mgmt_1 = d;
					delay_10ms(1); // after writting the bit wait 100 Micro Seconds
				}
			}
		}
	}
	break;

	default:
		break;

	}// end switch

	return result;
}

/*!
******************************************************************************
*   @return    Current wake status of the Ivory chip.
******************************************************************************
*/
uint8_t inv_get_chip_power_state()
{
	return base_state.wake_state;
}

/**
* @brief soft reset
* @details mems reset
* @return 0 - succesful
*/
int inv_reset_mems()
{
	int result = INV_SUCCESS;
	unsigned char data;

	data = BIT_H_RESET;
	result |= inv_write_mems_reg(REG_PWR_MGMT_1, 1, &data);
	delay_10ms(1);
	return result;
}

/** Wakes up DMP3 (MEMS).
*/
int inv_wakeup_mems()
{
	unsigned char data;
	int result = INV_SUCCESS;
	static unsigned char user_ctrl_reg;

	result = inv_set_chip_power_state(CHIP_AWAKE, 1);

	//data = 0x47;	// FIXME, should set up according to sensor/engines enabled.
	data = 0x07;	// FIXME, should set up according to sensor/engines enabled.
	result |= inv_write_mems_reg(REG_PWR_MGMT_2, 1, &data);

	if (base_state.firmware_loaded == 1)
	{
		user_ctrl_reg |= BIT_DMP_EN | BIT_FIFO_EN;
		result |= inv_write_single_mems_reg(REG_USER_CTRL, user_ctrl_reg);
	}

	result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 1);
	return result;
}

/** Puts DMP3 (MEMS) into the lowest power state. Assumes sensors are all off.
*/
int inv_sleep_mems()
{
	int result;
	unsigned char data;

	data = 0x7F;
	result = inv_write_mems_reg(REG_PWR_MGMT_2, 1, &data);

	result |= inv_set_chip_power_state(CHIP_AWAKE, 0);

	return result;
}

int inv_set_dmp_address()
{
	int result;
	unsigned char dmp_cfg[2] = { 0 };
	unsigned short config;

	// Write DMP Start address
	inv_get_dmp_start_address_20648(&config);
	/* setup DMP start address and firmware */
	dmp_cfg[0] = (unsigned char)((config >> 8) & 0xff);
	dmp_cfg[1] = (unsigned char)(config & 0xff);

	result = inv_write_mems_reg(REG_PRGM_START_ADDRH, 2, dmp_cfg);
	return result;
}

/** Should be called once on power up. Loads DMP3, initializes internal variables needed
*   for other lower driver functions.
*/
 // Init MEMS
 /* Initialize MEMS chip for I2C channel communication :
 - Load DMP image
 - Configure DMP FIFO watermark
 - Enable DMP interrupts at DMP level
 - Configure gyro fullscale to 2000dps and accel fullscale to 2g by default
 - Put MEMS in low power */
int init_motion_sensor_driver(void)
{
	int result = 0;
	unsigned char data;

	uint8_t lMemsWhoAmIRead = 0x00;

	// Read WhoAmI from hardware register and check it against expected value for current MEMS
        result = Sensors_I2C_ReadRegister(ICM20648_I2C_ADDR, REG_WHO_AM_I, 1, &lMemsWhoAmIRead);
        if(result)
              return result;
        //TM_I2C_ReadMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_WHO_AM_I, &lMemsWhoAmIRead, 1);

	if (lMemsWhoAmIRead != 0xE0)
        {
                SCB->AIRCR = (0x05FA0000) | 0x04;
		//printf("** Error MEMS whoami read %d not in line with the one expected %d\n", lMemsWhoAmIRead, lMemsWhoAmIExpected);
        }

	// Set varialbes to default values
	memset(&base_state, 0, sizeof(base_state));
	base_state.pwr_mgmt_2 = BIT_PWR_ACCEL_STBY | BIT_PWR_GYRO_STBY | BIT_PWR_PRESSURE_STBY;

        // and Wakeup !
	result |= inv_wakeup_mems();

	result |= inv_read_mems_reg(REG_WHO_AM_I, 1, &data);

	data = BIT_ACCEL_CYCLE;
	result |= inv_write_mems_reg(REG_LP_CONFIG, 1, &data);

	// Disable Ivory DMP.
	result |= inv_write_single_mems_reg(REG_USER_CTRL, 0);

	//Setup Ivory DMP.
	result |= inv_load_firmware_20648();
	if (result)
                SCB->AIRCR = (0x05FA0000) | 0x04; //hard fault software reset
	else
		base_state.firmware_loaded = 1;

	result |= inv_set_dmp_address();

	// Turn off all sensors on DMP by default.
	result |= dmp_reset_control_registers_20648();

	// set FIFO watermark to 80% of actual FIFO size
	result |= dmp_set_FIFO_watermark_20648(800);

	// Enable Interrupts.
	data = 0x02;
	result |= inv_write_mems_reg(REG_INT_ENABLE, 1, &data); // Enable DMP Interrupt
	data = 0x01;
	result |= inv_write_mems_reg(REG_INT_ENABLE_2, 1, &data); // Enable FIFO Overflow Interrupt

        result |= inv_set_int1_assertion(1);
        
        // TRACKING : To have accelerometers datas and the interrupt without gyro enables.
	data = 0xE4;
	result |= inv_write_mems_reg(REG_SINGLE_FIFO_PRIORITY_SEL, 1, &data);

	// Disable HW temp fix
	inv_read_mems_reg(REG_HW_FIX_DISABLE, 1, &data);
	data |= 0x08;
	inv_write_mems_reg(REG_HW_FIX_DISABLE, 1, &data);

	// Setup MEMs properties.
	base_state.accel_averaging = 1; //Change this value if higher sensor sample avergaing is required.
	base_state.gyro_averaging = 1;  //Change this value if higher sensor sample avergaing is required.
	inv_set_accel_divider(FIFO_DIVIDER);      //Initial sampling rate 1125Hz/10+1 = 102Hz.
	result |= inv_set_accel_fullscale(MPU_FS_2G);

	// FIFO Setup.
	result |= inv_write_single_mems_reg(REG_FIFO_CFG, BIT_SINGLE_FIFO_CFG); // FIFO Config. fixme do once? burst write?
	result |= inv_write_single_mems_reg(REG_FIFO_RST, 0x1f); // Reset all FIFOs.
	result |= inv_write_single_mems_reg(REG_FIFO_RST, 0x1e); // Keep all but Gyro FIFO in reset.
	result |= inv_write_single_mems_reg(REG_FIFO_EN, 0x0); // Slave FIFO turned off.
	result |= inv_write_single_mems_reg(REG_FIFO_EN_2, 0x0); // Hardware FIFO turned off.

	//result |= inv_read_mems(MPU_SOFT_UPDT_ADDR, 1, &data);  // Check LP_EN support.
        //data &= MPU_SOFT_UPTD_MASK;

        inv_mems_allow_lpen_control();
        base_state.lp_en_support = 1;

	// for use of user ... result |= inv_sleep_mems();

        result |= dmp_set_data_output_control1_20648( PED_STEPDET_SET | HEADER2_SET | PED_STEPIND_SET );
        result |= dmp_set_data_interrupt_control_20648( PED_STEPDET_SET | HEADER2_SET | PED_STEPIND_SET );
        result |= dmp_set_data_output_control2_20648( ACT_RECOG_SET );
#if 0
        result |= dmp_set_motion_event_control_20648( INV_PEDOMETER_EN | INV_PEDOMETER_INT_EN );
#else
        result |= dmp_set_motion_event_control_20648( INV_PEDOMETER_EN | INV_PEDOMETER_INT_EN | INV_SMD_EN );
#endif
        result |= dmp_set_data_rdy_status_20648( ACCEL_AVAILABLE );
        if(result)
                return result;
        
        result |= inv_wakeup_mems();

	return result;
}

int inv_set_accel_divider(short div)
{
	unsigned char data[2] = { 0 };

	base_state.accel_div = div;
	data[0] = (unsigned char)(div >> 8);
	data[1] = (unsigned char)(div & 0xff);

	return inv_write_mems_reg(REG_ACCEL_SMPLRT_DIV_1, 2, data);
}

short inv_get_accel_divider()
{
	return base_state.accel_div;
}

// not exported in Headers
extern unsigned long inv_androidSensor_enabled(unsigned char androidSensor);

int inv_set_accel_fullscale(int level)
{
	int result;
	base_state.accel_fullscale = level;
	result = inv_set_mems_accel_fullscale(level);
	result |= dmp_set_accel_fsr_20648(2 << level);
	result |= dmp_set_accel_scale2_20648(2 << level);

	return result;
}

uint8_t inv_get_accel_fullscale()
{
	return base_state.accel_fullscale;
}


int inv_set_mems_accel_fullscale(int level)
{
	int result = 0;
	unsigned char accel_config_1_reg;
	unsigned char accel_config_2_reg;
	unsigned char dec3_cfg;

	if (level >= NUM_MPU_AFS)
		return INV_ERROR_INVALID_PARAMETER;

	result |= inv_read_mems_reg(REG_ACCEL_CONFIG, 1, &accel_config_1_reg);
	accel_config_1_reg &= 0xC0;

	if (base_state.accel_averaging > 1)
		accel_config_1_reg |= (7 << 3) | (level << 1) | 1;   //fchoice = 1, filter = 7.
	else
		accel_config_1_reg |= (level << 1);  //fchoice = 0, filter = 0.

	result |= inv_write_single_mems_reg(REG_ACCEL_CONFIG, accel_config_1_reg);

	switch (base_state.accel_averaging) {
	case 1:
		dec3_cfg = 0;
		break;
	case 4:
		dec3_cfg = 0;
		break;
	case 8:
		dec3_cfg = 1;
		break;
	case 16:
		dec3_cfg = 2;
		break;
	case 32:
		dec3_cfg = 3;
		break;
	default:
		dec3_cfg = 0;
		break;
	}

	result |= inv_read_mems_reg(REG_ACCEL_CONFIG_2, 1, &accel_config_2_reg);
	accel_config_2_reg &= 0xFC;

	accel_config_2_reg |= dec3_cfg;
	result |= inv_write_single_mems_reg(REG_ACCEL_CONFIG_2, accel_config_2_reg);

	return result;
}


int inv_enable_mems_hw_sensors(int bit_mask)
{
	int rc = INV_SUCCESS;

	if ((base_state.pwr_mgmt_2 == (BIT_PWR_ACCEL_STBY | BIT_PWR_GYRO_STBY | BIT_PWR_PRESSURE_STBY)) | (bit_mask & 0x80))
        {
		// All sensors off, or override is on
		base_state.pwr_mgmt_2 = 0; // Zero means all sensors are on Gyro and Accel were off
		
                if ((bit_mask & 2) == 0)        base_state.pwr_mgmt_2 = BIT_PWR_ACCEL_STBY; // Turn off accel
		if ((bit_mask & 1) == 0)	base_state.pwr_mgmt_2 |= BIT_PWR_GYRO_STBY; // Turn off gyro
		if ((bit_mask & 4) == 0)        base_state.pwr_mgmt_2 |= BIT_PWR_PRESSURE_STBY; // Turn off pressure

		rc |= inv_write_mems_reg(REG_PWR_MGMT_2, 1, &base_state.pwr_mgmt_2);
	}

	return rc;
}

int inv_set_int1_assertion(int enable)
{
	int   result = 0;
	unsigned char reg_pin_cfg = 0;
	unsigned char reg_int_enable = 0;

	// INT1 held until interrupt status is cleared
	result |= inv_read_mems_reg(REG_INT_PIN_CFG, 1, &reg_pin_cfg);
	//reg_pin_cfg    |= BIT_INT_LATCH_EN ;	// Latchen : BIT5 held the IT until register is read
        reg_pin_cfg |= 0x80;	// (1000 0000) Latchen : BIT5 held the IT until register is read
	result |= inv_write_single_mems_reg(REG_INT_PIN_CFG, reg_pin_cfg);

	// Set int1 enable
	result |= inv_read_mems_reg(REG_INT_ENABLE, 1, &reg_int_enable);

	reg_int_enable = (enable)? BIT_DMP_INT_EN:~BIT_DMP_INT_EN;
	result |= inv_write_single_mems_reg(REG_INT_ENABLE, reg_int_enable);
	return result;
}
