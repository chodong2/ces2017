#include "global_define.h"

#ifdef ENABLE_PEDOMETER
/* Private functions ---------------------------------------------------------*/
void flash_erase(uint32_t start_sec, uint32_t end_sec)
{
	uint32_t uwStartSector = 0;
	uint32_t uwEndSector = 0;
	int i = 0;

	/* Get the number of the start and end sectors */
	uwStartSector = GetSector(start_sec);
	uwEndSector = GetSector(end_sec);

	/* Unlock the Flash *********************************************************/
	/* Enable the flash control register access */
	FLASH_Unlock();

	/* Erase the user Flash area ************************************************/
	/* area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR */

	/* Clear pending flags (if any) */
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
		FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

	/* Strat the erase operation */
	for (i = uwStartSector; i < uwEndSector; i += 8)
	{
		/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
		be done by word */
		if (FLASH_EraseSector(i, VoltageRange_3) != FLASH_COMPLETE)
		{
			/* Error occurred while sector erase.
			User can add here some code to deal with this error  */
			printf("FLASH_EraseSector error!!\n");
		}
	}

	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) */
	FLASH_Lock();
}

void flash_write_32(uint32_t st_addr, uint32_t *variable, uint32_t inter, uint32_t size)
{
	uint32_t uwAddress = 0;
	uint32_t start_addr = st_addr;
	uint32_t var_size = size;
	int cnt = 0;
	int number = (var_size / sizeof(uint32_t));
        __IO uint32_t SectorsWRPStatus;

	uwAddress = start_addr + (inter * var_size);

	/* Unlock the Flash *********************************************************/
	/* Enable the flash control register access */
	FLASH_Unlock();
        
	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) */
        for (cnt = 0; cnt<number; cnt++)
        {
                if (FLASH_ProgramWord(uwAddress, variable[cnt]) == FLASH_COMPLETE)
                        uwAddress += sizeof(uint32_t);
                else;
        }

	FLASH_Lock();
}

void flash_write_16(uint32_t st_addr, uint16_t *variable, uint16_t inter, uint16_t size)
{
	uint32_t uwAddress = 0;
	uint32_t start_addr = st_addr;
	uint16_t var_size = size;
	int cnt = 0;
	int number = (var_size / sizeof(uint16_t));
        __IO uint16_t SectorsWRPStatus;

	uwAddress = start_addr + (inter * var_size);

	/* Unlock the Flash *********************************************************/
	/* Enable the flash control register access */
	FLASH_Unlock();
        
	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) */
        for (cnt = 0; cnt<number; cnt++)
        {
                if (FLASH_ProgramHalfWord(uwAddress, variable[cnt]) == FLASH_COMPLETE)
                        uwAddress += sizeof(uint16_t);
                else;
        }

	FLASH_Lock();
}

void flash_read_32(uint32_t st_addr, uint32_t *variable, uint32_t inter, uint32_t size)
{
	uint32_t uwAddress = 0;
	uint32_t start_addr = st_addr;
	uint32_t var_size = size;
	int cnt = 0;
	int number = (var_size / sizeof(uint32_t));

	uwAddress = start_addr + (inter * var_size);

	/* Check if the programmed data is OK ***************************************/
	/*  MemoryProgramStatus = 0: data programmed correctly
	MemoryProgramStatus != 0: number of words not programmed correctly */

	for (cnt = 0; cnt<number; cnt++)
	{
		variable[cnt] = *(__IO uint32_t*)uwAddress;
		uwAddress += sizeof(uint32_t);
	}
}

void flash_read_16(uint32_t st_addr, uint16_t *variable, uint16_t inter, uint16_t size)
{
	uint32_t uwAddress = 0;
	uint32_t start_addr = st_addr;
	uint16_t var_size = size;
	int cnt = 0;
	int number = (var_size / sizeof(uint16_t));

	uwAddress = start_addr + (inter * var_size);

	/* Check if the programmed data is OK ***************************************/
	/*  MemoryProgramStatus = 0: data programmed correctly
	MemoryProgramStatus != 0: number of words not programmed correctly */

	for (cnt = 0; cnt<number; cnt++)
	{
		variable[cnt] = *(__IO uint32_t*)uwAddress;
		uwAddress += sizeof(uint16_t);
	}
}


/**
* @brief  Gets the sector of a given address
* @param  None
* @retval The sector of a given address
*/
uint32_t GetSector(uint32_t Address)
{
	if ((Address >= ADDR_FLASH_SECTOR_0) && (Address < ADDR_FLASH_SECTOR_1))  return FLASH_Sector_0;
	else if ((Address >= ADDR_FLASH_SECTOR_1) && (Address < ADDR_FLASH_SECTOR_2))  return FLASH_Sector_1;
	else if ((Address >= ADDR_FLASH_SECTOR_2) && (Address < ADDR_FLASH_SECTOR_3))  return FLASH_Sector_2;
	else if ((Address >= ADDR_FLASH_SECTOR_3) && (Address < ADDR_FLASH_SECTOR_4))  return FLASH_Sector_3;
	else if ((Address >= ADDR_FLASH_SECTOR_4) && (Address < ADDR_FLASH_SECTOR_5))  return FLASH_Sector_4;
	else if ((Address >= ADDR_FLASH_SECTOR_5) && (Address < ADDR_FLASH_SECTOR_6))  return FLASH_Sector_5;
	else return FLASH_Sector_6;
	//else if((Address >= ADDR_FLASH_SECTOR_6) && (Address < ADDR_FLASH_SECTOR_7))  return FLASH_Sector_6;  
	//else                                                                          return FLASH_Sector_7;  
}
#endif