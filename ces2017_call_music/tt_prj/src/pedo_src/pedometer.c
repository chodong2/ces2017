//_PEDOMETER_C_
#include "global_define.h"

#ifdef ENABLE_PEDOMETER

#define INVEN_INT_PIN                         GPIO_Pin_0
#define INVEN_INT_GPIO_PORT                   GPIOA
#define INVEN_INT_GPIO_CLK                    RCC_AHB1Periph_GPIOA
#define INVEN_INT_EXTI_PORT                   EXTI_PortSourceGPIOA
#define INVEN_INT_EXTI_PIN                    EXTI_PinSource0
#define INVEN_INT_EXTI_LINE                   EXTI_Line0
#define INVEN_INT_EXTI_IRQ                    EXTI0_IRQn

typedef enum BAC_Packet_e_
{
	BAC_DRIVE = (1 << 0),
	BAC_WALK = (1 << 1),
	BAC_RUN = (1 << 2),
	BAC_BIKE = (1 << 3),
	BAC_TILT = (1 << 4),
	BAC_STILL = (1 << 5),
}BAC_Packet_e;


pedometer_struct_t g_str_pedo;

uint32_t g_ui32_pedometer_timer = 0;
uint32_t g_ui32_pedometer_call_app = 0;
uint32_t g_ui32_pedometer_send_cnt = 0;
uint32_t g_ui32_flash_write_cnt = 0;

uint32_t g_ui32_backup_still_time = 0;
uint32_t g_ui32_backup_run_step = 0;
uint32_t g_ui32_backup_run_time = 0;
uint32_t g_ui32_backup_walk_step = 0;
uint32_t g_ui32_backup_walk_time = 0;
uint8_t now_state = 0;
uint32_t g_ui32_pedometer_steps = 0;

uint32_t g_ui32_date = 0;
uint16_t g_ui16_hour = 0;
uint32_t g_ui32_app_date = 0;
uint16_t g_ui16_app_hour = 0;
uint32_t g_ui32_mcu_hour = 0;


/**
  * @brief initialize mpu & dmp for pedometer 
  * @par Parameters None
  * @retval void None
  * @par Required preconditions: None
  */
void pedometer_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

        g_ui32_pedometer_send_cnt = 0;
	g_ui32_pedometer_timer = 0;
	g_ui32_flash_write_cnt = 0;
        
	RCC_AHB1PeriphClockCmd(INVEN_INT_GPIO_CLK, ENABLE);

	/* Configure PC13~15 in input mode */
	GPIO_InitStructure.GPIO_Pin = INVEN_INT_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(INVEN_INT_GPIO_PORT, &GPIO_InitStructure);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	SYSCFG_EXTILineConfig(INVEN_INT_EXTI_PORT, INVEN_INT_EXTI_PIN);

	/* PA0 is connected to EXTI_Line0 */
	EXTI_InitStructure.EXTI_Line = INVEN_INT_EXTI_LINE;
	/* Enable interrupt */
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	/* Interrupt mode */
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	/* Triggers on rising edge */
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	/* Add to EXTI */
	EXTI_Init(&EXTI_InitStructure);

	/* EXTI13~15 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = INVEN_INT_EXTI_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

         // Init MEMS
         /* Initialize MEMS chip for I2C channel communication :
         - Load DMP image
         - Configure DMP FIFO watermark
         - Enable DMP interrupts at DMP level
         - Configure gyro fullscale to 2000dps and accel fullscale to 2g by default
         - Put MEMS in low power */
	if (init_motion_sensor_driver() != INV_SUCCESS)
                SCB->AIRCR = (0x05FA0000) | 0x04;
}

void motion_sensor_isr(void)
{
        if (EXTI_GetITStatus(EXTI_Line0) != RESET)
        {
                g_str_bitflag.b1_motion_chk = 1;

                /* Clear interrupt flag */
                EXTI_ClearITPendingBit(EXTI_Line0);
        }
}

void pedometer_data_transfer(void)
{
        uint32_t pedometer_data[8] = {0,};
        uint32_t cnt = 0;
        uint8_t str[64] = {0,};
        uint8_t send_str[64] = {0,};
	uint32_t bac_ts = 0;
        uint32_t timestamp = 0;

        // Call from app on time
        if( g_ui32_pedometer_call_app == 1 )//$20보냈을때
        {
                uint32_t backup_date = 0;
                uint32_t backup_hour = 0;
                
                dmp_get_bac_ts((unsigned long *)&bac_ts); //10ms check
                timestamp = (bac_ts / 10);//100ms 단위

                switch(now_state)
                {
                        case BAC_STILL:
                                g_str_pedo.ui32_total_still_step = 0;
                                g_str_pedo.ui32_total_still_time += (timestamp - g_ui32_backup_still_time);
                                g_ui32_backup_still_time = timestamp;
                                break;

                        case BAC_WALK:
                                g_str_pedo.ui32_total_walk_step += (g_ui32_pedometer_steps - g_ui32_backup_walk_step);
                                g_str_pedo.ui32_total_walk_time += (timestamp - g_ui32_backup_walk_time);
                                g_ui32_backup_walk_step = g_ui32_pedometer_steps;
                                g_ui32_backup_walk_time = timestamp;
                                break;

                        case BAC_RUN:
                                g_str_pedo.ui32_total_run_step += (g_ui32_pedometer_steps - g_ui32_backup_run_step);
                                g_str_pedo.ui32_total_run_time += (timestamp - g_ui32_backup_run_time);
                                g_ui32_backup_run_step = g_ui32_pedometer_steps;
                                g_ui32_backup_run_time = timestamp;
                                break;
                }

                if(!g_ui32_flash_write_cnt)
                {
                        cnt = 0;
                        while(cnt < 744)//flash 끝까지 읽고 count값 얻어옴
                        {
                                flash_read_32(FLASH_ADDR_START_PEDO, pedometer_data, cnt, (uint32_t)sizeof(pedometer_data));
                                
                                if(pedometer_data[6] == 0xffffffff)//데이터없는경우
                                {
                                        if(cnt)//중간에있는경우
                                        {
                                                if(g_ui32_mcu_hour)//동기화x
                                                {
                                                        g_ui32_app_date = backup_date;
                                                        g_ui16_app_hour = backup_hour+1;//
                                                }
                                                g_ui32_flash_write_cnt = cnt;
                                                break;//
                                        }
                                        else//처음부터없을경우
                                        {
                                                if(g_ui32_mcu_hour)//동기화x
                                                {
                                                        g_ui32_app_date = 0;
                                                        g_ui16_app_hour = 0;
                                                        g_ui32_flash_write_cnt = 0;
                                                }
                                                break;//
                                        }
                                }
                                else//데이터있는경우
                                {
                                        backup_date = pedometer_data[4];
                                        backup_hour = pedometer_data[5];
                                }
                                cnt++;
                        }
                }
                
                pedometer_data[0] = g_str_pedo.ui32_total_walk_step;
                pedometer_data[1] = g_str_pedo.ui32_total_walk_time;
                pedometer_data[2] = g_str_pedo.ui32_total_run_step;
                pedometer_data[3] = g_str_pedo.ui32_total_run_time;

                if(g_ui32_mcu_hour)
                {
                        g_ui16_app_hour++;
                        if(g_ui16_app_hour == 24)
                        {       
                                uint32_t year = 0;
                                uint32_t month = 0;
                                uint32_t days = 0;

                                g_ui16_app_hour = 0;
                                year = (g_ui32_app_date/10000);
                                month = (g_ui32_app_date/100)%100;
                                days = (g_ui32_app_date % 100);
                                if( ( (month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12) ) && (days == 31) )
                                {
                                        month++;
                                        days = 1;
                                        if(month == 13)
                                        {
                                                year++;
                                                month = 1;
                                        }
                                        g_ui32_app_date = (year*10000)+(month*100)+days;
                                }
                                else if( ( (month == 4) || (month == 6) || (month == 9) || (month == 11) ) && (days == 30) )
                                {
                                        //161130 24
                                        month++;
                                        days = 1;
                                        g_ui32_app_date = (year*10000)+(month*100)+days;
                                }
                                else if(month == 2)
                                {
                                       if( ( ( (year / 4) == 0 ) && (days == 29) ) || ( ( (year / 4) != 0 ) && (days == 28) ) )//윤년
                                       {
                                                month++;
                                                days = 1;
                                                g_ui32_app_date = (year*10000)+(month*100)+days;
                                       }
                                }
                                else
                                {
                                        days++;
                                        g_ui32_app_date = (year*10000)+(month*100)+days;
                                }
                        }
                }

                pedometer_data[4] = g_ui32_app_date;
                pedometer_data[5] = g_ui16_app_hour - 1;
                pedometer_data[6] = g_ui32_flash_write_cnt % 24;
                pedometer_data[7] = 0;

                flash_write_32(FLASH_ADDR_START_PEDO, pedometer_data, g_ui32_flash_write_cnt, sizeof(pedometer_data));

                ++g_ui32_flash_write_cnt;
                g_ui32_pedometer_timer = 0;
                g_ui32_mcu_hour = 0;
                memset((pedometer_struct_t *)&g_str_pedo, 0x00, sizeof(g_str_pedo));
                g_ui32_pedometer_call_app = 0;
        }
        else if( g_ui32_pedometer_call_app == 2 )//$21 보냈을때
        {
                memset(str, 0x00, sizeof(str));
                memset(send_str, 0x00, sizeof(send_str));
                
                if( (g_ui32_app_date == g_ui32_date) && (g_ui16_app_hour == g_ui16_hour) )//실시간 데이터 보내기
                {
#if 0
                        dmp_get_bac_ts((unsigned long *)&bac_ts); //10ms check
                        timestamp = (bac_ts / 10);//100ms 단위

                        switch(now_state)
                        {
                                case BAC_STILL:
                                        g_str_pedo.ui32_total_still_step = 0;
                                        g_str_pedo.ui32_total_still_time += (timestamp - g_ui32_backup_still_time);
                                        g_ui32_backup_still_time = timestamp;
                                        break;

                                case BAC_WALK:
                                        g_str_pedo.ui32_total_walk_step += (g_ui32_pedometer_steps - g_ui32_backup_walk_step);
                                        g_str_pedo.ui32_total_walk_time += (timestamp - g_ui32_backup_walk_time);
                                        g_ui32_backup_walk_step = g_ui32_pedometer_steps;
                                        g_ui32_backup_walk_time = timestamp;
                                        break;

                                case BAC_RUN:
                                        g_str_pedo.ui32_total_run_step += (g_ui32_pedometer_steps - g_ui32_backup_run_step);
                                        g_str_pedo.ui32_total_run_time += (timestamp - g_ui32_backup_run_time);
                                        g_ui32_backup_run_step = g_ui32_pedometer_steps;
                                        g_ui32_backup_run_time = timestamp;
                                        break;
                        }

#endif
                        pedometer_data[0] = g_str_pedo.ui32_total_walk_step;
                        pedometer_data[1] = g_str_pedo.ui32_total_walk_time;
                        pedometer_data[2] = g_str_pedo.ui32_total_run_step;
                        pedometer_data[3] = g_str_pedo.ui32_total_run_time;
                        
                        pedometer_data[4] = g_ui32_app_date;//No zero day
                        pedometer_data[5] = g_ui16_app_hour;

                        sprintf((char *)str,"$22$%d$%d$%d:%d.%d$%d:%d.%d",pedometer_data[4],pedometer_data[5],
                                                                        pedometer_data[0],pedometer_data[1]/10,pedometer_data[1]%10,
                                                                        pedometer_data[2],pedometer_data[3]/10,pedometer_data[3]%10);
                        
                        sprintf((char *)send_str,"$%d%s\r",strlen((char *)str),str);
                        BT_SendSPPData(send_str, strlen((char *)send_str), 0);
                        g_ui32_pedometer_call_app = 0;
                }
                else//저장된 데이터 보내기
                {
                        cnt = 0;

                        while(1)//360까지 보름 //720이면 30일
                        {
                                flash_read_32(FLASH_ADDR_START_PEDO, pedometer_data, cnt, (uint32_t)sizeof(pedometer_data));

                                if((pedometer_data[4] == 0xffffffff) || (pedometer_data[5] == 0xffffffff))
                                {
                                        sprintf((char *)str,"$22$%d$%d$0:0.0$0:0.0",g_ui32_date,g_ui16_hour);
                                        sprintf((char *)send_str,"$%d%s\r",strlen((char *)str),str);
                                        BT_SendSPPData(send_str, strlen((char *)send_str), 0);
                                        break;
                                }
                                else if((g_ui32_date == pedometer_data[4]) && (g_ui16_hour == pedometer_data[5]))
                                {
                                        sprintf((char *)str,"$22$%d$%d$%d:%d.%d$%d:%d.%d",pedometer_data[4],pedometer_data[5],
                                                                                        pedometer_data[0],pedometer_data[1]/10,pedometer_data[1]%10,
                                                                                        pedometer_data[2],pedometer_data[3]/10,pedometer_data[3]%10);
                                        sprintf((char *)send_str,"$%d%s\r",strlen((char *)str),str);
                                        BT_SendSPPData(send_str, strlen((char *)send_str), 0);
                                        g_ui32_pedometer_send_cnt++;
                                        break;
                                }
                                
                                cnt++;
                        }

                        g_ui32_pedometer_call_app = 0;
                }
        }

}

void pedometer(void)
{
        if(g_ui32_pedometer_call_app)
        {
                pedometer_data_transfer();
        }

        pedometer_check();
}

static uint8_t state_move = 0;
static uint8_t state_high = 0;
static uint8_t state_low = 0;
static uint8_t state_stop = 0;
static uint32_t data_left_in_fifo = 0;
static uint16_t bac_state = 0;
static uint32_t bac_ts = 0;
static uint32_t timestamp = 0;

static uint16_t header = 0;
static uint16_t header2 = 0;

/**
  * @brief processing pedometer data of mpu.
  * @par Parameters None
  * @retval void None
  * @par Required preconditions: None
  */
void pedometer_check(void)
{
        if(g_str_bitflag.b1_motion_chk)
        {
                //static uint16_t intr_status = 0;
                //inv_identify_interrupt(&intr_status);
                //if(intr_status & (BIT_MSG_DMP_INT | BIT_MSG_DMP_INT_0))
                {
                        // data main loop for header packet from fifo
                        do
                        {
                                // Read FIFO contents and parse it.
                                dmp_process_fifo(&data_left_in_fifo, &header, &header2);

                                // Activity recognition sample available from DMP FIFO
                                if (header2 & ACT_RECOG_SET)
                                {
                                        // Read activity type and associated timestamp out of DMP FIFO
                                        // activity type is a set of 2 bytes :
                                        // - high byte indicates activity start
                                        // - low byte indicates activity end 
                                        dmp_get_bac_state(&bac_state);
                                        dmp_get_bac_ts((unsigned long *)&bac_ts); //10ms check
                                        timestamp = (bac_ts / 10);//100ms 단위

                                        // Tilt information is inside BAC, so extract it out 
                                        state_high = (bac_state >> 8);
                                        state_low = bac_state;

                                        if (state_high & BAC_STILL)
                                        {
                                                g_ui32_backup_still_time = timestamp;
                                                state_move = 0;
                                                state_stop = 1;
                                                now_state = BAC_STILL;
                                                //BT_SendSPPData("still", 5, 0);
                                        }
                                        else if ( (state_low & BAC_STILL) )
                                        {
                                                if(state_stop)
                                                {
                                                        g_str_pedo.ui32_still_step = 0;
                                                        g_str_pedo.ui32_still_time = timestamp - g_ui32_backup_still_time;
                                                        g_str_pedo.ui32_total_still_step += g_str_pedo.ui32_still_step;
                                                        g_str_pedo.ui32_total_still_time += g_str_pedo.ui32_still_time;

                                                        g_ui32_backup_still_time = timestamp;
                                                        state_stop = 0;
                                                        state_move = 1;
                                                        //BT_SendSPPData("stills", 6, 0);
                                                }
                                        }

                                        if (state_high & BAC_WALK)
                                        {
                                                g_ui32_backup_walk_step = g_ui32_pedometer_steps;
                                                g_ui32_backup_walk_time = timestamp;
                                                state_move = 1;
                                                now_state = BAC_WALK;
                                                //BT_SendSPPData("walk", 4, 0);
                                        }
                                        else if( (state_low & BAC_WALK) )
                                        {
                                                if ((g_ui32_backup_walk_step != g_ui32_pedometer_steps))
                                                {
                                                        g_str_pedo.ui32_walk_step = g_ui32_pedometer_steps - g_ui32_backup_walk_step;
                                                        g_str_pedo.ui32_walk_time = timestamp - g_ui32_backup_walk_time;
                                                        g_str_pedo.ui32_total_walk_step += g_str_pedo.ui32_walk_step;
                                                        g_str_pedo.ui32_total_walk_time += g_str_pedo.ui32_walk_time;
                                                        
                                                        g_ui32_backup_walk_step = g_ui32_pedometer_steps;
                                                        g_ui32_backup_walk_time = timestamp;

                                                        //flag_walk = 1;
                                                        state_move = 0;
                                                        //BT_SendSPPData("walks", 5, 0);
                                                }
                                        }

                                        if (state_high & BAC_RUN)
                                        {
                                                g_ui32_backup_run_step = g_ui32_pedometer_steps;
                                                g_ui32_backup_run_time = timestamp;
                                                state_move = 1;
                                                now_state = BAC_RUN;
                                                //BT_SendSPPData("run", 3, 0);
                                        }
                                        else if(state_low & BAC_RUN)
                                        {
                                                if ((g_ui32_backup_run_step != g_ui32_pedometer_steps))
                                                {
                                                        g_str_pedo.ui32_run_step = g_ui32_pedometer_steps - g_ui32_backup_run_step;
                                                        g_str_pedo.ui32_run_time = timestamp - g_ui32_backup_run_time;
                                                        g_str_pedo.ui32_total_run_step += g_str_pedo.ui32_run_step;
                                                        g_str_pedo.ui32_total_run_time += g_str_pedo.ui32_run_time;
                                                        
                                                        g_ui32_backup_run_step = g_ui32_pedometer_steps;
                                                        g_ui32_backup_run_time = timestamp;
                                                        //flag_run = 1;
                                                        state_move = 0;
                                                        //BT_SendSPPData("runs", 4, 0);
                                                }
                                        }
                                }

                                // Step detector available from DMP FIFO and step counter sensor is enabled
                                if (header & PED_STEPDET_SET)
                                {
                                        if(state_move)
                                        {
                                                dmp_get_pedometer_num_of_steps(&g_ui32_pedometer_steps);

                                                //uint8_t str[3];
                                                //sprintf((char *)&str, "%d", g_ui32_pedometer_steps);
                                                //BT_SendSPPData(str, strlen((char *)&str), 0);
                                        }
                                }
                                if(!data_left_in_fifo)
                                        g_str_bitflag.b1_motion_chk = 0;

                                g_str_timer.ui16_sleep_cnt = 0;
                                
                        }while (data_left_in_fifo);
                }
        }
}


#endif