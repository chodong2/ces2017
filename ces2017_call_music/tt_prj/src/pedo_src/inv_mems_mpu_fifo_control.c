/*
* ________________________________________________________________________________________________________
* Copyright ?2014-2015 InvenSense Inc. Portions Copyright ?2014-2015 Movea. All rights reserved.
* This software, related documentation and any modifications thereto (collectively �Software? is subject
* to InvenSense and its licensors' intellectual property rights under U.S. and international copyright and
* other intellectual property rights laws.
* InvenSense and its licensors retain all intellectual property and proprietary rights in and to the Software
* and any use, reproduction, disclosure or distribution of the Software without an express license
* agreement from InvenSense is strictly prohibited.
* ________________________________________________________________________________________________________
*/

#include "inv_defines.h"

struct inv_fifo_decoded_t fd;

static struct fifo_info_t
{
	int   fifoError;
	unsigned char fifo_overflow;
} fifo_info = { 0 };

int mpu_set_FIFO_RST_Diamond(unsigned char value)
{
	int result = 0;
	unsigned char reg;

	result |= inv_read_mems_reg(REG_FIFO_RST, 1, &reg);

	reg &= 0xe0;
	reg |= value;
	result |= inv_write_mems_reg(REG_FIFO_RST, 1, &reg);

	return result;
}

int inv_identify_interrupt(unsigned short *int_read)
{
	unsigned char int_status;
	int result = 0;

	if (int_read)
		*int_read = 0;

	result = inv_read_mems_reg(REG_INT_STATUS, 1, &int_status);
	if (int_read)
		*int_read = int_status;

	result = inv_read_mems_reg(REG_DMP_INT_STATUS, 1, &int_status);
	if (int_read)
		*int_read |= (int_status << 8);

	/*
	* We do not need to handle FIFO overflow here.
	* When we read FIFO_SIZE we can determine if FIFO overflow has occured.
	*/
	//result = inv_read_mems_reg(0x1B, 1, &int_status);

	return result;
}

/**
* @internal
* @brief   Get the length from the fifo
*
* @param[out] len amount of data currently stored in the fifo.
*
* @return MPU_SUCCESS or non-zero error code.
**/
static int dmp_get_fifo_length(uint_fast16_t * len)
{
	unsigned char fifoBuf[2];
	int result = 0;

	if (NULL == len)
		return INV_ERROR_INVALID_PARAMETER;

	/*---- read the 2 'count' registers and
	burst read the data from the FIFO ----*/
	result = inv_read_mems_reg(REG_FIFO_COUNT_H, 2, fifoBuf);
	if (result)
	{
		fifo_info.fifoError = INV_ERROR_FIFO_READ_COUNT;
		*len = 0;
		return result;
	}

	*len = (uint_fast16_t)(fifoBuf[0] << 8);
	*len += (uint_fast16_t)(fifoBuf[1]);

	return result;
}

/**
*  @internal
*  @brief  Clears the FIFO status and its content.
*  @note   Halt the DMP writing into the FIFO for the time
*          needed to reset the FIFO.
*  @return MPU_SUCCESS if successful, a non-zero error code otherwise.
*/
static int dmp_reset_fifo(void)
{
	uint_fast16_t len = HARDWARE_FIFO_SIZE;
	unsigned char tries = 0;
	unsigned char user_ctrl_reg;
	int result = 0;

	result |= inv_read_mems_reg(REG_USER_CTRL, 1, &user_ctrl_reg);

	while (len != 0 && tries < 6)
	{
		result |= inv_write_single_mems_reg(REG_USER_CTRL, (user_ctrl_reg  & (~BIT_FIFO_EN)));
		result |= mpu_set_FIFO_RST_Diamond(0x1f);
		result |= mpu_set_FIFO_RST_Diamond(0x1e);

		// Reset overflow flag
		fifo_info.fifo_overflow = 0;

		result |= dmp_get_fifo_length(&len);
		if (result)
			return result;

		tries++;
	}

	result |= inv_write_single_mems_reg(REG_USER_CTRL, user_ctrl_reg | BIT_FIFO_EN);

	return result;
}

/**
*  @internal
*  @brief  Read data from the fifo
*
*  @param[out] data Location to store the date read from the fifo
*  @param[in] len   Amount of data to read out of the fifo
*
*  @return MPU_SUCCESS or non-zero error code
**/
static int dmp_read_fifo(unsigned char *data, uint_fast16_t len)
{
	int result;
	uint_fast16_t bytesRead = 0;

	while (bytesRead<len)
	{
		unsigned short thisLen = min(INV_MAX_SERIAL_READ, len - bytesRead);

		result = inv_read_mems_reg(REG_FIFO_R_W, thisLen, &data[bytesRead]);
		if (result)
		{
			dmp_reset_fifo();
			fifo_info.fifoError = INV_ERROR_FIFO_READ_DATA;
			return result;
		}

		bytesRead += thisLen;
	}

	return result;
}

/**
*  @internal
*  @brief  used to get the FIFO data.
*  @param  length
*              Max number of bytes to read from the FIFO.
*  @param  buffer Reads up to length into the buffer.
*
*  @return number of bytes of read.
**/
static uint_fast16_t dmp_get_fifo_all(uint_fast16_t length, unsigned char *buffer, int *reset)
{
	int result;
	uint_fast16_t in_fifo;

	if (reset)
		*reset = 0;

	result = dmp_get_fifo_length(&in_fifo);
	if (result)
	{
		fifo_info.fifoError = result;
		return 0;
	}

	if (in_fifo >= HARDWARE_FIFO_SIZE)
	{
		dmp_reset_fifo();
		fifo_info.fifoError = INV_ERROR_FIFO_OVERFLOW;
		if (reset)
			*reset = 1;
		return 0;
	}

	// Nothing to read
	if (in_fifo == 0)
		return 0;

	result = dmp_read_fifo(&buffer[0], in_fifo);
	if (result)
	{
		fifo_info.fifoError = result;
		return 0;
	}
	return in_fifo;
}

/** Determines the packet size by decoding the header. Both header and header2 are set. header2 is set to zero
*   if it doesn't exist.
*/
static uint_fast16_t dmp_get_packet_size(unsigned char *data, unsigned short *header, unsigned short *header2)
{
	int sz = HEADER_SZ; // 2 for header

	*header = (((unsigned short)data[0]) << 8) | data[1];

	if (*header & PED_STEPDET_SET)
		sz += PED_STEPDET_TIMESTAMP_SZ;

	if (*header & HEADER2_SET) {
		*header2 = (((unsigned short)data[2]) << 8) | data[3];
		sz += HEADER2_SZ;
	}
	else {
		*header2 = 0;
	}

	if (*header2 & ACT_RECOG_SET) {
		sz += ACT_RECOG_SZ;
	}

	return sz;
}

int dmp_process_fifo(unsigned int *left_in_fifo, unsigned short *user_header, unsigned short *user_header2)
{
	int result = MPU_SUCCESS;
	int reset = 0;
	unsigned char fifo_data[HARDWARE_FIFO_SIZE]; //FIFO_READ_SIZE
	int need_sz = 0;
	unsigned char *fifo_ptr = fifo_data;
        int in_fifo = 0;

	if (!left_in_fifo)
		return -1;

	if (*left_in_fifo < HARDWARE_FIFO_SIZE)
	{
#if 0
		*left_in_fifo += dmp_get_fifo_all((HARDWARE_FIFO_SIZE - *left_in_fifo), &fifo_data[*left_in_fifo], &reset);
#else
		*left_in_fifo = dmp_get_fifo_all((HARDWARE_FIFO_SIZE - *left_in_fifo), &fifo_data[*left_in_fifo], &reset);
#endif

		if (reset)
		{
			*left_in_fifo = 0;
			return MPU_SUCCESS;
		}
	}

	if (*left_in_fifo > 3)
        {
		need_sz = dmp_get_packet_size(fifo_data, &fd.header, &fd.header2);

		// Guarantee there is a full packet before continuing to decode the FIFO packet
		if (*left_in_fifo < need_sz) {
			result = fifo_info.fifoError;
			fifo_info.fifoError = 0;
			return result;
		}

		if (user_header)
			*user_header = fd.header;

		if (user_header2)
			*user_header2 = fd.header2;

		if (fd.header == 0x0000)
                {
			// Decode error
			dmp_reset_fifo();
			*left_in_fifo = 0;
			return MPU_SUCCESS;
		}

		fifo_ptr += HEADER_SZ;

		if (fd.header & HEADER2_SET)
			fifo_ptr += HEADER2_SZ;

		fifo_ptr += inv_decode_one_ivory_fifo_packet(&fd, fifo_ptr);

		/* Parse the data in the fifo, in the order of the data control register, starting with the MSB(accel)
		*/


		*left_in_fifo -= need_sz;
		if (*left_in_fifo)
			memmove(fifo_data, &fifo_data[need_sz], *left_in_fifo);// Data left in FIFO
	}

	return result;
}

void inv_decode_3_16bit_elements(short *out_data, const unsigned char *in_data)
{
	out_data[0] = ((short)(0xff & in_data[0]) << 8) | (0xff & in_data[1]);
	out_data[1] = ((short)(0xff & in_data[2]) << 8) | (0xff & in_data[3]);
	out_data[2] = ((short)(0xff & in_data[4]) << 8) | (0xff & in_data[5]);
}

/** Decodes one packet of data from Ivory FIFO
* @param[in] fd Structure to be filled out with data. Assumes header and header2 are already set inside.
* @param[in] fifo_ptr FIFO data, points to just after any header information
* @return Returns the number of bytes consumed in FIFO data.
*/
int inv_decode_one_ivory_fifo_packet(struct inv_fifo_decoded_t *fd, const unsigned char *fifo_ptr)
{
	const unsigned char *fifo_ptr_start = fifo_ptr;

	if (fd->header & PED_STEPDET_SET) {
		fd->ped_step_det_ts = ((0xff & fifo_ptr[0]) << 24) | ((0xff & fifo_ptr[1]) << 16) | ((0xff & fifo_ptr[2]) << 8) | (0xff & fifo_ptr[3]);
		fifo_ptr += PED_STEPDET_TIMESTAMP_SZ;
	}

	if (fd->header2 & ACT_RECOG_SET) {
		fd->bac_state = ((0xff & fifo_ptr[0]) << 8) | (0xff & fifo_ptr[1]);
		fd->bac_ts = ((0xff & fifo_ptr[2]) << 24) | ((0xff & fifo_ptr[3]) << 16) | ((0xff & fifo_ptr[4]) << 8) | (0xff & fifo_ptr[5]);
		fifo_ptr += ACT_RECOG_SZ;
	}

	fd->new_data = 1; // Record a new data set

	return fifo_ptr - fifo_ptr_start;
}

int dmp_get_accel(long acl[3])
{
	if (!acl) return -1;
	memcpy(acl, fd.accel, 3 * sizeof(long));
	return MPU_SUCCESS;
}

int dmp_get_bac_state(uint16_t *bac_state)
{
	if (!bac_state) return -1;
	*bac_state = fd.bac_state;
	return INV_SUCCESS;
}

int dmp_get_bac_ts(unsigned long *bac_ts)
{
	if (!bac_ts) return -1;
	*bac_ts = fd.bac_ts;
	return INV_SUCCESS;
}
