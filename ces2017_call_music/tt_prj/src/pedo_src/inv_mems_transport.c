/*
* ________________________________________________________________________________________________________
* Copyright ?2014-2015 InvenSense Inc. Portions Copyright ?2014-2015 Movea. All rights reserved.
* This software, related documentation and any modifications thereto (collectively �Software? is subject
* to InvenSense and its licensors' intellectual property rights under U.S. and international copyright and
* other intellectual property rights laws.
* InvenSense and its licensors retain all intellectual property and proprietary rights in and to the Software
* and any use, reproduction, disclosure or distribution of the Software without an express license
* agreement from InvenSense is strictly prohibited.
* ________________________________________________________________________________________________________
*/

#include "inv_defines.h"

static unsigned char lLastBankSelected = 0xFF;

#define MAX_CH_NUM      12
// i2c Commands:  4 bits, [7:4]
#define I2C_MASTER_WRITE_COMMAND                0x10
#define I2C_MASTER_READ_COMMAND                 0x20
#define I2C_MASTER_WAIT_COMMAND                 0x40
#define I2C_MASTER_READ_DONE_COMMAND            0x80
#define I2C_MASTER_STOP_COMMAND                 0x00


static uint8_t check_reg_access_lp_disable(unsigned short reg)
{
	switch (reg) {
	case 0x05:
	case 0x06:
	case 0x07:
	case 0x0f:
	case 0x10:
	case 0x70:
	case 0x71:
	case 0x72:
	case 0x76:
	case 0x7e:
	case 0x7f:
		return 0;
		break;
	default:

		break;
	}
	return 1;
}

/**
*  @brief      Set up the register bank register for accessing registers in 20630.
*  @param[in]  register bank number
*  @return     0 if successful.
*/
//if bank reg was set before, just return

int inv_set_bank(unsigned char bank)
{
	int result = 0;
	unsigned char reg;
        static unsigned char lastBank = 0x7E;

	if (bank == lastBank)  		return 0;
	else            		lastBank = bank;
	result = Sensors_I2C_ReadRegister(ICM20648_I2C_ADDR, REG_BANK_SEL, 1, &reg);
	if (result)
		return result;
        //TM_I2C_ReadMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_BANK_SEL, &reg, 1);

	reg &= 0xce;
	reg |= (bank << 4);

	result = Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_BANK_SEL, 1, &reg);
        //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_BANK_SEL, &reg, 1);

	return result;
}

/* the following functions are used for configuring the secondary devices */

/**
*  @brief      Write data to a register on MEMs.
*  @param[in]  Register address
*  @param[in]  Length of data
*  @param[in]  Data to be written
*  @return     0 if successful.
*/
int inv_write_mems_reg(uint16_t reg, unsigned int length, unsigned char *data)
{
	int result = 0;
	unsigned int bytesWrite = 0;
	unsigned char regOnly = (unsigned char)(reg & 0x7F);

	unsigned char power_state = inv_get_chip_power_state();

	if ((power_state & CHIP_AWAKE) == 0)   // Wake up chip since it is asleep
                result = inv_set_chip_power_state(CHIP_AWAKE, 1);

	if (check_reg_access_lp_disable(reg))    // Check if register needs LP_EN to be disabled   
                result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 0);  //Disable LP_EN

	result |= inv_set_bank(reg >> 7);

	while (bytesWrite<length)
	{
		int thisLen = min(INV_MAX_SERIAL_WRITE, length - bytesWrite);

		result |= Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, regOnly + bytesWrite, thisLen, &data[bytesWrite]);
		if (result)
			return result;
                //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, regOnly + bytesWrite, &data[bytesWrite], thisLen);


		bytesWrite += thisLen;
	}

	if (check_reg_access_lp_disable(reg))   //Enable LP_EN since we disabled it at begining of this function.
                result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 1);

	return result;
}

/**
*  @brief      Write single byte of data to a register on MEMs.
*  @param[in]  Register address
*  @param[in]  Data to be written
*  @return     0 if successful.
*/
int inv_write_single_mems_reg(uint16_t reg, unsigned char data)
{
	int result = 0;
	unsigned char regOnly = (unsigned char)(reg & 0x7F);


	unsigned char power_state = inv_get_chip_power_state();

	if ((power_state & CHIP_AWAKE) == 0)   // Wake up chip since it is asleep
		result = inv_set_chip_power_state(CHIP_AWAKE, 1);

	if (check_reg_access_lp_disable(reg))   // Check if register needs LP_EN to be disabled
		result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 0);  //Disable LP_EN

	result |= inv_set_bank(reg >> 7);
	result |= Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, regOnly, 1, &data);
        //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, regOnly, &data, 1);
        
	if (check_reg_access_lp_disable(reg))   //Enable LP_EN since we disabled it at begining of this function.
		result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 1);

	return result;
}

/**
*  @brief      Read data from a register on MEMs.
*  @param[in]  Register address
*  @param[in]  Length of data
*  @param[in]  Data to be written
*  @return     0 if successful.
*/
int inv_read_mems_reg(uint16_t reg, unsigned int length, unsigned char *data)
{
	int result = 0;
	unsigned int bytesRead = 0;
	unsigned char regOnly = (unsigned char)(reg & 0x7F);
	unsigned char power_state = inv_get_chip_power_state();

	if ((power_state & CHIP_AWAKE) == 0)   // Wake up chip since it is asleep
		result = inv_set_chip_power_state(CHIP_AWAKE, 1);

	if (check_reg_access_lp_disable(reg))   // Check if register needs LP_EN to be disabled
		result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 0);  //Disable LP_EN

	result |= inv_set_bank(reg >> 7);

	while (bytesRead<length)
	{
		int thisLen = min(INV_MAX_SERIAL_READ, length - bytesRead);
                result |= Sensors_I2C_ReadRegister(ICM20648_I2C_ADDR, regOnly + bytesRead, thisLen, &data[bytesRead]);
                if (result)
                	return result;
                //TM_I2C_ReadMulti(SENSORS_I2C, ICM20648_I2C_ADDR, regOnly + bytesRead, &data[bytesRead], thisLen);
                
		bytesRead += thisLen;
	}

	if (check_reg_access_lp_disable(reg))   //Enable LP_EN since we disabled it at begining of this function.
		result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 1);

	return result;
}

/**
*  @brief      Read data from a register in DMP memory
*  @param[in]  DMP memory address
*  @param[in]  number of byte to be read
*  @param[in]  input data from the register
*  @return     0 if successful.
*/
int inv_read_mems(unsigned short reg, unsigned int length, unsigned char *data)
{
	int result = 0;
	unsigned int bytesWritten = 0;
	unsigned int thisLen;
	unsigned char i, dat[INV_MAX_SERIAL_READ + 1];
	unsigned char power_state = inv_get_chip_power_state();
	unsigned char lBankSelected;
	unsigned char lStartAddrSelected;

	if (!data)
		return -1;

	if ((power_state & CHIP_AWAKE) == 0)   // Wake up chip since it is asleep
		result = inv_set_chip_power_state(CHIP_AWAKE, 1);

	result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 0);

	result |= inv_set_bank(0);

	lBankSelected = (reg >> 8);
	if (lBankSelected != lLastBankSelected)
	{
		result |= Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_MEM_BANK_SEL, 1, &lBankSelected);
		if (result)
			return result;
                //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_MEM_BANK_SEL, &lBankSelected, 1);
          
		lLastBankSelected = lBankSelected;
	}

	while (bytesWritten < length)
	{
		lStartAddrSelected = (reg & 0xff);
		/* Sets the starting read or write address for the selected memory, inside of the selected page (see MEM_SEL Register).
		Contents are changed after read or write of the selected memory.
		This register must be written prior to each access to initialize the register to the proper starting address.
		The address will auto increment during burst transactions.  Two consecutive bursts without re-initializing the start address would skip one address. */
		result |= Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_MEM_START_ADDR, 1, &lStartAddrSelected);
		if (result)
			return result;
                //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_MEM_START_ADDR, &lStartAddrSelected, 1);

		thisLen = min(INV_MAX_SERIAL_READ, length - bytesWritten);

		/* Write data */
                result |= Sensors_I2C_ReadRegister(ICM20648_I2C_ADDR, REG_MEM_R_W, thisLen, &data[bytesWritten]);
		if (result)
			return result;

                //TM_I2C_ReadMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_MEM_R_W, &data[bytesWritten], thisLen);

		bytesWritten += thisLen;
		reg += thisLen;
	}

	//Enable LP_EN since we disabled it at begining of this function.
	result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 1);

	return result;
}

/**
*  @brief       Write data to a register in DMP memory
*  @param[in]   DMP memory address
*  @param[in]   number of byte to be written
*  @param[out]  output data from the register
*  @return     0 if successful.
*/
int inv_write_mems(unsigned short reg, unsigned int length, unsigned char *data)
{
	int result = 0;
	unsigned int bytesWritten = 0;
	unsigned int thisLen;
	unsigned char lBankSelected;
	unsigned char lStartAddrSelected;
	unsigned char power_state = inv_get_chip_power_state();

	if (!data)
		return -1;

	if ((power_state & CHIP_AWAKE) == 0)   // Wake up chip since it is asleep
		result = inv_set_chip_power_state(CHIP_AWAKE, 1);

	result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 0);

	result |= inv_set_bank(0);

	lBankSelected = (reg >> 8);
	if (lBankSelected != lLastBankSelected)
	{
		result |= Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_MEM_BANK_SEL, 1, &lBankSelected);
		if (result)
			return result;
                //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_MEM_BANK_SEL, &lBankSelected, 1);
          
		lLastBankSelected = lBankSelected;
	}

	while (bytesWritten < length)
	{
		lStartAddrSelected = (reg & 0xff);
		/* Sets the starting read or write address for the selected memory, inside of the selected page (see MEM_SEL Register).
		Contents are changed after read or write of the selected memory.
		This register must be written prior to each access to initialize the register to the proper starting address.
		The address will auto increment during burst transactions.  Two consecutive bursts without re-initializing the start address would skip one address. */
		result |= Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_MEM_START_ADDR, 1, &lStartAddrSelected);
		if (result)
			return result;
                //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_MEM_START_ADDR, &lStartAddrSelected, 1);
               
		thisLen = min(INV_MAX_SERIAL_WRITE, length - bytesWritten);

		/* Write data */
		result |= Sensors_I2C_WriteRegister(ICM20648_I2C_ADDR, REG_MEM_R_W, thisLen, &data[bytesWritten]);
		if (result)
			return result;
                //TM_I2C_WriteMulti(SENSORS_I2C, ICM20648_I2C_ADDR, REG_MEM_R_W, &data[bytesWritten], thisLen);

		bytesWritten += thisLen;
		reg += thisLen;
	}

	//Enable LP_EN since we disabled it at begining of this function.
	result |= inv_set_chip_power_state(CHIP_LP_ENABLE, 1);

	return result;
}