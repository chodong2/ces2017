set CC="C:\Program Files (x86)\IAR Systems\Embedded Workbench 7.2\common\bin\IarBuild.exe" Project.ewp
set PRJ_PATH=C:\work\tiptalk_new\IAR_prj\Debug

:clean 
	%CC% -clean Debug -log all

:make
	%CC% Debug

@echo off
if exist %PRJ_PATH%\Exe\project.hex goto end
::if not exist %PRJ_PATH%\Exe\Project.hex goto error


:error
echo build error
goto nend

:end
echo Debug build complete

	%CC% -clean Release
	%CC% Release

echo Release build complete

:nend