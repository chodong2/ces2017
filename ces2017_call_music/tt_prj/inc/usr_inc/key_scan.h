#ifndef _KEY_SCAN_H_
#define _KEY_SCAN_H_

typedef enum key_state_{
  	KEY_NONE			= 0x0000,
	KEY_POWER_PRESS		= 0x0001,
	KEY_POWER_RELEASE	= 0x0002,

	KEY_SOURCE_UP		= 0x0004,
	KEY_SOURCE_DOWN		= 0x0008,

	KEY_VOLUP_PRESS		= 0x0010,
	KEY_VOLUP_RELEASE	= 0x0020,

	KEY_VOLDN_PRESS		= 0x0040,
	KEY_VOLDN_RELEASE	= 0x0080,
	
	KEY_PAIR			= 0x0050,
}key_state_t;

typedef enum key_event_{
  	KEY_EVENT_NONE		= 0,
	KEY_EVENT_POWER		= 1,
	KEY_EVENT_SOURCE	= 2,
	KEY_EVENT_VOLUP		= 3,
	KEY_EVENT_VOLDN		= 4,
}key_event_t;

extern key_state_t g_key_state;
extern key_event_t g_key_event;

void key_scan( void );

#endif //_KEY_SCAN_H_