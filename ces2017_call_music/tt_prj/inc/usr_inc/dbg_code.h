#ifndef DBG_CODE_H
#define DBG_CODE_H

#define DBG_TIM_IRQ     TIM2_IRQn
#define DBG_TIM_RCC     RCC_APB1Periph_TIM2
#define DBG_TIM_BASE    TIM2

void dbg_init( void );
void dbg_timer_config_us( uint32_t ui32usec );
void dbg_timer_stop( void );
void dbg_timer_start( uint32_t ui32usec );

#define dbg_timer_isr                  TIM2_IRQHandler

extern uint16_t g_ui16_dbg_cnt;

#endif //DBG_CODE_H