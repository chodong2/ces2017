#ifndef AUDIO_EVENT_H
#define AUDIO_EVENT_H

#define AUDIO_I2S_FREQ                     I2S_AudioFreq_44k
#define AUDIO_RX_FREQ                      I2S_AudioFreq_44k

/* I2S2 Receiver peripheral configuration defines */
#define AUDIO_I2S_RX                       SPI2
#define AUDIO_I2S_RX_CLK                   RCC_APB1Periph_SPI2
#define AUDIO_I2S_RX_GPIO_AF               GPIO_AF_SPI2
#define AUDIO_I2S_RX_IRQ                   SPI2_IRQn
#define AUDIO_I2S_RX_GPIO_CLOCK            RCC_AHB1Periph_GPIOB
#define AUDIO_I2S_RX_WS_PIN                GPIO_Pin_9
#define AUDIO_I2S_RX_CK_PIN                GPIO_Pin_13
#define AUDIO_I2S_RX_SD_PIN                GPIO_Pin_15
#define AUDIO_I2S_RX_WS_SOURCE             GPIO_PinSource9
#define AUDIO_I2S_RX_CK_SOURCE             GPIO_PinSource13
#define AUDIO_I2S_RX_SD_SOURCE             GPIO_PinSource15
#define AUDIO_I2S_RX_GPIO_PORT             GPIOB


/* I2S3 Transmitter peripheral configuration defines */
#define AUDIO_I2S_TX                      SPI3
#define AUDIO_I2S_TX_CLK                  RCC_APB1Periph_SPI3
#define AUDIO_I2S_TX_GPIO_AF              GPIO_AF_SPI3
#define AUDIO_I2S_TX_SCK_GPIO_AF          GPIO_AF7_SPI3
#define AUDIO_I2S_TX_IRQ                  SPI3_IRQn
#define AUDIO_I2S_TX_GPIO_CLOCK           (RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOA)
#define AUDIO_I2S_TX_WS_PIN               GPIO_Pin_4
#define AUDIO_I2S_TX_SD_PIN               GPIO_Pin_5
#define AUDIO_I2S_TX_MCK_PIN              GPIO_Pin_10
#define AUDIO_I2S_TX_SCK_PIN              GPIO_Pin_12
#define AUDIO_I2S_TX_WS_SOURCE            GPIO_PinSource4
#define AUDIO_I2S_TX_SD_SOURCE            GPIO_PinSource5
#define AUDIO_I2S_TX_MCK_SOURCE           GPIO_PinSource10
#define AUDIO_I2S_TX_SCK_SOURCE           GPIO_PinSource12
#define AUDIO_I2S_TX_WS_GPIO              GPIOA
#define AUDIO_I2S_TX_GPIO                 GPIOB

#define AUDIO_FRAME_SIZE                  80

void audio_init(uint16_t audio);
void audio_i2s_config_rx(uint16_t audio);
void audio_i2s_config_tx(uint16_t audio);
void audio_rx_start( void );
void audio_rx_stop( void );

#define audio_rx_isr                 SPI2_IRQHandler
#define audio_tx_isr                 SPI3_IRQHandler

#endif //AUDIO_EVENT_H
