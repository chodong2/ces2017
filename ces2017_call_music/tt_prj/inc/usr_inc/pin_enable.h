#ifndef PIN_ENABLE_H
#define PIN_ENABLE_H

void pin_enable_init(void);
void pin_enable_gpio(void);

#if 0
void pin_enable_reset(void);
#endif

#define PIN_ENABLE_POWER                GPIO_Pin_12
#define PIN_ENABLE_AMP                  GPIO_Pin_14

#define PIN_ENABLE_MFB					GPIO_Pin_5
#define PIN_ENABLE_BTRESET				GPIO_Pin_6

#define PIN_ENABLE_ALL                  ( PIN_ENABLE_POWER | PIN_ENABLE_AMP )

#define POWER_HOLD_ENABLE               GPIOA->BSRRL = PIN_ENABLE_POWER
#define POWER_HOLD_DISABLE              GPIOA->BSRRH = PIN_ENABLE_POWER

#define AMP_STATUS_ENABLE               GPIOB->BSRRL = PIN_ENABLE_AMP
#define AMP_STATUS_DISABLE              GPIOB->BSRRH = PIN_ENABLE_AMP

#define MFB_ENABLE						GPIOA->BSRRL = PIN_ENABLE_MFB
#define MFB_DISABLE						GPIOA->BSRRH = PIN_ENABLE_MFB

#define BTRESET_ENABLE					GPIOA->BSRRL = PIN_ENABLE_BTRESET
#define BTRESET_DISABLE					GPIOA->BSRRH = PIN_ENABLE_BTRESET


#endif //PIN_ENABLE_H