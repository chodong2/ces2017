#ifndef KEY_EVENT_H
#define KEY_EVENT_H

#define KEY_GPIO_VOL_WAKE_PORT			GPIOC
#define KEY_GPIO_VOL_WAKE_RCC			RCC_AHB1Periph_GPIOC

#define KEY_EXTI_VOL_WAKE_PORT			EXTI_PortSourceGPIOC
#define KEY_EXTI_VOL_WAKE_IRQ			EXTI15_10_IRQn

#define KEY_GPIO_SOURCE_PORT			GPIOB
#define KEY_GPIO_SOURCE_RCC				RCC_AHB1Periph_GPIOB

#define KEY_EXTI_SOURCE_PORT			EXTI_PortSourceGPIOB
#define KEY_EXTI_SOURCE_IRQ				EXTI9_5_IRQn

#define KEY_GPIO_SOURCE_PIN				GPIO_Pin_8

#define KEY_EXTI_SOURCE_PIN				EXTI_PinSource8
#define KEY_EXTI_SOURCE_LINE			EXTI_Line8

#define KEY_GPIO_WAKEUP_PIN				GPIO_Pin_13

#define KEY_EXTI_WAKEUP_PIN				EXTI_PinSource13
#define KEY_EXTI_WAKEUP_LINE			EXTI_Line13

#define KEY_GPIO_VOLDN_PIN				GPIO_Pin_14

#define KEY_EXTI_VOLDN_PIN				EXTI_PinSource14
#define KEY_EXTI_VOLDN_LINE				EXTI_Line14

#define KEY_GPIO_VOLUP_PIN				GPIO_Pin_15

#define KEY_EXTI_VOLUP_PIN				EXTI_PinSource15
#define KEY_EXTI_VOLUP_LINE				EXTI_Line15

#define KEY_GPIO_POWER_PORT				GPIOA
#define KEY_GPIO_POWER_PIN				GPIO_Pin_1
#define KEY_GPIO_POWER_RCC				RCC_AHB1Periph_GPIOA

#define KEY_EXTI_POWER_PORT				EXTI_PortSourceGPIOA
#define KEY_EXTI_POWER_PIN				EXTI_PinSource1
#define KEY_EXTI_POWER_LINE				EXTI_Line1
#define KEY_EXTI_POWER_IRQ				EXTI1_IRQn

void key_init( void );
void key_config_power( void );
void key_config_vol_wake( void );
void key_config_source( void );

#define motion_sensor_isr                               EXTI0_IRQHandler
#define key_power_isr					EXTI1_IRQHandler
#define key_vol_wake_isr				EXTI15_10_IRQHandler
#define key_source_isr					EXTI9_5_IRQHandler

#define POWER_KEY_STATUS				( GPIOA->IDR & KEY_GPIO_POWER_PIN )

#define VOLUP_KEY_STATUS				( GPIOC->IDR & KEY_GPIO_VOLUP_PIN )
#define VOLDN_KEY_STATUS				( GPIOC->IDR & KEY_GPIO_VOLDN_PIN )

#define SOURCE_KEY_STATUS				( GPIOB->IDR & KEY_GPIO_SOURCE_PIN )

#endif //KEY_EVENT_H