#ifndef _LOW_POWER_H_
#define _LOW_POWER_H_

void set_clk_low( void );
void set_clk_high( void );

void set_sleepMode( void );

#endif //_LOW_POWER_H_