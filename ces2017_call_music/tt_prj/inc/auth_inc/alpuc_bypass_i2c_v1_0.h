#ifndef _ALPUC_BYPASS_H_
#define _ALPUC_BYPASS_H_
/***************************************************************************************
* Define
***************************************************************************************/
#define ERROR_CODE_TRUE		0
#define ERROR_CODE_FALSE	1
#define ERROR_CODE_WRITE_ADDR	10
#define ERROR_CODE_WRITE_DATA	20
#define ERROR_CODE_READ_ADDR	30
#define ERROR_CODE_READ_DATA	40
#define ERROR_CODE_START_BIT	50
#define ERROR_CODE_APROCESS	60
#define ERROR_CODE_DENY		70

/***************************************************************************************
* Global Variable
***************************************************************************************/
//라이브러리내에 선언된 변수입니다. extern으로 호출하여 사용하십시요.
extern unsigned char alpuc_tx_data[16];
extern unsigned char alpuc_rx_data[16];

/***************************************************************************************
* Function Proto Type
***************************************************************************************/

/***************************************************************************************
* Extern Function
***************************************************************************************/
extern unsigned char alpuc_process(unsigned char *, unsigned char *); //
extern void _alpu_delay_10ms(unsigned int i);
extern unsigned char _alpu_rand(void);
extern unsigned char _i2c_write(unsigned char dev_addr, unsigned char address, unsigned char *data, int ByteNo);
extern unsigned char _i2c_read(unsigned char dev_addr, unsigned char address, unsigned char *data, int ByteNo);
extern int authenticate(void);

/* EOF */
#endif