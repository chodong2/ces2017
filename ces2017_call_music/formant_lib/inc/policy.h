#ifndef POLICY_H
#define POLICY_H

#include <string.h>

#define debug_printf              printf
#define BufferReset(a, b)         memset(a, 0, b*sizeof(float));
#define BufferCopy(a, b, c)       memcpy(b, a, c*sizeof(float));

#endif
