################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/AHDR.c \
../src/AHDR_rom.c \
../src/Common.c \
../src/basicop2.c \
../src/enh_formant.c \
../src/intrinsics.c \
../src/levinson.c \
../src/oper_32b.c \
../src/r_fft.c \
../src/rom_s-peaker.c \
../src/vad.c 

OBJS += \
./src/AHDR.o \
./src/AHDR_rom.o \
./src/Common.o \
./src/basicop2.o \
./src/enh_formant.o \
./src/intrinsics.o \
./src/levinson.o \
./src/oper_32b.o \
./src/r_fft.o \
./src/rom_s-peaker.o \
./src/vad.o 

C_DEPS += \
./src/AHDR.d \
./src/AHDR_rom.d \
./src/Common.d \
./src/basicop2.d \
./src/enh_formant.d \
./src/intrinsics.d \
./src/levinson.d \
./src/oper_32b.d \
./src/r_fft.d \
./src/rom_s-peaker.d \
./src/vad.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM32F4 -DSTM32 -DSTM32F411CCYx -DDEBUG -I"C:/work/openstm/formant_lib/inc" -O3 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


