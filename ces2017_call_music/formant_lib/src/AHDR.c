#include <stdlib.h>
#include <stdio.h>
#include "Common.h"
#include "AHDR.h"
#include "intrinsics.h"



int prevEnvelope;
int prevG;
short x_lp[FRAMESIZE];
int zi[3][2];
int zo[3][2];

int env2gain(int val);
int log2_tablelookup(int val);
int fixed_pow2(short exponent, short fraction);
int iirFilter(int *coeff, short *x, short *y, int *zi, int *zo, int l);

int reset_AHDR()
{

    prevEnvelope = 1000;
    prevG = 32767;         //Q.15

    zi[0][0] = 0;
    zi[0][1] = 0;
    zo[0][0] = 0;
    zo[0][1] = 0;

    zi[1][0] = 0;
    zi[1][1] = 0;
    zo[1][0] = 0;
    zo[1][1] = 0;

    zi[2][0] = 0;
    zi[2][1] = 0;
    zo[2][0] = 0;
    zo[2][1] = 0;

    return NO_ERROR;
}


int AcousticHighDynamicRange_exe
(
short *x,
int l
)
{
    int i;
    int envelope;
    int val0, val1, g;


    iirFilter(IirCoeff[0], x, x_lp, zi[0], zo[0], l);
    iirFilter(IirCoeff[1], x_lp, x_lp, zi[1], zo[1], l);
    iirFilter(IirCoeff[2], x_lp, x_lp, zi[2], zo[2], l);


    // Search envelope
    envelope = iabs(x_lp[0]);
    for (i = 1; i<l; i++)
    {
        val0 = iabs(x_lp[i]);
        if(val0 > envelope)
            envelope = val0;
    }

    g = env2gain(envelope);  //Q.15


    for (i = 0; i < l; i++)
    {
        //val0 = smul32x32(g - prevG, INV_FRAMESIZE*i, 31) + prevG;
        val0 = g;
        val1 = (smul32x32(val0, x[i], 14) + 1) >> 1;
        x[i] = (short)val1;
        if (val1 > 0x00007FFF)
        {
            x[i] = (short)0x00007FFF;
        }
        if (val1 < -32768)
        {
            x[i] = (short)-32768;
        }
    }


    return NO_ERROR;
}


int env2gain(int val)
{
    int g, frac, exp;


    if (val < TN)
        g = AHDR_VAL0 + smul32x32(AHDR_VAL1, AHDR_VAL2 - log2_tablelookup(val), 24);
    else if ((TN <= val) && (val < TE))
        g = smul32x32(AHDR_VAL3, AHDR_VAL4 - log2_tablelookup(val), 24);
    else if((TE <= val) && (val < TC))
        g = 0;
    else if((TC <= val) && (val < TL))
        g = smul32x32(AHDR_VAL5, AHDR_VAL6 - log2_tablelookup(val), 24);
    else if (TL <= val)
        g = AHDR_VAL7 + smul32x32(AHDR_VAL8, AHDR_VAL9 - log2_tablelookup(val), 24);

    g = g >> 9;
    frac = g & 0x7fff;
    exp = g >> 15;
    g = fixed_pow2((short)(exp + 15), (short)frac); // Gain is represented in Q15 format


    if (g > prevG)
    {
        g = smul32x32(UP_SMOOTHING, prevG, 31) + smul32x32(0x7FFFFFFF - UP_SMOOTHING, g, 31);
    }
    else
    {
        g = smul32x32(DOWN_SMOOTHING, prevG, 31) + smul32x32(0x7FFFFFFF - DOWN_SMOOTHING, g, 31);
    }
    prevG = g;

    return g;
}


int log2_tablelookup(int val)
{
    int result, i;

    i = val >> 5;
    if (i > 1023)
        i = 1023;
    if (val > 0)
    {
        result = LogTable[i - 1] + smul32x32(LogTable[i] - LogTable[i - 1], val & 0x1F, 5);
    }
    else
    {
        i = val;
        result = LogTable[i - 1] - 5;
    }

    return result; //Q.28
}


int fixed_pow2(short exponent, short fraction)
{
    short exp, i, a, tmp;
    int L_x;
    L_x = smul32x32(fraction, 64, 0);
    i = (short)(L_x >> 16);
    L_x = L_x >> 1;
    a = (short)L_x;
    a = a & (short)0x7fff;
    L_x = (int)Pow_tab[i] << 16;
    tmp = Pow_tab[i] - Pow_tab[i + 1];
    L_x -= (tmp * a * 2);
    exp = 30 - exponent;
    L_x = iround(L_x, exp);
    return(L_x);
}


int iirFilter(int *coeff, short *x, short *y, int *zi, int *zo, int l)
{
    int i, val0, val1;
    int a1, a2, b1, b2, b3, g;
    int zo1, zo2, zi1, zi2;

    a1 = coeff[0];
    a2 = coeff[1];
    b1 = coeff[2];
    b2 = coeff[3];
    b3 = coeff[4];
    g = coeff[5];
    zo1 = zo[0];
    zo2 = zo[1];
    zi1 = zi[0];
    zi2 = zi[1];
    for (i = 0; i < l; i++)
    {
        val0 = smul32x32(x[i], g, 21);   //Q.24
        val1 = smul32x32(b1, val0, 30)
            + smul32x32(b2, zi1, 30)
            + smul32x32(b3, zi2, 30)
            - smul32x32(a1, zo1, 30)
            - smul32x32(a2, zo2, 30);

        zi2 = zi1;
        zi1 = val0;

        zo2 = zo1;
        zo1 = val1;

        y[i] = val1 >> 9;
    }

    zo[0] = zo1;
    zo[1] = zo2;
    zi[0] = zi1;
    zi[1] = zi2;


    return NO_ERROR;
}
