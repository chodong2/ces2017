/* r_fft.c */

/****************************************************************/
/*								*/
/*								*/
/*		COPYRIGHT 1995, MOTOROLA INC.			*/
/*								*/
/*								*/
/****************************************************************/

/*****************************************************************
 *
 * This is an implementation of decimation-in-time FFT algorithm for
 * real sequences.  The techniques used here can be found in several
 * books, e.g., i) Proakis and Manolakis, "Digital Signal Processing",
 * 2nd Edition, Chapter 9, and ii) W.H. Press et. al., "Numerical
 * Recipes in C", 2nd Ediiton, Chapter 12.
 *
 * Input -  There are two inputs to this function:
 *
 *	1) A float pointer to the input data array
 *	2) An integer value which should be set as +1 for FFT
 *	   and some other value, e.g., -1 for IFFT
 *
 * Output - There is no return value.
 *	The input data are replaced with transformed data.  If the
 *	input is a real time domain sequence, it is replaced with
 *	the complex FFT for positive frequencies.  The FFT value
 *	for DC and the foldover frequency are combined to form the
 *	first complex number in the array.  The remaining complex
 *	numbers correspond to increasing frequencies.  If the input
 *	is a complex frequency domain sequence arranged	as above,
 *	it is replaced with the corresponding time domain sequence.
 *
 * Notes:
 *
 *	1) This function is designed to be a part of a noise supp-
 *	   ression algorithm that requires 128-point FFT of real
 *	   sequences.  This is achieved here through a 64-point
 *	   complex FFT.  Consequently, the FFT size information is
 *	   not transmitted explicitly.  However, some flexibility
 *	   is provided in the function to change the size of the
 *	   FFT by specifying the size information through "define"
 *	   statements.
 *
 *	2) The values of the complex sinusoids used in the FFT
 *	   algorithm are computed once (i.e., the first time the
 *	   r_fft function is called) and stored in a table. To
 *	   further speed up the algorithm, these values can be
 *	   precomputed and stored in a ROM table in actual DSP
 *	   based implementations.
 *
 *	3) In the c_fft function, the FFT values are divided by
 *	   2 after each stage of computation thus dividing the
 *	   final FFT values by 64.  No multiplying factor is used
 *	   for the IFFT.  This is somewhat different from the usual
 *	   definition of FFT where the factor 1/N, i.e., 1/64, is
 *	   used for the IFFT and not the FFT.  No factor is used in
 *	   the r_fft function.
 *
 *	4) Much of the code for the FFT and IFFT parts in r_fft
 *	   and c_fft functions are similar and can be combined.
 *	   They are, however, kept separate here to speed up the
 *	   execution.
 *
 *
 * Written by:			Tenkasi V. Ramabadran
 * Date:				December 20, 1994
 *
 *
 * Version			Description
 *
 *   1.0				 Original
 *
 *****************************************************************/

/* Includes */

#include <math.h>
#include "Common.h"
#include "intrinsics.h"


#define			NUM_STAGE		6

/* External variables */
extern int phs_tbl[]; /* holds the complex sinusoids */
int farray_ptr[FFTSIZE];
void c_fft(int *farray_ptr, int isign);


/* FFT/IFFT function for real sequences */
void r_fft(float *farray_ptr1, int isign)
{
    int ftmp1_real, ftmp1_imag, ftmp2_real, ftmp2_imag;
    int i, j;

    for (i = 0; i < FFTSIZE; i++)
    {
        farray_ptr[i] = (int)(farray_ptr1[i] *31+0.5f);
    }
    /* If this is the first call to the function, fill up the phase table  */

    /* The FFT part */
    if (isign == 1)
    {
        /* Perform the complex FFT */
        c_fft(farray_ptr, isign);
        
        /* First, handle the DC and foldover frequencies */
        ftmp1_real = farray_ptr[0];
        ftmp2_real = farray_ptr[1];
        farray_ptr[0] = ftmp1_real + ftmp2_real;
        farray_ptr[1] = ftmp1_real - ftmp2_real;

        /* Now, handle the remaining positive frequencies */
        for (i = 2, j = FFTSIZE - i; i <= FFTSIZE_BY_TWO; i = i + 2, j = FFTSIZE - i)
        {
            ftmp1_real = farray_ptr[i] + farray_ptr[j];
            ftmp1_imag = farray_ptr[i + 1] - farray_ptr[j + 1];
            ftmp2_real = farray_ptr[i + 1] + farray_ptr[j + 1];
            ftmp2_imag = farray_ptr[j] - farray_ptr[i];

            farray_ptr[i] = (ftmp1_real + smul32x32(phs_tbl[i], ftmp2_real, 24) - smul32x32(phs_tbl[i + 1], ftmp2_imag, 24)) >> 1;
            farray_ptr[i + 1] = (ftmp1_imag + smul32x32(phs_tbl[i], ftmp2_imag, 24) + smul32x32(phs_tbl[i + 1], ftmp2_real, 24)) >> 1;
            farray_ptr[j] = (ftmp1_real + smul32x32(phs_tbl[j], ftmp2_real, 24) + smul32x32(phs_tbl[j + 1], ftmp2_imag, 24)) >> 1;
            farray_ptr[j + 1] = ( - ftmp1_imag - smul32x32(phs_tbl[j], ftmp2_imag, 24) + smul32x32(phs_tbl[j + 1], ftmp2_real, 24)) >> 1;
        }
    }
    /* The IFFT part */
    else
    {
        /* First, handle the DC and foldover frequencies */
        ftmp1_real = farray_ptr[0];
        ftmp2_real = farray_ptr[1];
        farray_ptr[0] = (ftmp1_real + ftmp2_real) >> 1;
        farray_ptr[1] = (ftmp1_real - ftmp2_real) >> 1;

        /* Now, handle the remaining positive frequencies */
        for (i = 2, j = FFTSIZE - i; i <= FFTSIZE_BY_TWO; i = i + 2, j = FFTSIZE - i)
        {
            ftmp1_real = farray_ptr[i] + farray_ptr[j];
            ftmp1_imag = farray_ptr[i + 1] - farray_ptr[j + 1];
            ftmp2_real =  - (farray_ptr[i + 1] + farray_ptr[j + 1]);
            ftmp2_imag =  - (farray_ptr[j] - farray_ptr[i]);

            farray_ptr[i] = (ftmp1_real + smul32x32(phs_tbl[i], ftmp2_real, 24) + smul32x32(phs_tbl[i + 1], ftmp2_imag, 24)) >> 1;
            farray_ptr[i + 1] = (ftmp1_imag + smul32x32(phs_tbl[i], ftmp2_imag, 24) - smul32x32(phs_tbl[i + 1], ftmp2_real, 24)) >> 1;
            farray_ptr[j] = (ftmp1_real + smul32x32(phs_tbl[j], ftmp2_real, 24) - smul32x32(phs_tbl[j + 1], ftmp2_imag, 24)) >> 1;
            farray_ptr[j + 1] = ( - ftmp1_imag - smul32x32(phs_tbl[j], ftmp2_imag, 24) - smul32x32(phs_tbl[j + 1], ftmp2_real, 24)) >> 1;
        }

        /* Perform the complex IFFT */
        c_fft(farray_ptr, isign);
    }

    for (i = 0; i < FFTSIZE; i++)
    {
        farray_ptr1[i] = (float)((float)farray_ptr[i] / 31.f);
    }
    return ;
} /* end r_fft () */



/* FFT/IFFT function for complex sequences */
/* The decimation-in-time complex FFT/IFFT is implemented below.
The input complex numbers are presented as real part followed by
imaginary part for each sample.  The counters are therefore
incremented by two to access the complex valued samples. */
void c_fft(int *farray_ptr, int isign)
{
    int i, j, k, ii, jj, kk, ji, kj;
    int ftmp, ftmp_real, ftmp_imag;

    /* Rearrange the input array in bit reversed order */
    for (i = 0, j = 0; i < FFTSIZE - 2; i = i + 2)
    {
        if (j > i)
        {
            ftmp = farray_ptr[i];
            farray_ptr[i] = farray_ptr[j];
            farray_ptr[j] = ftmp;

            ftmp = farray_ptr[i + 1];
            farray_ptr[i + 1] = farray_ptr[j + 1];
            farray_ptr[j + 1] = ftmp;
        }

        k = FFTSIZE_BY_TWO;
        while (j >= k)
        {
            j -= k;
            k >>= 1;
        }
        j += k;
    }

    /* The FFT part */
    if (isign == 1)
    {
        for (i = 0; i < NUM_STAGE; i++)
        {
            /* i is stage counter */
            jj = (2 << i); /* FFT size */
            kk = (jj << 1); /* 2 * FFT size */
            ii = FFTSIZE / jj; /* 2 * number of FFT's */
            for (j = 0; j < jj; j = j + 2)
            {
                /* j is sample counter */
                ji = j * ii; /* ji is phase table index */
                for (k = j; k < FFTSIZE; k = k + kk)
                {
                    /* k is butterfly top */
                    kj = k + jj; /* kj is butterfly bottom */

                    /* Butterfly computations */
                    ftmp_real = smul32x32(farray_ptr[kj], phs_tbl[ji], 24) - smul32x32(farray_ptr[kj + 1], phs_tbl[ji + 1], 24);
                    ftmp_imag = smul32x32(farray_ptr[kj + 1], phs_tbl[ji], 24) + smul32x32(farray_ptr[kj], phs_tbl[ji + 1], 24);

                    farray_ptr[kj] = (farray_ptr[k] - ftmp_real) >> 1;
                    farray_ptr[kj + 1] = (farray_ptr[k + 1] - ftmp_imag) >> 1;
                    farray_ptr[k] = (farray_ptr[k] + ftmp_real) >> 1;
                    farray_ptr[k + 1] = (farray_ptr[k + 1] + ftmp_imag) >> 1;
                }

            }
        }
    }
    /* The IFFT part */
    else
    {
        for (i = 0; i < NUM_STAGE; i++)
        {
            /* i is stage counter */
            jj = (2 << i); /* FFT size */
            kk = (jj << 1); /* 2 * FFT size */
            ii = FFTSIZE / jj; /* 2 * number of FFT's */
            for (j = 0; j < jj; j = j + 2)
            {
                /* j is sample counter */
                ji = j * ii; /* ji is phase table index */
                for (k = j; k < FFTSIZE; k = k + kk)
                {
                    /* k is butterfly top */
                    kj = k + jj; /* kj is butterfly bottom */

                    /* Butterfly computations */
                    ftmp_real = smul32x32(farray_ptr[kj], phs_tbl[ji], 24) + smul32x32(farray_ptr[kj + 1], phs_tbl[ji + 1], 24);
                    ftmp_imag = smul32x32(farray_ptr[kj + 1], phs_tbl[ji], 24) - smul32x32(farray_ptr[kj], phs_tbl[ji + 1], 24);

                    farray_ptr[kj] = farray_ptr[k] - ftmp_real;
                    farray_ptr[kj + 1] = farray_ptr[k + 1] - ftmp_imag;
                    farray_ptr[k] += ftmp_real;
                    farray_ptr[k + 1] += ftmp_imag;
                }
            }
        }
    }

    return ;
} /* end of c_fft () */
